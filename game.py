#!/usr/bin/env python

#
#  game.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#




import pygame
import pygame.freetype
import random
import elfi
import world
import utilities
import time
import sys

pygame.init()
pygame.freetype.init()
pygame.mixer.quit()

gameDisplay=pygame.display.set_mode((utilities.display_width, utilities.display_height))
pygame.display.set_caption('Elfis Dreamland')

world.current_world = world.World(gameDisplay)
world.current_world.main_loop()

