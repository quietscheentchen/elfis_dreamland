#
#  objects.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#




import pygame
import sprite
import utilities
import block
import random
import world
import math


class Cookie(sprite.Sprite):

    def __init__(self,x,y,speed_x=0,speed_y=0,gravity=0,collectmode = True):
        sprite.Sprite.__init__(self,x,y,speed_x,speed_y,gravity,collectmode)
        self.image = pygame.image.load('graphics/cookie.png')
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.delete = True
        self.speed_x = speed_x
        self.speed_y = speed_y
        self.gravity = gravity
        self.collectmode = collectmode
        self.on_top_of = None
        self.timer = 0

    def update(self):
        if self.collectmode == False:
            self.timer +=1
            if self.timer >60:
                self.collectmode = True

        if self.gravity == 0:
            return

        self.speed_y = self.speed_y + self.gravity

        self.rect.x += self.speed_x
        self.rect.y += self.speed_y
        if self.on_top_of != None:
            self.rect.x += self.on_top_of.speed_x
            self.rect.y += self.on_top_of.speed_y

        self.boundary_check()

        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        for tpl in collisions:
            t = tpl[0]
            block = tpl[1]
            direction = tpl[2]

            if block == self:
                continue

            if not utilities.collide_myrect(self,block):
                continue

            block.collision(direction,t,self,speed_x-block.speed_x,speed_y-block.speed_y)

    def boundary_check(self):

        if self.rect.x <= -self.rect.width and self.speed_x<0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].left
            self.rect.x = utilities.display_width - self.rect.width - 10

        elif self.rect.x >= utilities.display_width and self.speed_x > 0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].right
            self.rect.x = 5

        elif self.rect.y <= 0 and self.speed_y<0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].up
            if newroom == 0:
                return
            self.rect.y = utilities.display_height - self.rect.height

        elif self.rect.y >= utilities.display_height and self.speed_y > 0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].down
            self.rect.y = 1
        else:
            return

        if newroom == 0:
            self.kill()
        else:
            groups = self.groups()
            self.kill()

            for g0 in groups:
                if hasattr(g0,'name'):
                    world.current_world.current_level.rooms[newroom].add(g0.name,self)

    def block_collision(self,direction,t,player,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.rect.x -= utilities.round_up(t*speed_x) + utilities.sgn(speed_x)

        if direction == "up" or direction == "down":
            self.rect.y -= utilities.round_up(t*speed_y)
            self.speed_y = 0
            self.speed_x = 0

    def collect(self,player):
        if self.collectmode:
            player.cookiecount += 1
            self.kill()

    def sit_on(self,sprite):
        self.on_top_of = sprite
        sprite.rider = self

    def turn(self):
        pass

class FlowerCusp(sprite.Sprite):

    def __init__(self,x,y,n):
        sprite.Sprite.__init__(self,x,y,n)
        self.image = pygame.image.load('graphics/flower_cusp.png')

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.cookies = n

        self.speed_x = 0
        self.speed_y = 0
        self.cookies = n

    def get_wet(self):
        self.flower = Cookieflower(self.rect.x,self.rect.y-80,self.cookies)
        room = world.current_world.current_level.rooms[world.current_world.current_level.current_room]
        room.add('background',self.flower)
        #print('flower grows')
        self.kill()

    def destroy(self):
        if hasattr(self,'flower'):
            self.flower.destroy()
        sprite.Sprite.destroy(self)

    def collision(self,direction,t,blockspeed_x,speed_y,blockspeed_y):
        return

class Cookieflower(sprite.Sprite):

    def __init__(self,x,y,n):
        sprite.Sprite.__init__(self,x,y,n)
        self.image = pygame.image.load('graphics/cookieflower.png')
        self.wilted_image = pygame.image.load('graphics/wiltedflower.png')
        # muss man irgendwann ein andres rect nehmen was nur die blüte ist als maske
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.cookies = n
        self.dead = False
        self.delete = False
        self.timer = 0
        self.cookie_list = []

        self.myrect = pygame.Rect((20,22), (37,37))
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)

        self.speed_x = 0
        self.speed_y = 0


    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:
            return

        if utilities.diff_framecount(utilities.framecount,self.timer) < 40:
            return

        self.timer = utilities.framecount
        if self.dead == False:


            cookie_speed_x = random.randint(0,10)*(2*random.randint(0,1)-1)
            cookie_speed_y =- random.randint(10,40)

            cookie = Cookie(self.rect.x+self.rect.width/2-23,self.rect.y,cookie_speed_x,cookie_speed_y, 2, False)
            self.cookie_list.append(cookie)

            room = world.current_world.current_level.rooms[world.current_world.current_level.current_room];
            room.add('objects',cookie)
            self.cookies -= 1
            if self.cookies <0:
                self.wilt()

    def destroy(self):
        for cookie in self.cookie_list:
            cookie.destroy()
        sprite.Sprite.destroy(self)

    def wilt(self):
        if self.dead == False:
            self.dead = True
            self.image = self.wilted_image
            self.rect.y += 45

class WaterJet(sprite.Sprite):
    def __init__(self,x,y,xrel=0,yrel=1):
        sprite.Sprite.__init__(self,x,y,xrel,yrel)

        # xrel =0 yrel =0 : nach unten
        # yrel >0 : nach unten
        # yrel <0 : nach oben
        # xrel >0 : nach rechts
        # xrel <0 : nach links
        self.angle = math.degrees(math.atan2(-yrel,xrel))

        long_jet = pygame.image.load('graphics/jet_new_1.png')
        medium_jet = pygame.image.load('graphics/jet_new_2.png')
        short_jet = pygame.image.load('graphics/jet_new_3.png')
        rect = long_jet.get_rect()

        x0 = -5
        y0 = 30

        self.pictures = [
            (pygame.transform.rotate(short_jet, self.angle),20),
            (pygame.transform.rotate(medium_jet, self.angle),20),
            (pygame.transform.rotate(long_jet, self.angle),60),
            (pygame.transform.rotate(medium_jet, self.angle),20),
            (pygame.transform.rotate(short_jet, self.angle),20)
        ]


        self.picture = 0

        self._set_image()

        self.rect = self.image.get_rect()

        self.rect.x = x - (x0-rect.width/2)*math.cos(math.radians(self.angle)) - (y0-rect.height/2)*math.sin(math.radians(self.angle)) - self.rect.width/2
        self.rect.y = y + (x0-rect.width/2)*math.sin(math.radians(self.angle)) - (y0-rect.height/2)*math.cos(math.radians(self.angle)) - self.rect.height/2
        self.done = False
        self.timer = utilities.framecount



        #TODO: Für wasser exakte makse verwenden
        self.myrect = pygame.Rect((20,22), (37,37))
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)

        self.speed_x = 0
        self.speed_y = 0

    def _set_image(self):
        self.image = self.pictures[self.picture][0]
        self.mask = pygame.mask.from_surface(self.image)

    def update(self):

        if utilities.diff_framecount(utilities.framecount,self.timer) > self.pictures[self.picture][1]:
            self.timer = utilities.framecount
            self.picture += 1
            if self.picture == len(self.pictures):
                self.destroy()
            else:
                self._set_image()

    def destroy(self):
        self.done = True
        sprite.Sprite.destroy(self)

    def collect(self,player):
        if player.mode != 'umbrella':
            player.get_wet(3)
        else:
            return


class WaterCan(sprite.Sprite):

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/gieskanne.png')
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.delete = True
        self.speed_x = 0
        self.speed_y = 0


    def collect(self,player):
        if player.mode == 'gardener':
            player.cookiecount +=20
        else:
            player.set_mode('gardener')
        self.kill()



class WaterDrops(sprite.Sprite):
    def __init__(self,x,y,flipped):
        sprite.Sprite.__init__(self,x,y,flipped)

        self.pictures = [
                        pygame.image.load('graphics/water-drops.png')
        ]

        self.picture = 0
        self.flipped = flipped
        self._set_image()


        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0
        #self.timer = utilities.framecount

    def _set_image(self):
        if self.flipped:
            self.image = pygame.transform.flip(self.pictures[self.picture],True,False)
        else:
            self.image = self.pictures[self.picture]
        self.mask = pygame.mask.from_surface(self.image)

    def update(self):
        bg = world.current_world.current_level.rooms[world.current_world.current_level.current_room].background
        fg = world.current_world.current_level.rooms[world.current_world.current_level.current_room].foreground

        collisions = pygame.sprite.spritecollide(self,bg,False,utilities.collide_myrect) + \
                     pygame.sprite.spritecollide(self,fg,False,utilities.collide_myrect)

        for obj in collisions:
            if hasattr(obj,'get_wet'):
                obj.get_wet()

    def  collect(self,player):
        return

class Umbrella(sprite.Sprite):

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/umbrella.png')
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.delete = True
        self.speed_x = 0
        self.speed_y = 0


    def collect(self,player):
        if player.mode == 'umbrella':
            player.cookiecount +=20
        else:
            player.set_mode('umbrella')
        self.kill()


class RainDrop(sprite.Sprite):
    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)

        self.image = pygame.image.load('graphics/raindrop.png')

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 5

        self.mask = pygame.mask.from_surface(self.image)

    def update(self):
        bg = world.current_world.current_level.rooms[world.current_world.current_level.current_room].background
        fg = world.current_world.current_level.rooms[world.current_world.current_level.current_room].foreground

        collisions = pygame.sprite.spritecollide(self,bg,False,utilities.collide_myrect) + \
                     pygame.sprite.spritecollide(self,fg,False,utilities.collide_myrect)

        if len(collisions)>0:
            self.kill()

        self.rect.y += self.speed_y

        if self.rect.y > utilities.display_height:
            self.kill()

    def  collect(self,player):
        self.kill()
        if player.mode != 'umbrella':
            player.get_wet(5)


class Spiderweb(sprite.Sprite):

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/spiderweb.png')
        self.mask = pygame.mask.from_surface(self.image)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.delete = True
        self.speed_x = 0
        self.speed_y = 0


    def collect(self,player):

        player.spiderweb_timer = utilities.framecount

        self.kill()


