#
#   elfi.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#




import pygame
import sprite
import math
import utilities
import world
import time
import sys
import objects
import random

class Elfi(sprite.Sprite):

    def __init__(self,x=0,y=0,flipped=False):
        sprite.Sprite.__init__(self,x,y,flipped)

        self.is_player = True

        # normaler modus
        self.normal_pictures = self.prepare_pictures([
            (math.inf,pygame.image.load('graphics/elfi3.png'),pygame.Rect((39,0),(46,82)))
        ])

        self.normal_walking_pictures = self.prepare_pictures([
            (15,pygame.image.load('graphics/elfi3.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/elfi2.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/elfi3.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/elfi.png'),pygame.Rect((39,0),(46,82)))
        ])

        self.normal_shocked_pictures = self.prepare_pictures([
            (120,pygame.image.load('graphics/shocked_elfi.png'),pygame.Rect((39,0),(47,82)))
        ],('normal',None))

        # gärtner elfi
        self.gardener_pictures = self.prepare_pictures([
            (15,pygame.image.load('graphics/gaertner_elfi1.png'),pygame.Rect((39,17),(47,82)))
        ])

        self.gardener_walking_pictures = self.prepare_pictures([
            (15,pygame.image.load('graphics/gaertner_elfi1.png'),pygame.Rect((39,17),(47,82))),
            (15,pygame.image.load('graphics/gaertner_elfi_2.png'),pygame.Rect((39,17),(47,82))),
            (15,pygame.image.load('graphics/gaertner_elfi1.png'),pygame.Rect((39,17),(47,82))),
            (15,pygame.image.load('graphics/gaertner_elfi_3.png'),pygame.Rect((39,17),(47,82))),
        ])

        self.gardener_watering_pictures = self.prepare_pictures([
            (math.inf,pygame.image.load('graphics/watering_elfi.png'),pygame.Rect((39,17),(47,82)))
        ])

        self.gardener_shocked_pictures = self.prepare_pictures([
            (120,pygame.image.load('graphics/shocked_elfi.png'),pygame.Rect((39,0),(47,82)))    # TODO
        ],('gardener',None))

        # regenschirm elfi
        self.umbrella_pictures = self.prepare_pictures([
            (15,pygame.image.load('graphics/elfi_in_the_rain1.png'),pygame.Rect((39,40),(47,82)))
        ])

        self.umbrella_walking_pictures = self.prepare_pictures([
            (15,pygame.image.load('graphics/elfi_in_the_rain1.png'),pygame.Rect((39,40),(47,82))),
            (15,pygame.image.load('graphics/elfi_in_the_rain2.png'),pygame.Rect((39,40),(47,82))),
            (15,pygame.image.load('graphics/elfi_in_the_rain1.png'),pygame.Rect((39,40),(47,82))),
            (15,pygame.image.load('graphics/elfi_in_the_rain3.png'),pygame.Rect((39,40),(47,82))),
        ])

        self.umbrella_strike_pictures = self.prepare_pictures([
            (5,pygame.image.load('graphics/elfi_in_the_rain4.png'),pygame.Rect((39,19),(47,82))),
            (10,pygame.image.load('graphics/elfi_in_the_rain5.png'),pygame.Rect((39,6),(47,82))),
            (10,pygame.image.load('graphics/elfi_in_the_rain5.png'),pygame.Rect((39,6),(47,82))),
            (5,pygame.image.load('graphics/elfi_in_the_rain4.png'),pygame.Rect((39,19),(47,82)))
        ],('umbrella',None))

        self.umbrella_strike_walking_pictures = self.prepare_pictures([
            (5,pygame.image.load('graphics/elfi_in_the_rain6.png'),pygame.Rect((39,19),(47,82))),
            (10,pygame.image.load('graphics/elfi_in_the_rain8.png'),pygame.Rect((39,6),(47,82))),
            (10,pygame.image.load('graphics/elfi_in_the_rain9.png'),pygame.Rect((39,6),(47,82))),
            (5,pygame.image.load('graphics/elfi_in_the_rain7.png'),pygame.Rect((39,19),(47,82))),
        ],('umbrella',None))

        self.umbrella_shocked_pictures = self.prepare_pictures([
            (120,pygame.image.load('graphics/shocked_elfi.png'),pygame.Rect((39,0),(47,82)))    # TODO
        ],('umbrella',None))

        self.umbrella_swimming_pictures = self.prepare_pictures([
            (1,pygame.image.load('graphics/umbrella_boat.png'),pygame.Rect((39,19),(47,82))),
            (1,pygame.image.load('graphics/umbrella_boat.png'),pygame.Rect((39,19),(47,82)))
        ],('umbrella',None))

        self.umbrella_swimming_shocked_pictures = self.prepare_pictures([
            (120,pygame.image.load('graphics/elfi_in_the_rain7.png'),pygame.Rect((39,0),(47,82)))   # TODO
        ],('umbrella',None))

        self.umbrella_swimming_walking_pictures = self.umbrella_swimming_pictures

        # arme verletzte elfi
        self.hurt_pictures = self.prepare_pictures([
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82)))
        ],('normal',None))

        self.hurt_walking_pictures = self.prepare_pictures([
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi2.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi3.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi2.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi.png'),pygame.Rect((39,0),(46,82))),
            (15,pygame.image.load('graphics/hurt_elfi3.png'),pygame.Rect((39,0),(46,82)))
        ],('normal',None))


        self.modi = [
            "normal",
            "normal_shocked",
            "gardener",
            "gardener_shocked",
            "gardener_watering",
            "umbrella",
            "umbrella_shocked",
            "umbrella_strike",
            "umbrella_swimming",
            "umbrella_swimming_shocked",
            "hurt" ]

        self.generate_spiderweb()

        self.picture = 0
        self.is_dead = False

        self.flipped = flipped

        self.speed_x = 0
        self.speed_y =0

        self.mode = 'normal'
        self.timer = utilities.framecount
        self.freeze = False

        self.spiderweb_timer = None

        self._set_picture()

        self.rect.x = x
        self.rect.y = y

        # evtl geht das mit dem alten timer auch?
        self.hoptimer = utilities.framecount

        # will man evtl später ändern für das häschenkostüm
        self.gravity = 2

        self.on_ground = True
        self.on_top_of = None

        self.cookiecount = 0
        self.lives = 3
        self.wet = 0


    def prepare_pictures(self,pics,next_mode=None):
        l = []
        for time,pic,myrect in pics:
            pic_flipped = pygame.transform.flip(pic,True,False)
            myrect_flipped = utilities.flip_myrect(myrect,pic.get_rect(),True,False)

            mask = pygame.mask.from_surface(pic)
            mask_flipped = pygame.mask.from_surface(pic_flipped)

            #pygame.draw.rect(pic, (255,0,0), myrect, 1)                         # zeichne das rechteck
            #pygame.draw.rect(pic_flipped, (255,0,0), myrect_flipped, 1)         # zeichne das rechteck

            #pygame.draw.lines(pic,(255,0,0),1,mask.outline())                   # zeichne die maske
            #pygame.draw.lines(pic_flipped,(255,0,0),1,mask_flipped.outline())   # zeichne die maske

            l.append( ( time, (pic,myrect,mask), (pic_flipped,myrect_flipped,mask_flipped) ) )
        if next_mode != None:
            l.append(next_mode)

        return l


    def generate_spiderweb(self):
        # finde maximale größe aller bilder heraus

        width=0
        height=0

        for modus in self.modi:
            for extra in [ '', '_walking' ]:
                if not hasattr(self,modus+extra+'_pictures'):
                    continue
                pics = getattr(self,modus+extra+'_pictures')

                for tpl in pics:
                    if len(tpl) == 2:
                        continue

                    pic = tpl[1][0]
                    rect = pic.get_rect()
                    if rect.width > width:
                        width = rect.width
                    if rect.height > height:
                        height = rect.height

        # zeichne spinnennetz

        web = pygame.Surface( (width, height), pygame.SRCALPHA)

        for n in range(0,80):
            pos1 = random.randint(0,width+height)
            pos2 = random.randint(0,width+height)

            if pos1 < width:
                pos1 = (pos1,0)
            else:
                pos1 = (0,pos1-width)

            if pos2 < width:
                pos2 = (pos2,height-1)
            else:
                pos2 = (width-1,pos2-width)

            pygame.draw.line(web, (200,200,200,220), pos1, pos2,2)


        # überlagere spinnennetz mit bilder

        for modus in self.modi:
            for extra in [ '', '_walking' ]:
                if not hasattr(self,modus+extra+'_pictures'):
                    continue
                pics = getattr(self,modus+extra+'_pictures')


                l = []
                for entry in pics:
                    if len(entry) == 2:
                        l.append(entry)
                        continue
                    time,(pic0,myrect,mask),(pic0_flipped,myrect_flipped,mask_flipped) = entry

                    rect = pic0.get_rect()

                    pic = pygame.Surface( (rect.width, rect.height), pygame.SRCALPHA)
                    pic.blits( blit_sequence = ((pic0,(0,0)), (web,(0,0))) )

                    for x in range(0,rect.width):
                        for y in range(0,rect.height):
                            if mask.get_at((x,y)) == 0:
                                pic.set_at((x,y),(0,0,0,0))

                    pic_flipped = pygame.transform.flip(pic,True,False)

                    l.append( ( time, (pic,myrect,mask), (pic_flipped,myrect_flipped,mask_flipped) ) )

                setattr(self,modus+extra+'_spiderweb_pictures',l)

    def update(self):
        self.on_ground = False
        self.rect.x += self.speed_x
        if self.on_top_of != None:
            self.rect.x += self.on_top_of.speed_x

        if self.rect.x > -self.rect.width and self.rect.x < utilities.display_width:
            self.rect.y += self.speed_y
            if self.on_top_of != None:
                self.rect.y += self.on_top_of.speed_y

        if self.boundary_check():
            return

        self._collision_detect()
        objects = world.current_world.current_level.rooms[world.current_world.current_level.current_room].objects
        colliding_objects = pygame.sprite.spritecollide(self,objects,False,pygame.sprite.collide_mask)
        for obj in colliding_objects:
            obj.collect(self)

        self.speed_y += self.gravity

        if self.spiderweb_timer != None and \
           utilities.diff_framecount(utilities.framecount,self.spiderweb_timer) > 300:
            self.spiderweb_timer = None

        # elfi-laufanimation
        self._set_picture()

        if not self.on_ground and self.on_top_of != None:
            self.on_top_of.rider = None
            self.on_top_of = None

        # schlag mit dem schirm
        if self.mode == 'umbrella' and hasattr(self,'submode') and self.submode == 'strike':
            length = len(self.umbrella_strike_pictures) if self.speed_x == 0 else len(self.umbrella_strike_walking_pictures)
            if self.picture < length-1:
                bg = world.current_world.current_level.rooms[world.current_world.current_level.current_room].background
                fg = world.current_world.current_level.rooms[world.current_world.current_level.current_room].foreground
                collisions = pygame.sprite.spritecollide(self.rect,bg,False,utilities.collide_myrect) + \
                             pygame.sprite.spritecollide(self.rect,fg,False,utilities.collide_myrect)

                for obj in collisions:
                    if not hasattr(obj,'get_hit'):
                        continue

                    obj_rect = utilities.get_myrect(obj)

                    if not self.flipped:
                        if self.rect.x + self.myrect.x + self.myrect.width > obj_rect.x:
                            continue
                    else:
                        if self.rect.x + self.myrect.x < obj_rect.x + obj_rect.width:
                            continue

                    obj.get_hit(self)


        #elfi trocknen
        self.wet-=0.01
        if self.wet < 0:
            self.wet = 0

        # 100 kekse = 1 leben
        if self.cookiecount >= 100:
            self.lives += 1
            self.cookiecount -= 100

    def _set_picture(self):
        s = self.mode
        if hasattr(self,'submode'):
            s += '_'+self.submode

        if self.speed_x != 0:
            s += '_walking'

        if self.spiderweb_timer != None:
            s += '_spiderweb'

        pictures = getattr(self,s+'_pictures')

        self.picture %= len(pictures)

        if len(pictures[self.picture]) == 2:
            mode,submode = pictures[self.picture]
            self.picture = 0
            self.mode = mode
            self.freeze = False;
            if submode != None:
                self.submode = submode
            elif hasattr(self,'submode'):
                delattr(self,'submode')
            self._set_picture()
            return
        elif utilities.diff_framecount(utilities.framecount,self.timer) > pictures[self.picture][0]:
            self.timer = utilities.framecount
            self.picture += 1
            self.picture %= len(pictures)
            self._set_picture()
            return

        x = 0
        y = 0

        if hasattr(self,'rect') and hasattr(self,'myrect'):
            x = self.rect.x + self.myrect.x
            y = self.rect.y + self.myrect.y + self.myrect.height
            if self.speed_x > 0:
                x += self.myrect.width

        self.image,self.myrect,self.mask = pictures[self.picture][2 if self.flipped else 1]

        self.rect = self.image.get_rect()
        self.rect.x = x - self.myrect.x
        self.rect.y = y - self.myrect.y - self.myrect.height
        if self.speed_x > 0:
            self.rect.x -= self.myrect.width


    def real_speed(self):
        if self.on_top_of == None:
            return self.speed_x,self.speed_y
        else:
            return self.speed_x+self.on_top_of.speed_x,self.speed_y+self.on_top_of.speed_y

    def set_mode(self,mode):
        self.mode = mode
        if hasattr(self,'submode'):
            delattr(self,'submode')
        if hasattr(self,'drops'):
            self.drops.kill()
            delattr(self,'drops')
        self.timer = utilities.framecount
        self.picture = 0
        self.freeze = False
        self._set_picture()

    def boo(self):
        self.speed_x = 0
        self.freeze = True
        if self.mode == 'umbrella' and hasattr(self,'submode') and self.submode == 'swimming':
            self.submode = 'swimming_shocked'
        else:
            self.submode = 'shocked'
        self.timer = utilities.framecount
        self.picture = 0
        self._set_picture()

    def turn(self):
        self.flipped = not self.flipped
        self._set_picture()

    def move(self,key):
        if utilities.diff_framecount(utilities.framecount,self.hoptimer) < 15:
            self.on_ground = True

        if self.freeze:
            return

        if key == "left":
            self.speed_x = -8
            self.flipped = True

        if key == "right":
            self.speed_x = 8
            self.flipped = False

        if key =="jump":
            if self.on_ground == True:
                self.speed_y = -35
                self.on_ground = False

        if self.spiderweb_timer != None:
            self.speed_x = int(self.speed_x*.4)


    def _collision_detect(self):
        collisions = utilities.detect_collisions(self)

        speed_x,speed_y = self.real_speed()

        for tpl in collisions:
            block = tpl[1]

            if not utilities.collide_myrect(self,block):
                continue

            t = tpl[0]
            direction = tpl[2]

            block.collision(direction,t,self,speed_x-block.speed_x,speed_y-block.speed_y)


    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.rect.x -= utilities.round_up(t*speed_x) + utilities.sgn(speed_x)

        if direction == "up" or direction == "down":
            self.rect.y -= utilities.round_up(t*speed_y)
            self.speed_y = 0

        if direction == "up":
            self.on_ground = True


    def get_hit(self):
        if self.mode == 'normal':
            self.is_dead = True
        elif self.mode != 'hurt':
            self.set_mode('hurt')

    def reset(self):
        self.speed_x = 0
        self.speed_y = 0
        self.set_mode('normal')
        self.spiderweb_timer = None

    def boundary_check(self):
        if self.rect.x+self.myrect.x <= -self.myrect.width and self.speed_x<0:
            return world.current_world.current_level.leave_room(self,"left")

        if self.rect.x+self.myrect.x >= utilities.display_width and self.speed_x > 0:
            return world.current_world.current_level.leave_room(self,"right")

        if self.rect.y+self.myrect.y <= 0 and self.speed_y<0:
            return world.current_world.current_level.leave_room(self,"up")

        if self.rect.y+self.myrect.y >= utilities.display_height and self.speed_y > 0:
            return world.current_world.current_level.leave_room(self,"down")

    def get_wet(self,w,swimming=False):
        if self.mode == 'hurt':
            return

        if swimming and self.mode == 'umbrella':
            if hasattr(self,'submode') and self.submode == 'swimming_shocked':
                return

            if hasattr(self,'submode') and self.submode == 'shocked':
                self.submode = 'swimming_shocked'
            else:
                self.submode = 'swimming'

            self.picture = 0
            return

        self.wet+=w
        if self.wet > 100:
            self.wet = 0
            self.get_hit()


    def sit_on(self,sprite):
        self.on_top_of = sprite
        sprite.rider = self

    def action(self,down):
        if self.mode == 'gardener':
            if not hasattr(self,'submode'):
                if not down or not self.on_ground:
                    return

                self.picture = 0
                self.submode = 'watering'
                self.freeze = True

                if self.flipped:
                    self.drops = objects.WaterDrops(self.rect.x-60,self.rect.y + 50, self.flipped)
                else:
                    self.drops = objects.WaterDrops(self.rect.x+self.rect.width+20,self.rect.y + 50,self.flipped)
                room = world.current_world.current_level.rooms[world.current_world.current_level.current_room]
                room.add('objects',self.drops)
            elif self.submode == 'watering':
                if down:
                    return

                self.picture = 0
                delattr(self,'submode')
                self.freeze = False

                if hasattr(self,'drops'):
                    self.drops.kill()
                    delattr(self,'drops')
        elif self.mode == 'umbrella':
            if not hasattr(self,'submode'):
                if not down:
                    return

                self.picture = 0
                self.submode = 'strike'


