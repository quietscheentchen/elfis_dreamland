#
#  friends.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



import pygame
import sprite
import utilities
import math
import random

class Benjamin(sprite.Sprite):
    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image_original = pygame.image.load('graphics/benjamin.png')
        self.image = self.image_original
        self.flipped = False
        self.rect = self.image.get_rect()

        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0
        self.gravity = 2

        self.on_ground = True

    def update(self):
        #if self.rect.x < self.xi:
         #   self.flipped = True
        #if self.rect.x > self.xf:
         #   self.flipped = False

        self.image = pygame.transform.flip(self.image_original,self.flipped,False)

class Murphy(sprite.Sprite):
    def __init__(self,x,y,minx=0,maxx=utilities.display_width):
        sprite.Sprite.__init__(self,x,y,minx,maxx)

        self.walk_pictures = [
            pygame.image.load('graphics/murphy1.png'),
            pygame.image.load('graphics/murphy2.png'),
            pygame.image.load('graphics/murphy1.png'),
            pygame.image.load('graphics/murphy3.png')
        ]

        self.look_pictures = [
            pygame.image.load('graphics/murphy1.png'),
            pygame.image.load('graphics/murphy4.png'),
            pygame.image.load('graphics/murphy6.png'),
            pygame.image.load('graphics/murphy4.png'),
            pygame.image.load('graphics/murphy1.png'),
            pygame.image.load('graphics/murphy5.png'),
            pygame.image.load('graphics/murphy5.png'),
            pygame.image.load('graphics/murphy5.png'),
            pygame.image.load('graphics/murphy1.png')

        ]
        self.picture = 0
        self.look_picture = 0

        self.image = self.walk_pictures[self.picture]
        self.rect = self.image.get_rect()

        self.minx = minx
        self.maxx = maxx

        self.confusion_counter = 0
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 2
        self.speed_y = 0
        self.gravity = 2

        self.rider = None

        self.flipped = False
        self._set_myrect()

        self.timer = utilities.framecount

    def update(self):
        old_x = self.rect.x

        self.speed_y = self.speed_y + self.gravity

        redraw = utilities.diff_framecount(utilities.framecount,self.timer) > 20

        if self.rect.x < self.minx and self.speed_x < 0:
            self._turn()
            redraw = True


        if self.rect.x > self.maxx and self.speed_x > 0:
            self._turn()
            redraw = True


        if redraw:
            if self.confusion_counter < 15:
                self.picture += 1
                self.picture %= len(self.walk_pictures)
                self.image = pygame.transform.flip(self.walk_pictures[self.picture],self.flipped,False)
                self._set_myrect()
                self.timer = utilities.framecount
                self.confusion_counter +=1
            else:
                self.speed_x = 0
                self.look_picture +=1
                if self.look_picture == 8:
                    self.confusion_counter = 0
                    self.look_picture = 0
                    self.picture = 1
                    if self.flipped:
                        self.speed_x = -2
                    else:
                        self.speed_x = 2
                    if random.randint(0,1) == 1:
                        self._turn()

                    self.image = pygame.transform.flip(self.walk_pictures[self.picture],self.flipped,False)
                else:
                    self.image = pygame.transform.flip(self.look_pictures[self.look_picture],self.flipped,False)

                self._set_myrect()
                self.timer = utilities.framecount

        self.rect.y += self.speed_y
        self.rect.x += self.speed_x

        self._collision_detect()

    def _collision_detect(self):
        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        for tpl in collisions:
            block = tpl[1]

            if not utilities.collide_myrect(self,block):
                continue

            t = tpl[0]
            direction = tpl[2]

            block.collision(direction,t,self,speed_x-block.speed_x,speed_y-block.speed_y)

    def _set_myrect(self):
        if not self.flipped:
            self.myrect = pygame.Rect( (35,self.rect.height*.3), (self.rect.width-100, self.rect.height*.7-4) )
        else:
            self.myrect = pygame.Rect( (65,self.rect.height*.3), (self.rect.width-100, self.rect.height*.7-4) )

        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)

    def _turn(self):
        self.speed_x = -self.speed_x

        diff = 0
        if self.rider != None:
            rider_myrect = utilities.get_myrect(self.rider)
            diff = (self.rect.x+self.myrect.x+self.myrect.width/2)-(rider_myrect.x+rider_myrect.width/2)

        self.rect.x += 2*self.myrect.x - self.rect.width + self.myrect.width

        self.flipped = not self.flipped
        self._set_myrect()
        if self.rider != None:
            self.rider.turn()
            self.rider.rect.x += diff + self.rect.x+self.myrect.x+self.myrect.width/2-rider_myrect.x-rider_myrect.width/2


    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.rect.x -= t*speed_x + utilities.sgn(speed_x)
            self._turn()

        if direction == "up" or direction == "down":
            self.rect.y -= t*speed_y
            self.speed_y = 0

    def collision(self,direction,t,player,speed_x,speed_y):
        if (not hasattr(player,'is_player') or not player.is_player) and not hasattr(player,'sit_on'):
            return
        if direction != "up" or t > 1:
            return

        player.block_collision(direction,t,self,speed_x,speed_y)
        player.sit_on(self)


