#
#  world.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



import pygame
import utilities
import elfi
import time

# importiere levels
import levels.level1.level
import levels.level2.level
import levels.level3.level

current_world = None

class World:
    def __init__(self,display):
        self.display = display
        self.current_level = None
        self.players = pygame.sprite.RenderUpdates()
        self.won = False
        self.play_mode = False

        self.black = (0,0,0)
        self.blue = (0,0,255)
        self.darkred = (180,0,0)

        self.life = pygame.image.load('graphics/life.png')
        self.font = pygame.freetype.Font("fonts/LiberationSans-Bold.ttf",32)
        self.gameover_font = pygame.freetype.Font("fonts/LiberationSans-Bold.ttf",200)
        self.clock = pygame.time.Clock()

        self.current_level = None

        # initialisiere Elfi
        self.elfi = elfi.Elfi()

        self.mouse = (0,0)

        # initialisiere karte (das kommt in eine abgeleitete klasse, sobald es mehr als eine welt gibt)
        self.red_marker = pygame.image.load("graphics/levelmarker.png")
        self.blue_marker = pygame.image.load("graphics/levelmarkerblue.png")
        self.yellow_marker = pygame.image.load("graphics/levelmarkeryellow.png")
        self.marker_rect = self.red_marker.get_rect()
        #self.background = pygame.Surface((utilities.display_width,utilities.display_height))
        #self.background.fill((255,255,255))
        self.background = pygame.image.load('graphics/maptiles/world1.png')
        self.active_levels = 3

        self.levels = [
            ((190,800),levels.level1.level.Level1),
            ((468,857),levels.level2.level.Level2),
            ((800,820),levels.level3.level.Level3)
        ]

    def dirty_status(self):
        dirty = False

        if not hasattr(self,'xold_lives') or self.xold_lives != self.elfi.lives:
            dirty = True
        if not hasattr(self,'xold_cookiecount') or self.xold_cookiecount != self.elfi.cookiecount:
            dirty = True
        if not hasattr(self,'xold_wet') or self.xold_wet != self.elfi.wet:
            dirty = True

        if not dirty:
            return False

        self.xold_lives = self.elfi.lives
        self.xold_cookiecount = self.elfi.cookiecount
        self.xold_wet = self.elfi.wet
        return True

    def show_status(self):
        #anzahl leben
        for n in range(self.elfi.lives):
            self.display.blit(self.life,(10+n*32,10))

        #anzahl kekse
        self.font.render_to(self.display,(utilities.display_width-250,10),"Kekse: "+str(self.elfi.cookiecount))

        #nässe
        color = self.blue
        if self.elfi.wet > 80:
            color = self.darkred

        wet = self.elfi.wet
        if wet > 100:
            wet = 100

        if wet > 0:
            pygame.draw.rect(self.display,color,(utilities.display_width/2-100,10,200*wet/100,25))

        pygame.draw.rect(self.display,self.black,(utilities.display_width/2-100,10,200,25),2)

    def play_loop(self):
        self.elfi.speed_x = 0

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "quit"
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                self.elfi.reset()
                return "escape"
            elif event.type == pygame.KEYDOWN and (event.key == pygame.K_SPACE or event.key == pygame.K_UP):
                self.elfi.move("jump")
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_e:
                self.elfi.action(True)
                self.elfi.timer = utilities.framecount
            elif event.type == pygame.KEYUP and event.key == pygame.K_e:
                self.elfi.action(False)

        keys = pygame.key.get_pressed()

        if keys[pygame.K_LEFT]:
            self.elfi.move("left")
        if keys[pygame.K_RIGHT]:
            self.elfi.move("right")

        utilities.framecount +=1

        if self.dirty_status():
            room = self.current_level.rooms[self.current_level.current_room]
            room.render_group.repaint_rect( pygame.Rect( (0,0), (utilities.display_width,40) ) )

        rects = self.current_level.draw(self.display)
        self.show_status()
        pygame.display.update(rects)

        if self.elfi.is_dead:
            self.elfi.wet = 0
            self.elfi.lives -= 1
            self.elfi.is_dead = False
            self.elfi.reset()

            if self.elfi.lives < 0:
                self.gameover_font.render_to(self.display,(100,400),"Game Over",fgcolor=(255,157,46))
                pygame.display.update()
                time.sleep(3)
                return "lost"

            time.sleep(2)
            self.current_level.reset(self.elfi)

        if self.won:
            self.elfi.wet = 0
            self.won = False
            return "won"

        self.current_level.update()
        #self.clock.tick(60) # fps
        self.clock.tick(60) # fps

    def check_pos(self,pos):
        x,y = pos

        for idx,level in enumerate(self.levels):
            marker_x,marker_y = level[0]
            if x >= marker_x and x < marker_x+self.marker_rect.width and \
               y >= marker_y and y < marker_y+self.marker_rect.height:
                return idx
        return None


    def map_click(self,pos):
        idx = self.check_pos(pos)
        if idx == None:
            return
        if idx >= self.active_levels:
            return

        self.current_level = self.levels[idx][1]()
        self.players.add(self.elfi)
        self.current_level.respawn(self.elfi)
        self.play_mode = True

    def map_loop(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "quit"
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                self.map_click(event.pos)
            elif event.type == pygame.MOUSEMOTION:
                self.mouse = event.pos

        self.display.blit(self.background,(0,0))

        mouseover = self.check_pos(self.mouse)

        for idx,level in enumerate(self.levels):
            if idx < self.active_levels:
                marker = self.yellow_marker if idx == mouseover else self.red_marker
            else:
                marker = self.blue_marker

            self.display.blit(marker,level[0])

        pygame.display.update()
        self.clock.tick(60) # fps

    def main_loop(self):
        while True:
            if self.play_mode:
                ret = self.play_loop()
            else:
                ret = self.map_loop()

            if ret == "quit":
                return
            elif ret == "lost":
                print("Game Over!")
                return
            elif ret == "dead" or ret == "escape":
                self.play_mode = False
                self.current_level = None
            elif ret == "won":
                if self.active_levels < len(self.levels) and \
                   self.current_level == self.levels[self.active_levels-1][1]:
                    self.active_levels += 1

                self.play_mode = False
                self.current_level = None

