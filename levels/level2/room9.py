import pygame
import level
import level
import block
import objects
import opponents
import friends
import utilities

class Room9(level.Room):
    def __init__(self):
        level.Room.__init__(self,9)

        self.bgcolor = (216,242,255)

        self.add('background',block.Block(0,964))
        self.add('background',block.Block(60,964))
        self.add('background',block.Block(120,964))
        self.add('background',block.Block(180,964))
        self.add('background',block.Block(240,964))
        self.add('background',block.Block(540,964))
        self.add('background',block.Block(600,964))
        self.add('background',block.Block(660,964))
        self.add('background',block.Block(720,964))
        self.add('background',block.Block(780,964))
        self.add('background',block.Block(840,964))
        self.add('background',block.Block(900,964))
        self.add('background',block.Block(1080,964))
        self.add('background',block.Block(1140,964))
        self.add('background',block.Block(1200,964))
        self.add('background',block.Block(1260,964))
        self.add('background',block.Block(1260,904))
        self.add('background',block.Block(1200,904))
        self.add('background',block.Block(1140,904))
        self.add('background',block.Block(120,904))
        self.add('background',block.Block(60,904))
        self.add('background',block.Block(0,904))
        self.add('background',block.Block(0,844))
        self.add('background',block.Block(60,844))
        self.add('background',block.Block(1200,844))
        self.add('background',block.Block(1260,844))
        self.add('background',block.GrasBlock(0,773))
        self.add('background',block.Block(180,904))
        self.add('background',block.Block(240,904))
        self.add('background',block.Block(540,904))
        self.add('background',block.Block(600,904))
        self.add('background',block.Block(660,904))
        self.add('background',block.Block(720,904))
        self.add('background',block.Block(780,904))
        self.add('background',block.Block(840,904))
        self.add('background',block.Block(120,844))
        self.add('background',block.Block(300,964))
        self.add('background',block.Block(360,964))
        self.add('background',block.Block(420,964))
        self.add('background',block.Block(480,964))
        self.add('background',block.Block(480,904))
        self.add('background',block.Block(420,904))
        self.add('background',block.Block(360,904))
        self.add('background',block.Block(300,904))
        self.add('background',block.Block(960,964))
        self.add('background',block.Block(1020,964))
        self.add('background',block.Block(1080,904))
        self.add('background',block.Block(1020,904))
        self.add('background',block.Block(960,904))
        self.add('background',block.Block(900,904))
        self.add('background',block.Block(1140,844))
        self.add('background',block.Block(180,844))
        self.add('background',block.Block(240,844))
        self.add('background',block.Block(300,844))
        self.add('background',block.Block(360,844))
        self.add('background',block.Block(420,844))
        self.add('background',block.Block(480,844))
        self.add('background',block.Block(540,844))
        self.add('background',block.Block(600,844))
        self.add('background',block.Block(660,844))
        self.add('background',block.Block(720,844))
        self.add('background',block.Block(780,844))
        self.add('background',block.Block(840,844))
        self.add('background',block.Block(900,844))
        self.add('background',block.Block(960,844))
        self.add('background',block.Block(1020,844))
        self.add('background',block.Block(1080,844))
        self.add('background',opponents.Upsi(593,689),True)
        self.add('background',opponents.Upsi(300,686),True)
        self.add('background',block.Box(177,723),True)
        self.add('background',block.Box(1067,723),True)
        self.add('background',block.Bar(406,402,(0,170),(0,402),(0,3)))
        self.add('background',block.Bar(670,170,(0,170),(0,402),(0,3)))
        self.add('background',block.Bar(939,500,(0,140),(0,550),(0,3)))
        self.add('background',block.Bar(143,547,(0,140),(0,550),(0,3)))
        self.add('background',opponents.UpsiOnCloud(100,10,(180, 185, 175)),True)
        self.add('objects',objects.Cookie(204,285),False)
        self.add('objects',objects.Cookie(276,114),False)
        self.add('objects',objects.Cookie(404,44),False)
        self.add('objects',objects.Cookie(568,39),False)
        self.add('objects',objects.Cookie(325,59),False)
        self.add('objects',objects.Cookie(234,186),False)
        self.add('objects',objects.Cookie(641,38),False)
        self.add('objects',objects.Cookie(719,36),False)
        self.add('objects',objects.Cookie(799,36),False)
        self.add('objects',objects.Cookie(867,36),False)
        self.add('objects',objects.Cookie(944,38),False)
        self.add('objects',objects.Cookie(1027,61),False)
        self.add('objects',objects.Cookie(1100,130),False)
        self.add('objects',objects.Cookie(1121,218),False)
        self.add('objects',objects.Cookie(486,36),False)
        self.add('objects',objects.Cookie(1150,315),False)
        self.add('background',opponents.Upsi(908,689,flipped=False),True)
        self.add('background',block.GrasBlock(60,773))
        self.add('background',block.GrasBlock(120,773))
        self.add('background',block.GrasBlock(180,773))
        self.add('background',block.GrasBlock(240,773))
        self.add('background',block.GrasBlock(300,773))
        self.add('background',block.GrasBlock(360,773))
        self.add('background',block.GrasBlock(420,773))
        self.add('background',block.GrasBlock(480,773))
        self.add('background',block.GrasBlock(540,773))
        self.add('background',block.GrasBlock(600,773))
        self.add('background',block.GrasBlock(660,773))
        self.add('background',block.GrasBlock(720,773))
        self.add('background',block.GrasBlock(780,773))
        self.add('background',block.GrasBlock(840,773))
        self.add('background',block.GrasBlock(900,773))
        self.add('background',block.GrasBlock(960,773))
        self.add('background',block.GrasBlock(1020,773))
        self.add('background',block.GrasBlock(1080,773))
        self.add('background',block.GrasBlock(1140,773))
        self.add('background',block.GrasBlock(1200,773))
        self.add('background',block.GrasBlock(1260,773))
