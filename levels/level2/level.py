import level
import levels.level2.room1
import levels.level2.room2
import levels.level2.room3
import levels.level2.room4
import levels.level2.room5
import levels.level2.room6
import levels.level2.room7
import levels.level2.room8
import levels.level2.room9
import levels.level2.room10
import levels.level2.room11
import levels.level2.room101

class Level2(level.Level):
    def __init__(self):
        level.Level.__init__(self)

        # räume
        self.rooms[1]  = levels.level2.room1.Room1()
        self.rooms[2]  = levels.level2.room2.Room2()
        self.rooms[3]  = levels.level2.room3.Room3()
        self.rooms[4]  = levels.level2.room4.Room4()
        self.rooms[5]  = levels.level2.room5.Room5()
        self.rooms[6]  = levels.level2.room6.Room6()
        self.rooms[7]  = levels.level2.room7.Room7()
        self.rooms[8]  = levels.level2.room8.Room8()
        self.rooms[9]  = levels.level2.room9.Room9()
        self.rooms[10] = levels.level2.room10.Room10()
        self.rooms[11] = levels.level2.room11.Room11()

        # geheimräume
        self.rooms[101] = levels.level2.room101.Room101()

        # definiere topologie
        self.rooms[101].right = 1
        self.rooms[1].right = 2
        self.rooms[2].right = 3
        self.rooms[3].right = 4
        self.rooms[4].right = 5
        self.rooms[5].right = 6
        self.rooms[6].right = 7
        self.rooms[7].right = 8
        self.rooms[8].right = 9
        self.rooms[9].right = 10
        self.rooms[10].right = 11

        self.rooms[1].left = 101
        self.rooms[2].left = 1
        self.rooms[3].left = 2
        self.rooms[4].left = 3
        self.rooms[5].left = 4
        self.rooms[6].left = 5
        self.rooms[7].left = 6
        self.rooms[8].left = 7
        self.rooms[9].left = 8
        self.rooms[10].left = 9
        self.rooms[11].left = 10

        # elfis spawn position
        self.spawn_room = 1
        self.spawn_x = 130
        self.spawn_y = 702
        self.spawn_flipped = False

