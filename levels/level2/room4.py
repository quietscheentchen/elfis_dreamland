import pygame
import level
import level
import block
import objects
import opponents
import friends
import utilities

class Room4(level.Room):
    def __init__(self):
        level.Room.__init__(self,4)

        self.bgcolor = (216,242,255)

        self.add('background',block.Block(0,964))
        self.add('background',block.Block(60,964))
        self.add('background',block.Block(1020,964))
        self.add('background',block.Block(1080,964))
        self.add('background',block.Block(1140,964))
        self.add('background',block.Block(1200,964))
        self.add('background',block.Block(1260,964))
        self.add('background',block.Block(1260,904))
        self.add('background',block.Block(1200,904))
        self.add('background',block.Block(1140,904))
        self.add('background',block.Block(1080,904))
        self.add('background',block.Block(1020,904))
        self.add('background',block.Block(60,904))
        self.add('background',block.Block(0,904))
        self.add('background',block.Block(0,844))
        self.add('background',block.Block(60,844))
        self.add('background',block.Block(1020,844))
        self.add('background',block.Block(1080,844))
        self.add('background',block.Block(1140,844))
        self.add('background',block.Block(1200,844))
        self.add('background',block.Block(1260,844))
        self.add('background',block.GrasBlock(1139,773))
        self.add('background',block.GrasBlock(1080,773))
        self.add('background',block.GrasBlock(1020,773))
        self.add('background',block.GrasBlock(1199,773))
        self.add('background',block.GrasBlock(1259,773))
        self.add('background',block.Gras(-60,799))
        self.add('background',block.GrasBlock(60,773))
        self.add('background',block.GrasBlock(0,773))
        self.add('bgdeco',block.Cloud(949,523))
        self.add('bgdeco',block.Cloud(207,214))
        self.add('bgdeco',block.Cloud(81,229))
        self.add('bgdeco',block.Cloud(130,277))
        self.add('bgdeco',block.Cloud(682,376))
        self.add('background',block.Block(120,964))
        self.add('background',block.Block(180,964))
        self.add('background',block.Block(240,964))
        self.add('background',block.Block(300,964))
        self.add('background',block.Block(360,964))
        self.add('background',block.Block(420,964))
        self.add('background',block.Block(480,964))
        self.add('background',block.Block(540,964))
        self.add('background',block.Block(600,964))
        self.add('background',block.Block(660,964))
        self.add('background',block.Block(720,964))
        self.add('background',block.Block(780,964))
        self.add('background',block.Block(840,964))
        self.add('background',block.Block(900,964))
        self.add('background',block.Block(960,964))
        self.add('background',block.Block(960,904))
        self.add('background',block.Block(900,904))
        self.add('background',block.Block(840,904))
        self.add('background',block.Block(780,904))
        self.add('background',block.Block(720,904))
        self.add('background',block.Block(660,904))
        self.add('background',block.Block(600,904))
        self.add('background',block.Block(540,904))
        self.add('background',block.Block(480,904))
        self.add('background',block.Block(420,904))
        self.add('background',block.Block(360,904))
        self.add('background',block.Block(300,904))
        self.add('background',block.Block(240,904))
        self.add('background',block.Block(180,904))
        self.add('background',block.Block(120,904))
        self.add('background',block.Block(120,844))
        self.add('background',block.Block(180,844))
        self.add('background',block.Block(240,844))
        self.add('background',block.Block(300,844))
        self.add('background',block.Block(360,844))
        self.add('background',block.Block(420,844))
        self.add('background',block.Block(480,844))
        self.add('background',block.Block(540,844))
        self.add('background',block.Block(600,844))
        self.add('background',block.Block(660,844))
        self.add('background',block.Block(720,844))
        self.add('background',block.Block(780,844))
        self.add('background',block.Block(840,844))
        self.add('background',block.Block(900,844))
        self.add('background',block.Block(960,844))
        self.add('background',block.GrasBlock(960,773))
        self.add('background',block.GrasBlock(900,773))
        self.add('background',block.GrasBlock(840,773))
        self.add('background',block.GrasBlock(780,773))
        self.add('background',block.GrasBlock(720,773))
        self.add('background',block.GrasBlock(660,773))
        self.add('background',block.GrasBlock(600,773))
        self.add('background',block.GrasBlock(540,773))
        self.add('background',block.GrasBlock(480,773))
        self.add('background',block.GrasBlock(420,773))
        self.add('background',block.GrasBlock(360,773))
        self.add('background',block.GrasBlock(300,773))
        self.add('background',block.GrasBlock(240,773))
        self.add('background',block.GrasBlock(180,773))
        self.add('background',block.GrasBlock(120,773))
        self.add('background',block.Trampoline(330,757),True)
        self.add('background',block.Trampoline(975,758),True)
        self.add('background',objects.Cookieflower(1091,586,3),False)
        self.add('bgdeco',block.DecoFlower(613,587))
        self.add('background',opponents.EvilFlower(150,587),True)
        self.add('background',opponents.UpsiOnCloud(1001,195,(180, 185, 175)),True)
        self.add('background',block.Gras(675,325))
        self.add('objects',objects.Cookie(681,253),False)
        self.add('background',opponents.Mouse(450,747),True)
        self.add('background',opponents.Mouse(835,745,flipped=False),True)
        self.add('objects',objects.Cookie(487,294),False)
        self.add('objects',objects.Cookie(374,403),False)
        self.add('objects',objects.Cookie(344,594),False)
        self.add('objects',objects.Cookie(838,299),False)
        self.add('objects',objects.Cookie(950,400),False)
        self.add('objects',objects.Cookie(986,590),False)
        self.add('bgdeco',block.Cloud(316,93))
        self.add('bgdeco',block.Cloud(208,114))
        self.add('fgdeco',block.Cloud(409,285))
        self.add('fgdeco',block.Cloud(348,334))
        self.add('bgdeco',block.Cloud(343,183))
        self.add('bgdeco',block.Cloud(536,145))
        self.add('bgdeco',block.Cloud(241,-18))
        self.add('bgdeco',block.Cloud(103,12))
        self.add('bgdeco',block.Cloud(49,69))
        self.add('bgdeco',block.Cloud(102,126))
        self.add('bgdeco',block.Cloud(52,316))
        self.add('bgdeco',block.Cloud(-3,167))
        self.add('bgdeco',block.Cloud(13,9))
        self.add('bgdeco',block.Cloud(443,204))
        self.add('bgdeco',block.Cloud(575,27))
        self.add('bgdeco',block.Cloud(663,68))
        self.add('bgdeco',block.Cloud(763,159))
        self.add('bgdeco',block.Cloud(766,-20))
        self.add('bgdeco',block.Cloud(929,179))
        self.add('bgdeco',block.Cloud(863,46))
        self.add('bgdeco',block.Cloud(1060,75))
        self.add('bgdeco',block.Cloud(1201,204))
        self.add('bgdeco',block.Cloud(1200,293))
