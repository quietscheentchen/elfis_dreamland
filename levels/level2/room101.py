import pygame
import level
import level
import block
import objects
import opponents
import friends
import utilities

class Room101(level.Room):
    def __init__(self):
        level.Room.__init__(self,101)

        self.bgcolor = (216,242,255)

        self.add('background',block.Block(0,964))
        self.add('background',block.Block(60,964))
        self.add('background',block.Block(120,964))
        self.add('background',block.Block(180,964))
        self.add('background',block.Block(240,964))
        self.add('background',block.Block(300,964))
        self.add('background',block.Block(360,964))
        self.add('background',block.Block(420,964))
        self.add('background',block.Block(480,964))
        self.add('background',block.Block(540,964))
        self.add('background',block.Block(600,964))
        self.add('background',block.Block(660,964))
        self.add('background',block.Block(720,964))
        self.add('background',block.Block(780,964))
        self.add('background',block.Block(840,964))
        self.add('background',block.Block(900,964))
        self.add('background',block.Block(960,964))
        self.add('background',block.Block(1020,964))
        self.add('background',block.Block(1080,964))
        self.add('background',block.Block(1140,964))
        self.add('background',block.Block(1200,964))
        self.add('background',block.Block(1260,964))
        self.add('background',block.Block(1260,904))
        self.add('background',block.Block(1200,904))
        self.add('background',block.Block(1140,904))
        self.add('background',block.Block(1080,904))
        self.add('background',block.Block(1020,904))
        self.add('background',block.Block(960,904))
        self.add('background',block.Block(900,904))
        self.add('background',block.Block(840,904))
        self.add('background',block.Block(780,904))
        self.add('background',block.Block(720,904))
        self.add('background',block.Block(660,904))
        self.add('background',block.Block(600,904))
        self.add('background',block.Block(540,904))
        self.add('background',block.Block(480,904))
        self.add('background',block.Block(420,904))
        self.add('background',block.Block(360,904))
        self.add('background',block.Block(300,904))
        self.add('background',block.Block(240,904))
        self.add('background',block.Block(180,904))
        self.add('background',block.Block(120,904))
        self.add('background',block.Block(60,904))
        self.add('background',block.Block(0,904))
        self.add('background',block.Block(0,844))
        self.add('background',block.Block(60,844))
        self.add('background',block.Block(120,844))
        self.add('background',block.Block(180,844))
        self.add('background',block.Block(240,844))
        self.add('background',block.Block(300,844))
        self.add('background',block.Block(360,844))
        self.add('background',block.Block(420,844))
        self.add('background',block.Block(480,844))
        self.add('background',block.Block(540,844))
        self.add('background',block.Block(600,844))
        self.add('background',block.Block(660,844))
        self.add('background',block.Block(720,844))
        self.add('background',block.Block(780,844))
        self.add('background',block.Block(840,844))
        self.add('background',block.Block(900,844))
        self.add('background',block.Block(960,844))
        self.add('background',block.Block(1020,844))
        self.add('background',block.Block(1080,844))
        self.add('background',block.Block(1140,844))
        self.add('background',block.Block(1200,844))
        self.add('background',block.Block(1260,844))
        self.add('background',block.GrasBlock(1139,773))
        self.add('background',block.GrasBlock(1080,773))
        self.add('background',block.GrasBlock(1020,773))
        self.add('background',block.GrasBlock(960,773))
        self.add('background',block.GrasBlock(900,773))
        self.add('background',block.GrasBlock(840,773))
        self.add('background',block.GrasBlock(780,773))
        self.add('background',block.GrasBlock(720,773))
        self.add('background',block.GrasBlock(660,773))
        self.add('background',block.GrasBlock(600,773))
        self.add('background',block.GrasBlock(540,773))
        self.add('background',block.GrasBlock(480,773))
        self.add('background',block.GrasBlock(420,773))
        self.add('background',block.GrasBlock(360,773))
        self.add('background',block.GrasBlock(300,773))
        self.add('background',block.GrasBlock(240,773))
        self.add('background',block.GrasBlock(180,773))
        self.add('background',block.GrasBlock(120,773))
        self.add('background',block.Block(60,784))
        self.add('background',block.Block(0,784))
        self.add('background',block.Block(0,724))
        self.add('background',block.Block(0,664))
        self.add('background',block.Block(0,604))
        self.add('background',block.Block(0,544))
        self.add('background',block.Block(0,484))
        self.add('background',block.Block(0,424))
        self.add('background',block.Block(0,364))
        self.add('background',block.Block(0,304))
        self.add('background',block.Block(0,244))
        self.add('background',block.Block(0,184))
        self.add('background',block.Block(0,124))
        self.add('background',block.Block(0,64))
        self.add('background',block.Block(0,4))
        self.add('background',block.Block(0,-56))
        self.add('background',block.Block(60,-56))
        self.add('background',block.Block(60,4))
        self.add('background',block.Block(60,64))
        self.add('background',block.Block(60,124))
        self.add('background',block.Block(60,184))
        self.add('background',block.Block(60,244))
        self.add('background',block.Block(60,304))
        self.add('background',block.Block(60,364))
        self.add('background',block.Block(60,424))
        self.add('background',block.Block(60,484))
        self.add('background',block.Block(60,544))
        self.add('background',block.Block(60,604))
        self.add('background',block.Block(60,664))
        self.add('background',block.Block(60,724))
        self.add('background',block.Trampoline(121,758),True)
        self.add('background',block.Trampoline(188,758),True)
        self.add('background',block.Trampoline(255,758),True)
        self.add('background',block.Trampoline(322,758),True)
        self.add('background',block.Trampoline(389,758),True)
        self.add('background',block.Trampoline(456,758),True)
        self.add('background',block.Trampoline(523,758),True)
        self.add('background',block.Trampoline(590,758),True)
        self.add('background',block.Trampoline(657,758),True)
        self.add('background',block.Trampoline(724,758),True)
        self.add('background',block.Trampoline(791,758),True)
        self.add('background',block.Trampoline(858,758),True)
        self.add('background',block.Trampoline(925,758),True)
        self.add('background',block.Trampoline(992,758),True)
        self.add('background',block.Trampoline(1059,758),True)
        self.add('background',block.Trampoline(1126,758),True)
        self.add('background',block.Block(1199,785))
        self.add('background',block.Block(1259,785))
        self.add('background',block.Block(1259,725))
        self.add('background',block.Block(1259,665))
        self.add('background',block.Block(1259,605))
        self.add('background',block.Block(1259,545))
        self.add('background',block.Block(1259,485))
        self.add('background',block.Block(1259,425))
        self.add('background',block.Block(1259,365))
        self.add('background',block.Block(1259,305))
        self.add('background',block.Block(1259,245))
        self.add('background',block.Block(1199,245))
        self.add('background',block.Block(1199,305))
        self.add('background',block.Block(1199,365))
        self.add('background',block.Block(1199,425))
        self.add('background',block.Block(1199,485))
        self.add('background',block.Block(1199,545))
        self.add('background',block.Block(1199,605))
        self.add('background',block.Block(1199,665))
        self.add('background',block.Block(1199,725))
        self.add('objects',objects.Cookie(150,400),False)
        self.add('objects',objects.Cookie(150,200),False)
        self.add('objects',objects.Cookie(150,300),False)
        self.add('objects',objects.Cookie(150,600),False)
        self.add('objects',objects.Cookie(150,500),False)
        self.add('objects',objects.Cookie(150,100),False)
        self.add('objects',objects.Cookie(250,200),False)
        self.add('objects',objects.Cookie(250,300),False)
        self.add('objects',objects.Cookie(250,600),False)
        self.add('objects',objects.Cookie(250,400),False)
        self.add('objects',objects.Cookie(250,500),False)
        self.add('objects',objects.Cookie(250,100),False)
        self.add('objects',objects.Cookie(350,100),False)
        self.add('objects',objects.Cookie(350,200),False)
        self.add('objects',objects.Cookie(350,300),False)
        self.add('objects',objects.Cookie(350,400),False)
        self.add('objects',objects.Cookie(350,500),False)
        self.add('objects',objects.Cookie(350,600),False)
        self.add('objects',objects.Cookie(450,100),False)
        self.add('objects',objects.Cookie(450,200),False)
        self.add('objects',objects.Cookie(450,300),False)
        self.add('objects',objects.Cookie(450,400),False)
        self.add('objects',objects.Cookie(450,500),False)
        self.add('objects',objects.Cookie(450,600),False)
        self.add('objects',objects.Cookie(550,100),False)
        self.add('objects',objects.Cookie(550,200),False)
        self.add('objects',objects.Cookie(550,300),False)
        self.add('objects',objects.Cookie(550,400),False)
        self.add('objects',objects.Cookie(550,500),False)
        self.add('objects',objects.Cookie(550,600),False)
        self.add('objects',objects.Cookie(650,100),False)
        self.add('objects',objects.Cookie(650,200),False)
        self.add('objects',objects.Cookie(650,300),False)
        self.add('objects',objects.Cookie(650,400),False)
        self.add('objects',objects.Cookie(650,500),False)
        self.add('objects',objects.Cookie(650,600),False)
        self.add('objects',objects.Cookie(750,100),False)
        self.add('objects',objects.Cookie(750,200),False)
        self.add('objects',objects.Cookie(750,300),False)
        self.add('objects',objects.Cookie(750,400),False)
        self.add('objects',objects.Cookie(750,500),False)
        self.add('objects',objects.Cookie(750,600),False)
        self.add('objects',objects.Cookie(850,100),False)
        self.add('objects',objects.Cookie(850,200),False)
        self.add('objects',objects.Cookie(850,300),False)
        self.add('objects',objects.Cookie(850,400),False)
        self.add('objects',objects.Cookie(850,500),False)
        self.add('objects',objects.Cookie(850,600),False)
        self.add('objects',objects.Cookie(950,100),False)
        self.add('objects',objects.Cookie(950,200),False)
        self.add('objects',objects.Cookie(950,300),False)
        self.add('objects',objects.Cookie(950,400),False)
        self.add('objects',objects.Cookie(950,500),False)
        self.add('objects',objects.Cookie(950,600),False)
        self.add('objects',objects.Cookie(1050,100),False)
        self.add('objects',objects.Cookie(1050,200),False)
        self.add('objects',objects.Cookie(1050,300),False)
        self.add('objects',objects.Cookie(1050,600),False)
        self.add('objects',objects.Cookie(1050,400),False)
        self.add('objects',objects.Cookie(1050,500),False)
        self.add('objects',objects.WaterCan(602,350),False)
