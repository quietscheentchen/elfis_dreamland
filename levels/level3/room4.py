import pygame
import level
import level
import block
import objects
import opponents
import friends
import utilities

class Room4(level.Room):
    def __init__(self):
        level.Room.__init__(self,4)

        self.bgcolor = (216,242,255)

        self.add('background',block.Block(0,964))
        self.add('background',block.Block(60,964))
        self.add('background',block.Block(120,964))
        self.add('background',block.Block(1140,964))
        self.add('background',block.Block(1200,964))
        self.add('background',block.Block(1260,964))
        self.add('background',block.Block(1260,904))
        self.add('background',block.Block(1200,904))
        self.add('background',block.Block(1140,904))
        self.add('background',block.Block(120,904))
        self.add('background',block.Block(60,904))
        self.add('background',block.Block(0,904))
        self.add('background',block.Block(0,844))
        self.add('background',block.Block(60,844))
        self.add('background',block.Block(120,844))
        self.add('background',block.Block(1200,844))
        self.add('background',block.Block(1260,844))
        self.add('background',block.GrasBlock(120,773))
        self.add('background',block.Block(1140,844))
        self.add('background',block.GrasBlock(1140,773))
        self.add('background',block.GrasBlock(1200,773))
        self.add('background',block.GrasBlock(1260,773))
        self.add('background',block.GrasBlock(60,773))
        self.add('background',block.GrasBlock(0,773))
        self.add('fgdeco',block.Tree(31,328))
        self.add('fgdeco',block.Cloud(188,896))
        self.add('fgdeco',block.Cloud(107,950))
        self.add('fgdeco',block.Cloud(238,944))
        self.add('fgdeco',block.Cloud(339,909))
        self.add('fgdeco',block.Cloud(360,948))
        self.add('fgdeco',block.Cloud(428,923))
        self.add('fgdeco',block.Cloud(533,955))
        self.add('fgdeco',block.Cloud(534,914))
        self.add('fgdeco',block.Cloud(722,911))
        self.add('fgdeco',block.Cloud(1043,949))
        self.add('fgdeco',block.Cloud(941,949))
        self.add('fgdeco',block.Cloud(820,949))
        self.add('fgdeco',block.Cloud(658,960))
        self.add('fgdeco',block.Cloud(919,898))
        self.add('fgdeco',block.Cloud(1058,914))
        self.add('foreground',block.Branch(386,793,False))
        self.add('background',block.Bar(861,507,(861,165),(861,810),(0,-3)))
        self.add('background',block.Gras(769,195))
        self.add('background',block.Gras(587,195))
        self.add('background',block.Gras(409,195))
        self.add('background',block.Gras(229,195))
        self.add('background',block.Gras(49,195))
        self.add('objects',objects.Cookie(685,34),False)
        self.add('objects',objects.Cookie(505,34),False)
        self.add('objects',objects.Cookie(315,34),False)
        self.add('objects',objects.Cookie(145,34),False)
        self.add('objects',objects.Cookie(55,145),False)
        self.add('objects',objects.Cookie(235,145),False)
        self.add('objects',objects.Cookie(415,145),False)
        self.add('objects',objects.Cookie(593,145),False)
        self.add('objects',objects.Cookie(775,145),False)
        self.add('background',opponents.Moskito(307,134,minx=15,maxx=800,miny=30,maxy=330),True)
        self.add('fgdeco',block.Cloud(664,27))
        self.add('fgdeco',block.Cloud(209,184))
        self.add('bgdeco',block.Cloud(1047,111))
        self.add('fgdeco',block.Cloud(31,22))
        self.add('bgdeco',block.DecoFlower(1168,584))
        self.add('background',opponents.Moskito(629,243,flipped=True,minx=15,maxx=800,miny=30,maxy=330),True)
        self.add('objects',objects.Cookie(865,34),False)
        self.add('objects',objects.Cookie(955,145),False)
        self.add('objects',objects.Cookie(865,256),False)
        self.add('objects',objects.Cookie(955,367),False)
        self.add('objects',objects.Cookie(865,478),False)
        self.add('objects',objects.Cookie(955,589),False)
        self.add('objects',objects.Cookie(865,700),False)
        self.add('objects',objects.Cookie(1045,700),False)
        self.add('objects',objects.Cookie(1045,478),False)
        self.add('objects',objects.Cookie(1045,256),False)
        self.add('objects',objects.Cookie(1045,34),False)
        self.add('foreground',opponents.DancingUpsi(948,406))
