import level
import levels.level3.room1
import levels.level3.room2
import levels.level3.room3
import levels.level3.room4

class Level3(level.Level):
    def __init__(self):
        level.Level.__init__(self)

        # räume
        self.rooms[1]  = levels.level3.room1.Room1()
        self.rooms[2]  = levels.level3.room2.Room2()
        self.rooms[3]  = levels.level3.room3.Room3()
        self.rooms[4]  = levels.level3.room4.Room4()

        # elfis spawn position
        self.spawn_room = 3
        self.spawn_x = 130
        self.spawn_y = 702
        self.spawn_flipped = False

        # definiere topologie

        self.rooms[1].right = 2
        self.rooms[2].right = 3
        self.rooms[3].right = 4

        self.rooms[2].left = 1
        self.rooms[3].left = 2
        self.rooms[4].left = 3

