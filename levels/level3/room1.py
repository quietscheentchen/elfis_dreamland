import pygame
import level
import level
import block
import objects
import opponents
import friends
import utilities

class Room1(level.Room):
    def __init__(self):
        level.Room.__init__(self,1)

        self.bgcolor = (216,242,255)

        self.add('background',block.Block(0,964))
        self.add('background',block.Block(60,964))
        self.add('background',block.Block(120,964))
        self.add('background',block.Block(180,964))
        self.add('background',block.Block(240,964))
        self.add('background',block.Block(300,964))
        self.add('background',block.Block(360,964))
        self.add('background',block.Block(420,964))
        self.add('background',block.Block(480,964))
        self.add('background',block.Block(540,964))
        self.add('background',block.Block(600,964))
        self.add('background',block.Block(660,964))
        self.add('background',block.Block(720,964))
        self.add('background',block.Block(780,964))
        self.add('background',block.Block(840,964))
        self.add('background',block.Block(900,964))
        self.add('background',block.Block(960,964))
        self.add('background',block.Block(1020,964))
        self.add('background',block.Block(1080,964))
        self.add('background',block.Block(1140,964))
        self.add('background',block.Block(1200,964))
        self.add('background',block.Block(1260,964))
        self.add('background',block.Block(1260,904))
        self.add('background',block.Block(1200,904))
        self.add('background',block.Block(1140,904))
        self.add('background',block.Block(1080,904))
        self.add('background',block.Block(1020,904))
        self.add('background',block.Block(960,904))
        self.add('background',block.Block(900,904))
        self.add('background',block.Block(840,904))
        self.add('background',block.Block(780,904))
        self.add('background',block.Block(720,904))
        self.add('background',block.Block(660,904))
        self.add('background',block.Block(600,904))
        self.add('background',block.Block(540,904))
        self.add('background',block.Block(480,904))
        self.add('background',block.Block(420,904))
        self.add('background',block.Block(360,904))
        self.add('background',block.Block(300,904))
        self.add('background',block.Block(240,904))
        self.add('background',block.Block(180,904))
        self.add('background',block.Block(120,904))
        self.add('background',block.Block(60,904))
        self.add('background',block.Block(0,904))
        self.add('background',block.Block(0,844))
        self.add('background',block.Block(60,844))
        self.add('background',block.Block(120,844))
        self.add('background',block.Block(180,844))
        self.add('background',block.Block(240,844))
        self.add('background',block.Block(300,844))
        self.add('background',block.Block(420,844))
        self.add('background',block.Block(480,844))
        self.add('background',block.Block(540,844))
        self.add('background',block.Block(600,844))
        self.add('background',block.Block(660,844))
        self.add('background',block.Block(720,844))
        self.add('background',block.Block(780,844))
        self.add('background',block.Block(840,844))
        self.add('background',block.Block(900,844))
        self.add('background',block.Block(960,844))
        self.add('background',block.Block(1020,844))
        self.add('background',block.Block(1080,844))
        self.add('background',block.Block(1200,844))
        self.add('background',block.Block(1260,844))
        self.add('background',block.GrasBlock(1080,773))
        self.add('background',block.GrasBlock(1020,773))
        self.add('background',block.GrasBlock(960,773))
        self.add('background',block.GrasBlock(300,773))
        self.add('background',block.GrasBlock(240,773))
        self.add('background',block.GrasBlock(180,773))
        self.add('background',block.GrasBlock(120,773))
        self.add('background',block.Block(1140,844))
        self.add('background',block.GrasBlock(1140,773))
        self.add('background',block.GrasBlock(1200,773))
        self.add('background',block.GrasBlock(1260,773))
        self.add('background',block.GrasBlock(60,773))
        self.add('background',block.GrasBlock(0,773))
        self.add('bgdeco',block.Tree(335,124))
        self.add('background',block.GrasBlock(360,773))
        self.add('background',block.GrasBlock(420,773))
        self.add('background',block.GrasBlock(480,773))
        self.add('background',block.GrasBlock(540,773))
        self.add('background',block.GrasBlock(600,773))
        self.add('background',block.GrasBlock(660,773))
        self.add('background',block.GrasBlock(720,773))
        self.add('background',block.GrasBlock(780,773))
        self.add('background',block.GrasBlock(840,773))
        self.add('background',block.GrasBlock(900,773))
        self.add('background',block.Block(360,844))
        self.add('background',opponents.Bug(252,752),True)
        self.add('background',opponents.Bug(1027,752,flipped=True),True)
        self.add('objects',objects.Umbrella(1153,567),False)
        self.add('background',friends.Murphy(318,667,minx=200,maxx=800),True)
        self.add('objects',objects.Cookie(379,609,gravity=3),False)
        self.add('background',opponents.Adelheid(762,271),True)
        self.add('background',block.Trampoline(1098,758),True)
        self.add('background',block.Branch(686,496,False))
        self.add('background',block.Branch(396,559,True))
        self.add('background',opponents.Moskito(611,478,minx=400,maxx=800,miny=300,maxy=600),True)
