import pygame
import random
import level
import block
import objects
import opponents
import friends
import utilities

class Room7(level.Room):
    def __init__(self):
        level.Room.__init__(self,7)

        # ein paar wölkchen für den hintergrund
        number_of_clouds = random.randint(0,10)
        for n in range (0,number_of_clouds):
            pos1 = random.randint(0,utilities.display_width)
            pos2 = random.randint(30,utilities.display_height*0.5)
            self.bgdeco.add(block.Clouds(pos1-20,pos2))

        ground=utilities.display_height*0.8

        # zeichne boden
        for i in range(0,22):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        # was zum drauf stehen
        for i in range(7,17):
            self.background.add(block.Block(i*60,500))
            self.background.add(block.Gras(i*60,485))

        # cookies :)
        for i in range(8,16):
            self.objects.add(objects.Cookie(i*60,350))

        # eine dekoblume :)
        self.fgdeco.add(block.Decoflower(1000,utilities.display_height-370))

        # eine cookieblume :)
        self.background.add(objects.Cookieflower(200,utilities.display_height-370,3))

        # elne böse blume :(
        self.background.add(opponents.EvilFlower(600,utilities.display_height-370))

        # der murphy :)
        self.foreground.add(friends.Murphy(400,723,200,600))



