import pygame
import random
import level
import block
import objects
import opponents
import utilities

class Room1(level.Room):
    def __init__(self):
        level.Room.__init__(self,1)

        # ein paar wölkchen für den hintergrund
        number_of_clouds = random.randint(0,10)
        for n in range (0,number_of_clouds):
            pos1 = random.randint(0,utilities.display_width)
            pos2 = random.randint(30,utilities.display_height*0.5)
            self.bgdeco.add(block.Clouds(pos1-20,pos2))

        # zeichne boden
        ground=utilities.display_height*0.8

        for i in range(0,3):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        for i in range(5,22):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        # wand links
        for i in range(0,15):
            self.background.add(block.Block(0,i*60))

        self.background.add(block.Box(300,100))

        # und ein paar blöcke zum drauf hüpfen
        self.background.add(block.GrasBlock(800,650))
        self.background.add(block.GrasBlock(500,450))

        self.background.add(block.GrasBlock(300,669))
        self.background.add(block.Block(300,740))
        self.background.add(block.Block(300,800))

        self.background.add(block.Box(420,800))

        for i in range(800,300,-60):
            self.background.add(block.Block(1000,i))

        self.background.add(block.Trampoline(1120,820))

        #self.background.add(block.Block(1000,740))
        #self.background.add(block.Gras(1000,730))

        # schwebendes gras
        self.background.add(block.Gras(900,580))

        # und die cookies
        self.objects.add(objects.Cookie(800,500))
        self.objects.add(objects.Cookie(500,200))
        self.objects.add(objects.Cookie(400,ground-80))

        upsi = opponents.Upsi(600,ground-90)

        self.background.add(upsi)

