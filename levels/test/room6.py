import pygame
import random
import level
import block
import objects
import opponents
import friends
import utilities

class Room6(level.Room):
    def __init__(self):
        level.Room.__init__(self,6)

        # ein paar wölkchen für den hintergrund
        number_of_clouds = random.randint(0,10)
        for n in range (0,number_of_clouds):
            pos1 = random.randint(0,utilities.display_width)
            pos2 = random.randint(30,utilities.display_height*0.5)
            self.bgdeco.add(block.Clouds(pos1-20,pos2))

        ground=utilities.display_height*0.8

        # zeichne volle bodenreihe
        for i in range(0,22):
            j=utilities.display_height-60
            while j > (ground+60):
                self.background.add(block.Block(i*60,j))
                j=j-60



        # zeichne boden
        for i in range(0,4):
            j=utilities.display_height-180
            self.background.add(block.Block(i*60,j))


            # und gras obendrauf
            self.background.add(block.Gras(i*60,j-10))

        # zeichne Wasser
        for i in range(4,15):
            j=utilities.display_height-180
            self.foreground.add(block.Water(i*60,j))

        # noch mehr boden
        for i in range(15,22):
            j=utilities.display_height-180
            self.background.add(block.Block(i*60,j))


            # und gras obendrauf
            self.background.add(block.Gras(i*60,j-10))

            # und gras obendrauf
            #self.background.add(block.Gras(i*60,j+50))


        #frei schwebender block
        self.background.add(block.Block(700,650))
        self.background.add(block.Gras(700,635))

        self.background.add(block.Block(400,650))
        self.background.add(block.Gras(400,635))



        self.background.add(block.Box(120,800))



