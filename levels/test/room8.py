import pygame
import random
import level
import block
import objects
import opponents
import friends
import utilities

class Room8(level.Room):
    def __init__(self):
        level.Room.__init__(self,8)

        # ein paar wölkchen für den hintergrund
        number_of_clouds = random.randint(0,10)
        for n in range (0,number_of_clouds):
            pos1 = random.randint(0,utilities.display_width)
            pos2 = random.randint(30,utilities.display_height*0.5)
            self.bgdeco.add(block.Clouds(pos1-20,pos2))

        ground=utilities.display_height*0.8

        # zeichne boden
        for i in range(0,5):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        for i in range(17,22):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        for i in range(0,15):
            self.background.add(block.Block(i*60,300))
            self.background.add(block.Gras(i*60,285))

        for i in range(1,14):
            self.objects.add(objects.Cookie(i*60,200))


        self.background.add(block.Bar(500,ground+20,(320,0),(1000,0),(3,0)))
        self.background.add(block.Bar(1000,500,(0,300),(0,ground-200),(0,-3)))

        # und ein schirm
        self.objects.add(objects.Umbrella(600,ground-50))
