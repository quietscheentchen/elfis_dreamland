import pygame
import random
import level
import block
import objects
import opponents
import friends
import utilities

class Room5(level.Room):
    def __init__(self):
        level.Room.__init__(self,5)

        for i in range(0,18):
            for j in range(0,5):
                self.background.add(block.Block(j*60,i*60))
            for j in range(7,22):
                self.background.add(block.Block(j*60,i*60))

        for i in range(0,16):
            self.objects.add(objects.Cookie(300,i*60))
            self.objects.add(objects.Cookie(360,i*60))

        # teleport zu raum 4
        self.background.add(block.Teleport(300,utilities.display_height-50,120,50,4,1100,100))

