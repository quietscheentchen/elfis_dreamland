import pygame
import random
import level
import block
import objects
import opponents
import friends
import utilities

class Room2(level.Room):
    def __init__(self):
        level.Room.__init__(self,2)

        # ein paar wölkchen für den hintergrund
        number_of_clouds = random.randint(0,10)
        for n in range (0,number_of_clouds):
            pos1 = random.randint(0,utilities.display_width)
            pos2 = random.randint(30,utilities.display_height*0.5)
            self.bgdeco.add(block.Clouds(pos1-20,pos2))

        # zeichne boden
        ground=utilities.display_height*0.8

        for i in range(0,2):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        for i in range(5,22):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        for i in range(800,300,-60):
            self.background.add(block.Block(500,i))

        self.background.add(block.Trampoline(620,820))
        self.background.add(block.Trampoline(380,820))

        self.background.add(block.Block(1000,800))
        self.background.add(block.Gras(1000,785))
        self.background.add(opponents.Upsi(700,ground-90))



