import pygame
import random
import level
import block
import objects
import opponents
import friends
import utilities

class Room4(level.Room):
    def __init__(self):
        level.Room.__init__(self,4)

        # ein paar wölkchen für den hintergrund
        number_of_clouds = random.randint(0,10)
        for n in range (0,number_of_clouds):
            pos1 = random.randint(0,utilities.display_width)
            pos2 = random.randint(30,utilities.display_height*0.5)
            self.bgdeco.add(block.Clouds(pos1-20,pos2))

        ground=utilities.display_height*0.8

        # zeichne boden
        for i in range(0,5):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        # zeichne boden
        for i in range(7,13):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        # zeichne boden
        for i in range(15,22):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        self.background.add(opponents.Upsi(600,ground-90,410,720))

        # wande rechts
        for i in range(0,15):
            self.background.add(block.Block(utilities.display_width-60,i*60))

        # zwischendecke
        for i in range(0,17):
            self.background.add(block.Block(i*60,300))
            self.background.add(block.Gras(i*60,285))

        self.background.add(block.Trampoline(950,820))

        # bett für die elfi
        self.background.add(block.Bed(100,210,True))

        # teleport zu raum 5
        self.background.add(block.Teleport(300,utilities.display_height-50,120,50,5,300,0))

