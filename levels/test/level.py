import level
import levels.level1.room1
import levels.level1.room2
import levels.level1.room3
import levels.level1.room4
import levels.level1.room5
import levels.level1.room6
import levels.level1.room7
import levels.level1.room8
import levels.level1.room9

class Level1(level.Level):
    def __init__(self):
        level.Level.__init__(self)

        self.rooms[1] = levels.level1.room1.Room1()
        self.rooms[2] = levels.level1.room2.Room2()
        self.rooms[3] = levels.level1.room3.Room3()
        self.rooms[4] = levels.level1.room4.Room4()
        self.rooms[5] = levels.level1.room5.Room5()
        self.rooms[6] = levels.level1.room6.Room6()
        self.rooms[7] = levels.level1.room7.Room7()
        self.rooms[8] = levels.level1.room8.Room8()
        self.rooms[9] = levels.level1.room9.Room9()

        # definiere topologie
        self.rooms[1].left = 2
        self.rooms[1].right = 2
        self.rooms[2].left = 1
        self.rooms[2].right = 6
        self.rooms[2].down = 3
        self.rooms[3].up = 2
        self.rooms[4].left = 8
        self.rooms[6].left = 2
        self.rooms[6].right = 7
        self.rooms[7].left = 6
        self.rooms[7].right = 9
        self.rooms[8].left = 9
        self.rooms[8].right = 4
        self.rooms[9].left = 7
        self.rooms[9].right = 8

        self.spawn_room = 1
        self.spawn_x = 70
        self.spawn_y = 760
        self.spawn_flipped = False

        self.current_room = 1

