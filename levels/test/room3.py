import pygame
import random
import level
import block
import objects
import opponents
import friends
import utilities

class Room3(level.Room):
    def __init__(self):
        level.Room.__init__(self,3)

        # ein paar wölkchen für den hintergrund
        number_of_clouds = random.randint(0,10)
        for n in range (0,number_of_clouds):
            pos1 = random.randint(0,utilities.display_width)
            pos2 = random.randint(30,utilities.display_height*0.5)
            self.bgdeco.add(block.Clouds(pos1-20,pos2))

        # zeichne boden
        ground=utilities.display_height*0.8

        for i in range(0,22):
            j=utilities.display_height-60
            while j > ground:
                self.background.add(block.Block(i*60,j))
                j=j-60

            # und gras obendrauf
            self.background.add(block.Gras(i*60,j+50))

        for i in range(1,15):
            # und trampolins obendrauf
            self.background.add(block.Trampoline(i*80,utilities.display_height-200))

        # wände rechts und links
        for i in range(0,15):
            self.background.add(block.Block(0,i*60))
            self.background.add(block.Block(utilities.display_width-60,i*60))

        # und die decke

        for i in range(0,2):
            self.background.add(block.Block(i*60,0))

        for i in range(5,22):
            self.background.add(block.Block(i*60,0))

        # ganz viele cookies
        for i in range(0,18):
            for j in range(0,6):
                self.objects.add(objects.Cookie(i*60+120,j*60+400))

        # was zum drauf stehen
        for i in range(0,15):
            self.background.add(block.Block(60+i*60,300))
            self.background.add(block.Gras(60+i*60,285))


        self.background.add(block.Trampoline(740,270))



