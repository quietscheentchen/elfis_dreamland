import pygame
import level
import level
import block
import objects
import opponents
import friends
import utilities

class Room101(level.Room):
    def __init__(self):
        level.Room.__init__(self,101)

        self.bgcolor = (216,242,255)

        self.add('background',block.Block(0,964))
        self.add('background',block.Block(60,964))
        self.add('background',block.Block(120,964))
        self.add('background',block.Block(180,964))
        self.add('background',block.Block(240,964))
        self.add('background',block.Block(300,964))
        self.add('background',block.Block(360,964))
        self.add('background',block.Block(420,964))
        self.add('background',block.Block(480,964))
        self.add('background',block.Block(540,964))
        self.add('background',block.Block(600,964))
        self.add('background',block.Block(660,964))
        self.add('background',block.Block(720,964))
        self.add('background',block.Block(780,964))
        self.add('background',block.Block(840,964))
        self.add('background',block.Block(900,964))
        self.add('background',block.Block(960,964))
        self.add('background',block.Block(1020,964))
        self.add('background',block.Block(1080,964))
        self.add('background',block.Block(1140,964))
        self.add('background',block.Block(1200,964))
        self.add('background',block.Block(1260,964))
        self.add('background',block.Block(1260,904))
        self.add('background',block.Block(1260,844))
        self.add('background',block.Block(1260,784))
        self.add('background',block.Block(1260,724))
        self.add('background',block.Block(1260,664))
        self.add('background',block.Block(1260,604))
        self.add('background',block.Block(1260,544))
        self.add('background',block.Block(1260,484))
        self.add('background',block.Block(1260,424))
        self.add('background',block.Block(1260,364))
        self.add('background',block.Block(1260,304))
        self.add('background',block.Block(1260,244))
        self.add('background',block.Block(1260,4))
        self.add('background',block.Block(1260,-56))
        self.add('background',block.Block(1200,-56))
        self.add('background',block.Block(1140,-56))
        self.add('background',block.Block(1080,-56))
        self.add('background',block.Block(1020,-56))
        self.add('background',block.Block(960,-56))
        self.add('background',block.Block(900,-56))
        self.add('background',block.Block(840,-56))
        self.add('background',block.Block(780,-56))
        self.add('background',block.Block(720,-56))
        self.add('background',block.Block(660,-56))
        self.add('background',block.Block(600,-56))
        self.add('background',block.Block(540,-56))
        self.add('background',block.Block(480,-56))
        self.add('background',block.Block(420,-56))
        self.add('background',block.Block(360,-56))
        self.add('background',block.Block(300,-56))
        self.add('background',block.Block(0,-56))
        self.add('background',block.Block(0,4))
        self.add('background',block.Block(0,64))
        self.add('background',block.Block(0,124))
        self.add('background',block.Block(0,184))
        self.add('background',block.Block(0,244))
        self.add('background',block.Block(0,424))
        self.add('background',block.Block(0,484))
        self.add('background',block.Block(0,544))
        self.add('background',block.Block(0,604))
        self.add('background',block.Block(0,664))
        self.add('background',block.Block(0,724))
        self.add('background',block.Block(0,784))
        self.add('background',block.Block(0,844))
        self.add('background',block.Block(0,904))
        self.add('background',block.Block(60,904))
        self.add('background',block.Block(120,904))
        self.add('background',block.Block(180,904))
        self.add('background',block.Block(240,904))
        self.add('background',block.Block(300,904))
        self.add('background',block.Block(360,904))
        self.add('background',block.Block(420,904))
        self.add('background',block.Block(480,904))
        self.add('background',block.Block(540,904))
        self.add('background',block.Block(600,904))
        self.add('background',block.Block(660,904))
        self.add('background',block.Block(720,904))
        self.add('background',block.Block(780,904))
        self.add('background',block.Block(840,904))
        self.add('background',block.Block(900,904))
        self.add('background',block.Block(960,904))
        self.add('background',block.Block(1020,904))
        self.add('background',block.Block(1080,904))
        self.add('background',block.Block(1140,904))
        self.add('background',block.Block(1200,904))
        self.add('background',block.Block(1200,844))
        self.add('background',block.Block(1200,784))
        self.add('background',block.Block(1200,724))
        self.add('background',block.Block(1200,664))
        self.add('background',block.Block(1200,604))
        self.add('background',block.Block(1200,544))
        self.add('background',block.Block(1200,484))
        self.add('background',block.Block(1200,424))
        self.add('background',block.Block(1200,364))
        self.add('background',block.Block(1200,304))
        self.add('background',block.Block(1200,244))
        self.add('background',block.Block(1200,4))
        self.add('background',block.Block(1140,4))
        self.add('background',block.Block(1080,4))
        self.add('background',block.Block(1020,4))
        self.add('background',block.Block(960,4))
        self.add('background',block.Block(900,4))
        self.add('background',block.Block(840,4))
        self.add('background',block.Block(780,4))
        self.add('background',block.Block(720,4))
        self.add('background',block.Block(660,4))
        self.add('background',block.Block(600,4))
        self.add('background',block.Block(540,4))
        self.add('background',block.Block(480,4))
        self.add('background',block.Block(420,4))
        self.add('background',block.Block(360,4))
        self.add('background',block.Block(300,4))
        self.add('background',block.Bar(200,240,(200,240),(720,240),(3,0)))
        self.add('foreground',block.Water(240,844))
        self.add('foreground',block.Water(300,844))
        self.add('foreground',block.Water(360,844))
        self.add('foreground',block.Water(420,844))
        self.add('foreground',block.Water(480,844))
        self.add('foreground',block.Water(540,844))
        self.add('foreground',block.Water(600,844))
        self.add('foreground',block.Water(660,844))
        self.add('foreground',block.Water(720,844))
        self.add('foreground',block.Water(780,844))
        self.add('foreground',block.Water(840,844))
        self.add('foreground',block.Water(900,844))
        self.add('foreground',block.Water(960,844))
        self.add('background',block.Block(1020,844))
        self.add('background',block.Block(1080,844))
        self.add('background',block.Block(1140,844))
        self.add('background',block.Box(179,779),True)
        self.add('background',block.Trampoline(423,732),True)
        self.add('objects',objects.Cookie(304,106),False)
        self.add('objects',objects.Cookie(351,106),False)
        self.add('objects',objects.Cookie(398,106),False)
        self.add('objects',objects.Cookie(445,106),False)
        self.add('objects',objects.Cookie(492,106),False)
        self.add('objects',objects.Cookie(539,106),False)
        self.add('objects',objects.Cookie(586,106),False)
        self.add('objects',objects.Cookie(742,105),False)
        self.add('objects',objects.Cookie(789,105),False)
        self.add('objects',objects.Cookie(836,105),False)
        self.add('objects',objects.Cookie(883,105),False)
        self.add('objects',objects.Cookie(930,105),False)
        self.add('objects',objects.WaterCan(656,112),False)
        self.add('background',block.Gras(790,420))
        self.add('background',block.Gras(850,420))
        self.add('background',block.Gras(910,420))
        self.add('background',block.Box(179,719),True)
        self.add('background',block.Box(179,659),True)
        self.add('foreground',objects.FlowerCusp(61,726,20),False)
        self.add('background',objects.Cookieflower(1117,587,3),False)
        self.add('background',block.Gras(778,751))
        self.add('background',block.Teleport(1244,64,60,180,6,400,0,exact_pos=True,reset_speed=True))
        self.add('background',block.GrasBlock(60,833))
        self.add('background',block.GrasBlock(120,833))
        self.add('background',block.GrasBlock(180,833))
        self.add('background',block.GrasBlock(1020,773))
        self.add('background',block.GrasBlock(1080,773))
        self.add('background',block.GrasBlock(1140,773))
        self.add('background',block.GrasBlock(426,746))
        self.add('background',block.Trampoline(1024,754,minx=1024),True)
        self.add('background',block.Block(240,4))
        self.add('background',block.Block(180,4))
        self.add('background',block.Block(120,4))
        self.add('background',block.Block(60,4))
        self.add('background',block.Block(60,-56))
        self.add('background',block.Block(120,-56))
        self.add('background',block.Block(180,-56))
        self.add('background',block.Block(240,-56))
        self.add('background',block.Block(0,304))
        self.add('background',block.Block(0,364))
