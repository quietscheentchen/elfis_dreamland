import pygame
import level
import level
import block
import objects
import opponents
import friends
import utilities

class Room2(level.Room):
    def __init__(self):
        level.Room.__init__(self,2)

        self.bgcolor = (216,242,255)

        self.add('background',block.Block(0,964))
        self.add('background',block.Block(60,964))
        self.add('background',block.Block(120,964))
        self.add('background',block.Block(180,964))
        self.add('background',block.Block(240,964))
        self.add('background',block.Block(300,964))
        self.add('background',block.Block(360,964))
        self.add('background',block.Block(420,964))
        self.add('background',block.Block(480,964))
        self.add('background',block.Block(540,964))
        self.add('background',block.Block(600,964))
        self.add('background',block.Block(660,964))
        self.add('background',block.Block(720,964))
        self.add('background',block.Block(780,964))
        self.add('background',block.Block(840,964))
        self.add('background',block.Block(900,964))
        self.add('background',block.Block(960,964))
        self.add('background',block.Block(1020,964))
        self.add('background',block.Block(1080,964))
        self.add('background',block.Block(1140,964))
        self.add('background',block.Block(1200,964))
        self.add('background',block.Block(1260,964))
        self.add('background',block.Block(1260,904))
        self.add('background',block.Block(1200,904))
        self.add('background',block.Block(1140,904))
        self.add('background',block.Block(1080,904))
        self.add('background',block.Block(1020,904))
        self.add('background',block.Block(960,904))
        self.add('background',block.Block(900,904))
        self.add('background',block.Block(840,904))
        self.add('background',block.Block(780,904))
        self.add('background',block.Block(720,904))
        self.add('background',block.Block(660,904))
        self.add('background',block.Block(600,904))
        self.add('background',block.Block(540,904))
        self.add('background',block.Block(480,904))
        self.add('background',block.Block(420,904))
        self.add('background',block.Block(360,904))
        self.add('background',block.Block(300,904))
        self.add('background',block.Block(240,904))
        self.add('background',block.Block(180,904))
        self.add('background',block.Block(120,904))
        self.add('background',block.Block(60,904))
        self.add('background',block.Block(0,904))
        self.add('foreground',block.Water(420,844))
        self.add('foreground',block.Water(480,844))
        self.add('foreground',block.Water(540,844))
        self.add('foreground',block.Water(600,844))
        self.add('foreground',block.Water(660,844))
        self.add('foreground',block.Water(720,844))
        self.add('foreground',block.Water(780,844))
        self.add('foreground',block.Water(840,844))
        self.add('background',block.Bar(550,700,(410,700),(700,700),(3,0)))
        self.add('background',block.Trampoline(671,595),True)
        self.add('objects',objects.Cookie(681,514),False)
        self.add('objects',objects.Cookie(587,469),False)
        self.add('objects',objects.Cookie(776,469),False)
        self.add('objects',objects.Cookie(584,359),False)
        self.add('objects',objects.Cookie(771,359),False)
        self.add('objects',objects.Cookie(681,296),False)
        self.add('objects',objects.Cookie(681,405),False)
        self.add('objects',objects.Cookie(868,408),False)
        self.add('objects',objects.Cookie(498,411),False)
        self.add('background',opponents.Upsi(950,745,minx=890,maxx=1190),True)
        self.add('background',block.GrasBlock(0,833))
        self.add('background',block.GrasBlock(60,833))
        self.add('background',block.GrasBlock(120,833))
        self.add('background',block.GrasBlock(180,833))
        self.add('background',block.GrasBlock(240,833))
        self.add('background',block.GrasBlock(300,833))
        self.add('background',block.GrasBlock(360,833))
        self.add('background',block.GrasBlock(900,833))
        self.add('background',block.GrasBlock(960,833))
        self.add('background',block.GrasBlock(1020,833))
        self.add('background',block.GrasBlock(1080,833))
        self.add('background',opponents.EvilFlower(318,649),True)
        self.add('background',block.GrasBlock(1140,833))
        self.add('background',block.GrasBlock(1200,833))
        self.add('background',block.GrasBlock(1260,833))
        self.add('background',block.GrasBlock(706,609))
        self.add('background',block.GrasBlock(646,609))
        self.add('bgdeco',block.DecoFlower(117,650))
        self.add('bgdeco',block.Cloud(105,531))
        self.add('bgdeco',block.Cloud(416,414))
        self.add('bgdeco',block.Cloud(485,390))
        self.add('bgdeco',block.Cloud(11,18))
        self.add('bgdeco',block.Cloud(1028,573))
        self.add('bgdeco',block.Cloud(840,300))
