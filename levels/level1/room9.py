import pygame
import level
import level
import block
import objects
import opponents
import friends
import utilities

class Room9(level.Room):
    def __init__(self):
        level.Room.__init__(self,9)

        self.bgcolor = (216,242,255)

        self.add('background',block.Block(0,964))
        self.add('background',block.Block(1080,964))
        self.add('background',block.Block(1140,964))
        self.add('background',block.Block(1200,964))
        self.add('background',block.Block(1260,964))
        self.add('background',block.Block(1260,904))
        self.add('background',block.Block(1200,904))
        self.add('background',block.Block(1140,904))
        self.add('background',block.Block(1080,904))
        self.add('background',block.Block(0,905))
        self.add('background',block.Block(1020,904))
        self.add('background',block.Block(1020,964))
        self.add('background',block.Block(60,904))
        self.add('background',block.GrasBlock(0,834))
        self.add('background',block.GrasBlock(1080,834))
        self.add('background',block.GrasBlock(1020,834))
        self.add('background',block.GrasBlock(60,834))
        self.add('background',block.GrasBlock(120,834))
        self.add('background',block.GrasBlock(-60,473))
        self.add('background',block.Block(60,964))
        self.add('background',block.Block(120,964))
        self.add('background',block.Block(120,904))
        self.add('background',block.GrasBlock(1140,834))
        self.add('background',block.GrasBlock(1200,834))
        self.add('background',block.GrasBlock(1260,834))
        self.add('background',block.Gras(306,715))
        self.add('background',block.Gras(437,585))
        self.add('background',block.Gras(137,430))
        self.add('background',block.Gras(549,429))
        self.add('background',block.Gras(609,428))
        self.add('background',block.Gras(669,428))
        self.add('background',block.Gras(802,598))
        self.add('background',block.Gras(935,741))
        self.add('background',block.Gras(1003,432))
        self.add('objects',objects.Cookie(143,378),False)
        self.add('objects',objects.Cookie(1010,373),False)
        self.add('objects',objects.Cookie(442,525),False)
        self.add('objects',objects.Cookie(812,540),False)
        self.add('objects',objects.Cookie(939,680),False)
        self.add('objects',objects.Cookie(311,660),False)
        self.add('objects',objects.WaterCan(618,49),False)
        self.add('objects',objects.Cookie(617,120),False)
        self.add('objects',objects.Cookie(619,202),False)
        self.add('objects',objects.Cookie(621,278),False)
        self.add('objects',objects.Cookie(620,355),False)
        self.add('fgdeco',block.Cloud(555,27))
        self.add('fgdeco',block.Cloud(506,88))
        self.add('fgdeco',block.Cloud(619,127))
        self.add('bgdeco',block.Cloud(469,628))
        self.add('bgdeco',block.Cloud(630,530))
        self.add('bgdeco',block.Cloud(64,255))
        self.add('bgdeco',block.Cloud(152,287))
