#
#  utilities.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#






import pygame
import math

display_width = 1280
display_height = 1024

framecount = 0

import world

def get_myrect(sprite):
    if isinstance(sprite,pygame.Rect):
        return sprite

    if hasattr(sprite,'myrect'):
        return sprite.myrect.move(sprite.rect.x,sprite.rect.y)
    else:
        return sprite.rect

def collide_myrect(left,right):
    return get_myrect(left).colliderect(get_myrect(right))

def flip_myrect(myrect,rect,h,v):
    myrect1 = myrect.copy()

    if h:
        myrect1.x = rect.width-myrect.width-myrect.x
    if v:
        myrect1.y = rect.height-myrect.height-myrect.y

    return myrect1

def detect_collisions_with_group(sprite,group):
    colliding_objects = pygame.sprite.spritecollide(sprite,group,False,collide_myrect)

    objects = []

    if hasattr(sprite,'real_speed'):
        sprite_speed_x,sprite_speed_y = sprite.real_speed()
    else:
        sprite_speed_x = sprite.speed_x
        sprite_speed_y = sprite.speed_y


    for obj in colliding_objects:
        sprite_myrect = get_myrect(sprite)
        obj_myrect = get_myrect(obj)

        tx = math.inf
        ty = math.inf

        if hasattr(obj,'real_speed'):
            obj_speed_x,obj_speed_y = obj.real_speed()
        else:
            obj_speed_x = obj.speed_x
            obj_speed_y = obj.speed_y

        speed_x = sprite_speed_x - obj_speed_x
        speed_y = sprite_speed_y - obj_speed_y

        if speed_y > 0:
            ty = (sprite_myrect.y+sprite_myrect.height-obj_myrect.y)/speed_y
        elif speed_y < 0:
            ty = (sprite_myrect.y-obj_myrect.y-obj_myrect.height)/speed_y

        if speed_x > 0:
            tx = (sprite_myrect.x+sprite_myrect.width-obj_myrect.x)/speed_x
        elif speed_x < 0:
            tx = (sprite_myrect.x-obj_myrect.x-obj_myrect.width)/speed_x

        if tx == math.inf and ty == math.inf:
            continue

        if tx < ty:
            t = tx
            if speed_x < 0:
                dir = "right"
            else:
                dir = "left"
        else:
            t = ty
            if speed_y < 0:
                dir = "down"
            else:
                dir = "up"

        objects.append( (t,obj,dir) )

    objects.sort(key = lambda elem: elem[0], reverse=True)

    return objects

def sgn(v):
    if v == 0:
        return 0
    else:
        return math.copysign(1,v)

def round_up(v):
    if v < 0:
        return math.floor(v)
    else:
        return math.ceil(v)

def merge_collisions(list1,list2):
    res = list1 + list2
    res.sort(key = lambda elem: elem[0], reverse=True)
    return res

def diff_framecount(a,b):
    return a-b

def detect_collisions(sprite):
    background = world.current_world.current_level.rooms[world.current_world.current_level.current_room].background
    bgcollisions = detect_collisions_with_group(sprite,background)

    foreground = world.current_world.current_level.rooms[world.current_world.current_level.current_room].foreground
    fgcollisions = detect_collisions_with_group(sprite,foreground)
    return merge_collisions(fgcollisions, bgcollisions)

