#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



import pygame
import pygame.freetype
import utilities
import tkinter
import tkinter.colorchooser
import math
import yaml

class RoomContainer:
    def __init__(self,room):
        self.objects = []
        for obj in room.objects.sprites():
            self.objects.append(obj.get_attributes())

        self.number = room.number
        self.bgcolor = room.bgcolor

    def load(self,room):
        for obj in self.objects:
            sprite = obj.sprite_type()
            sprite.set_attributes(obj)
            room.objects.add(sprite)

        room.number = self.number
        room.bgcolor = self.bgcolor

class Room:
    def __init__(self,filename=None):
        self.objects = pygame.sprite.LayeredUpdates()
        self.font = pygame.freetype.Font(utilities.dirname+"/fonts/LiberationSans-Bold.ttf",16)
        self.number = 0
        self.bgcolor = (216,242,255)
        self.selected = None
        self.grab = None

        if filename == None:
            return

        f = open(filename,'r')
        container = yaml.load(f)
        f.close()

        container.load(self)

    def add(self,sprite,positioner):
        if positioner == None or self.selected == None:
            def check_pos(x,y):
                for obj in self.objects.sprites():
                    if obj.rect.x == x and obj.rect.y == y:
                        return False
                return True

            relpos = [(10,10), (10,0), (10,-10), (0,-10), (-10,-10), (-10,0), (-10,10), (0,10) ]

            x0 = int((utilities.display_width-sprite.rect.width)/2)
            y0 = int((utilities.display_height-sprite.rect.height)/2)
            x = None
            y = None
            n=1
            while True:
                for p in relpos:
                    x1 = x0+n*p[0]
                    y1 = y0+n*p[1]

                    if check_pos(x1,y1):
                        x = x1
                        y = y1
                        break

                if x != None and y != None:
                    break
                n += 1

            if x == None:
                x = x0
            if y == None:
                y = y0
        else:
            selected_rect = utilities.get_myrect(self.selected)
            sprite_rect = utilities.get_myrect(sprite)

            if positioner == "up":
                x = int(selected_rect.x + (selected_rect.width - sprite_rect.width)/2)
                y = selected_rect.y - sprite_rect.height
            elif positioner == "down":
                x = int(selected_rect.x + (selected_rect.width - sprite_rect.width)/2)
                y = selected_rect.y + selected_rect.height
            elif positioner == "left":
                x = selected_rect.x - sprite_rect.width
                y = int(selected_rect.y + (selected_rect.height - sprite_rect.height)/2)
            elif positioner == "right":
                x = selected_rect.x + selected_rect.width
                y = int(selected_rect.y + (selected_rect.height - sprite_rect.height)/2)

            if hasattr(sprite,'myrect'):
                x -= sprite.myrect.x
                y -= sprite.myrect.y


        sprite.rect.x = x
        sprite.rect.y = y

        self.objects.add(sprite)
        self.selected = sprite
        utilities.modified = True

    def cleanup(self):
        for obj in self.objects.sprites():
            if obj == self.selected:
                continue

            if obj.rect.x+obj.rect.width < 0 or \
               obj.rect.y+obj.rect.height < 0 or \
               obj.rect.x > utilities.display_width or \
               obj.rect.y > utilities.display_height:
                obj.kill()

    def save(self,filename):
        f = open(filename,'w')

        container = RoomContainer(self)
        yaml.dump(container,f)

        f.close()

        utilities.modified = False

        print("saved to",filename)

    def export(self,filename):
        f = open(filename,'w')

        f.write("import pygame\n")
        f.write("import level\n")
        f.write("import level\n")
        f.write("import block\n")
        f.write("import objects\n")
        f.write("import opponents\n")
        f.write("import friends\n")
        f.write("import utilities\n\n")


        f.write("class Room%d(level.Room):\n" % self.number)
        f.write("    def __init__(self):\n")
        f.write("        level.Room.__init__(self,%d)\n\n" % self.number)
        f.write("        self.bgcolor = (%d,%d,%d)\n\n" % (self.bgcolor[0], self.bgcolor[1], self.bgcolor[2]))

        for obj in self.objects.sprites():
            obj.export(f)

        f.close()
        print("exported to",filename)

    def draw(self,display):
        pygame.draw.rect(display,self.bgcolor,pygame.rect.Rect( (0,0), (utilities.display_width, utilities.display_height) ))
        self.objects.draw(display)

        if self.selected != None:
            obj = self.selected

            # zeichne rechteck im falle einer kollision
            if len(pygame.sprite.spritecollide(obj,self.objects,False,utilities.collide_myrect)) > 1:
                pygame.draw.rect(display,(255,0,0),utilities.get_myrect(obj),1)

            # schreibe koordinaten
            pos = (obj.rect.x,obj.rect.y-20)
            if pos[0] < 0:
                pos = (0,pos[1])
            if pos[0] > utilities.display_width-80:
                pos = (utilities.display_width-80,pos[1])
            if pos[1] < 0:
                pos = (pos[0],obj.rect.y+obj.rect.height+5)
                if pos[1] < 0:
                    pos = (pos[0],0)
            if pos[1] > utilities.display_height-18:
                pos = (pos[0],utilities.display_height-18)

            self.font.render_to(display,pos,"("+str(obj.rect.x)+","+str(obj.rect.y)+")")

    def click(self,pos):
        if pos[0] > utilities.display_width:
            self.grab = None
            return

        objects = self.objects.get_sprites_at(pos)
        if len(objects) == 0:
            self.selected = None
            self.grab = None
        else:
            obj = objects[len(objects)-1]
            self.selected = obj
            self.grab = (pos[0]-obj.rect.x,pos[1]-obj.rect.y)

    def mouse_move(self,pos):
        if self.selected == None or self.grab == None:
            return
        x,y = self.grab
        self.selected.rect.x = pos[0] - x
        self.selected.rect.y = pos[1] - y
        utilities.modified = True

    def key_move(self,pos):
        if self.selected == None:
            return
        self.selected.rect.x += pos[0]
        self.selected.rect.y += pos[1]

    def sprite_select(self,pos):
        objects = self.objects.get_sprites_at(pos)
        if len(objects) == 0:
            return None
        if len(objects) == 1:
            return objects[0]

        done = False
        obj = None

        def cancel():
            nonlocal done,obj

            done = True
            obj = None

        def ok():
            nonlocal done,obj
            sel = lb.curselection()
            if len(sel) == 1:
                obj = objects[sel[0]]
            done = True

        root = tkinter.Toplevel(utilities.tk)
        root.title("Mehrfach Auswahl")

        root.protocol("WM_DELETE_WINDOW", cancel)

        lb = tkinter.Listbox(root)
        lb.grid(row=0,column=0,columnspan=2)
        for o in objects:
            lb.insert(tkinter.END,type(o).__name__)

        tkinter.Button(root,text="OK", command=ok).grid(row = 1, column = 0)
        tkinter.Button(root,text="Cancel", command=cancel).grid(row = 1, column = 1)

        while not done:
            utilities.tk.update()

        root.destroy()
        return obj

    def right_click(self,pos):
        obj = self.sprite_select(pos)
        if obj != None:
            obj.right_click()
            return

        if pos[0] > utilities.display_width:
            return

        # right click at background

        number0 = self.number
        bgcolor0 = self.bgcolor

        svnumber = tkinter.StringVar()
        svnumber.set(str(self.number))

        def cancel():
            root.destroy()
            self.number = number0
            self.bgcolor = bgcolor0

        def apply():
            try:
                self.number = int(svnumber.get())
            except ValueError:
                pass

        def colorpicker():
            color = tkinter.colorchooser.askcolor(utilities.webcolor(self.bgcolor))[0]
            if color == None:
                return

            self.bgcolor = (int(color[0]), int(color[1]), int(color[2]))
            webcolor = utilities.webcolor(self.bgcolor)
            colorbutton.config(background=webcolor, activebackground=webcolor)

        def ok():
            utilities.modified = True
            root.destroy()

        root = tkinter.Toplevel(utilities.tk)
        root.title("Room")

        root.protocol("WM_DELETE_WINDOW", cancel)

        svnumber.trace("w",lambda name, index, mode, sv=svnumber: apply())

        tkinter.Label(root,text="room number").grid(row=0,column=0)
        tkinter.Spinbox(root,textvariable=svnumber,from_=0,to=1000).grid(row=0,column=1)

        tkinter.Label(root,text="background color").grid(row=1,column=0)

        bgcolor = utilities.webcolor(self.bgcolor)
        colorbutton = tkinter.Button(root,background=bgcolor,activebackground=bgcolor,command=colorpicker)
        colorbutton.grid(row=1,column=1)

        tkinter.Button(root,text="OK", command=ok).grid(row = 2, column = 0)
        tkinter.Button(root,text="Cancel", command=cancel).grid(row = 2, column = 1)

    def middle_click(self,pos):
        obj = self.sprite_select(pos)
        if obj == None:
            self.selected = None
            return

        if self.selected == obj:
            self.selected = None

        obj.kill()
        utilities.modified = True

