import pygame
import sprite
import utilities

class Cookie(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/cookie.png')
        self.rect = self.image.get_rect()
        self.attr.group = "objects"
        self.attr.reset = False
        self.attributes = [ ("speed_x",int,True), ("speed_y",int,True), ("gravity",int,True), ("collectmode",bool,True) ]

    def export(self,f):
        f.write("        self.add('%s',objects.Cookie(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))


class Umbrella(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/umbrella.png')
        self.rect = self.image.get_rect()
        self.attr.group = "objects"
        self.attr.reset = False

    def export(self,f):
        f.write("        self.add('%s',objects.Umbrella(%d,%d),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.reset))


class WaterCan(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/gieskanne.png')
        self.rect = self.image.get_rect()
        self.attr.reset = False
        self.attr.group = "objects"

    def export(self,f):
        f.write("        self.add('%s',objects.WaterCan(%d,%d),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.reset))


class Cookieflower(sprite.Sprite):

    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/cookieflower.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect((20,22), (37,37))
        self.attributes = [("cookies",int)]
        self.attr.cookies = 3
        self.attr.reset = False

    def export(self,f):
        f.write("        self.add('%s',objects.Cookieflower(%d,%d,%d),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.cookies,self.attr.reset))

class FlowerCusp(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/flower_cusp.png')
        self.rect = self.image.get_rect()
        self.attributes = [("cookies",int)]
        self.attr.cookies = 3
        self.attr.reset = False

    def export(self,f):
        f.write("        self.add('%s',objects.FlowerCusp(%d,%d,%d),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.cookies,self.attr.reset))

class Spiderweb(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/spiderweb.png')
        self.rect = self.image.get_rect()
        self.attr.group = "objects"
        self.attr.reset = True

    def export(self,f):
        f.write("        self.add('%s',objects.Spiderweb(%d,%d),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.reset))

