import pygame
import sprite
import utilities

class Murphy(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/murphy1.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect( (35,self.rect.height*.3), (self.rect.width-100, self.rect.height*.7-4) )
        self.attributes = [("minx",int,True),("maxx",int,True)]
        self.attr.reset = True

    def export(self,f):
        f.write("        self.add('%s',friends.Murphy(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))
