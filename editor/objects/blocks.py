import pygame
import sprite
import utilities

class Block(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/block.png')
        self.rect = self.image.get_rect()

    def export(self,f):
        f.write("        self.add('%s',block.Block(%d,%d))\n" % (self.attr.group,self.rect.x,self.rect.y))

class GrasBlock(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/grasblock.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect( (0,10), (60, 61) )

    def export(self,f):
        f.write("        self.add('%s',block.GrasBlock(%d,%d))\n" % (self.attr.group,self.rect.x,self.rect.y))

class Gras(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/gras.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect( (0,10), (60, 11) )

    def export(self,f):
        f.write("        self.add('%s',block.Gras(%d,%d))\n" % (self.attr.group,self.rect.x,self.rect.y))

class WaterLily(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/seerose.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect( (0,0), (40, 20) )
        self.attr.group = "foreground"
        self.attr.reset = True

    def export(self,f):
        f.write("        self.add('%s',block.WaterLily(%d,%d),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.reset))



class DecoFlower(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/decoflower.png')
        self.rect = self.image.get_rect()
        self.attr.group = "bgdeco"

    def export(self,f):
        f.write("        self.add('%s',block.DecoFlower(%d,%d))\n" % (self.attr.group,self.rect.x,self.rect.y))

class Cloud(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/cloud.png')
        self.rect = self.image.get_rect()
        self.attr.group = "bgdeco"

    def export(self,f):
        f.write("        self.add('%s',block.Cloud(%d,%d))\n" % (self.attr.group,self.rect.x,self.rect.y))


class Water(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/water.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect((0,.6*self.rect.height), (self.rect.width,.4*self.rect.height))
        self.attr.group = "foreground"

    def export(self,f):
        f.write("        self.add('%s',block.Water(%d,%d))\n" % (self.attr.group,self.rect.x,self.rect.y))

class Bar(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/bar.png')
        self.rect = self.image.get_rect()
        self.attr.min_x = self.rect.x
        self.attr.min_y = self.rect.y
        self.attr.max_x = self.rect.x
        self.attr.max_y = self.rect.y
        self.attr.speed_x = 0
        self.attr.speed_y = 0
        self.attributes = [ ("min_x" , int),
                            ("min_y" , int),
                            ("max_x" , int),
                            ("max_y" , int),
                            ("speed_x",int),
                            ("speed_y",int)
                            ]

    def export(self,f):
        f.write("        self.add('%s',block.Bar(%d,%d,(%d,%d),(%d,%d),(%d,%d)))\n" %
        (self.attr.group,
        self.rect.x,
        self.rect.y,
        self.attr.min_x,
        self.attr.min_y,
        self.attr.max_x,
        self.attr.max_y,
        self.attr.speed_x,
        self.attr.speed_y))

class Box(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/box.png')
        self.rect = self.image.get_rect()
        self.attr.reset = True
        self.attributes = [("minx",int,True),("maxx",int,True)]

    def export(self,f):
        f.write("        self.add('%s',block.Box(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))

class Trampoline(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/trampolin.png')
        self.rect = self.image.get_rect()
        self.attr.reset = True
        self.attributes = [("minx",int,True),("maxx",int,True)]

    def export(self,f):
        f.write("        self.add('%s',block.Trampoline(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))

class Bed(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)

        self.attr.flipped = False
        self.attributes = [("flipped",bool)]

        self.changed()

        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect((0,.6*self.rect.height), (self.rect.width,.4*self.rect.height))

    def changed(self):
        self.image = pygame.transform.flip(pygame.image.load(utilities.dirname+'/graphics/bed.png'),self.attr.flipped,False)

    def export(self,f):
        f.write("        self.add('%s',block.Bed(%d,%d,%s))\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.flipped))


class Teleport(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)

        self.attr.width = 120
        self.attr.height = 60
        self.attr.spawn_room = 0
        self.attr.spawn_x = int(utilities.display_width/2)
        self.attr.spawn_y = int(utilities.display_height/2)

        self.attributes = [("width",int),("height",int),("spawn_room",int),("spawn_x",int),("spawn_y",int),("exact_pos",bool,True),("reset_speed",bool,True)]

        self.changed()

    def changed(self):
        x=0
        y=0

        if hasattr(self,'rect'):
            x = self.rect.x
            y = self.rect.y

        self.image = pygame.Surface((self.attr.width,self.attr.height),pygame.SRCALPHA)
        self.rect = pygame.Rect((x,y),(self.attr.width,self.attr.height))

        for x in range(5,self.attr.width,20):
            x1 = x + 10
            if x1 >= self.attr.width:
                x1 = self.attr.width-1
            pygame.draw.line(self.image,(128,128,128),(self.attr.width-x-1,1),(self.attr.width-x1-1,1),2)
            pygame.draw.line(self.image,(128,128,128),(x,self.attr.height-2),(x1,self.attr.height-2),2)

        for y in range(5,self.attr.height,20):
            y1 = y + 10
            if y1 >= self.attr.height:
                y1 = self.attr.height-1
            pygame.draw.line(self.image,(128,128,128),(1,y),(1,y1),2)
            pygame.draw.line(self.image,(128,128,128),(self.attr.width-2,self.attr.height-y-1),(self.attr.width-2,self.attr.height-y1-1),2)

    def export(self,f):
        f.write("        self.add('%s',block.Teleport(%d,%d,%d,%d,%d,%d,%d%s))\n" % \
            (self.attr.group,
             self.rect.x,
             self.rect.y,
             self.attr.width,
             self.attr.height,
             self.attr.spawn_room,
             self.attr.spawn_x,
             self.attr.spawn_y,
             self.opt_attributes_string()))

class Tree(sprite.Sprite):
    def __init__(self,shrink = False):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/tree.png')
        if shrink:
            self.image = pygame.transform.scale(self.image,(250,273))

        self.rect = self.image.get_rect()
        self.attr.group = "bgdeco"

    def export(self,f):
        f.write("        self.add('%s',block.Tree(%d,%d))\n" % (self.attr.group,self.rect.x,self.rect.y))

class Branch(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)

        self.attr.flipped = False
        self.attributes = [("flipped",bool)]

        self.changed()

        self.rect = self.image.get_rect()

    def changed(self):
        self.image = pygame.transform.flip(pygame.image.load(utilities.dirname+'/graphics/branch.png'),self.attr.flipped,False)
        self.myrect = pygame.Rect( (0 if self.attr.flipped else 54,15), (190, 11) )

    def export(self,f):
        f.write("        self.add('%s',block.Branch(%d,%d,%s))\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.flipped))

