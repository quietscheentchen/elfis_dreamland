import pygame
import sprite
import utilities

class Upsi(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.changed()
        self.attributes = [("flipped",bool,True),("minx",int,True),("maxx",int,True)]
        self.attr.reset = True

    def changed(self):
        if hasattr(self,'rect'):
            x = self.rect.x
            y = self.rect.y
        else:
            x = y = 0

        if hasattr(self.attr,'flipped'):
            flipped = self.attr.flipped
        else:
            flipped = False

        self.image = pygame.transform.flip(pygame.image.load(utilities.dirname+'/graphics/upsi.png'),not flipped,False)
        self.rect = self.image.get_rect()

        if not flipped:
            self.myrect = pygame.Rect( (self.rect.width*.05, 0), (self.rect.width*.8, self.rect.height) )
        else:
            self.myrect = pygame.Rect( (self.rect.width*.15, 0), (self.rect.width*.8, self.rect.height) )

        self.rect.x = x
        self.rect.y = y

    def export(self,f):
        f.write("        self.add('%s',opponents.Upsi(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))

class DancingUpsi(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/jumping_upsi.png')
        self.rect = self.image.get_rect()

    def export(self,f):
        f.write("        self.add('%s',opponents.DancingUpsi(%d,%d))\n" % (self.attr.group,self.rect.x,self.rect.y))

class Mouse(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.changed()
        self.attributes = [("flipped",bool,True),("minx",int,True),("maxx",int,True)]
        self.attr.reset = True

    def changed(self):
        if hasattr(self,'rect'):
            x = self.rect.x
            y = self.rect.y
        else:
            x = y = 0

        if hasattr(self.attr,'flipped'):
            flipped = self.attr.flipped
        else:
            flipped = False

        self.image = pygame.transform.flip(pygame.image.load(utilities.dirname+'/graphics/mouse2.png'),not flipped,False)
        self.rect = self.image.get_rect()

        if not flipped:
            self.myrect = pygame.Rect( (self.rect.width*.35, 0), (self.rect.width*.65, self.rect.height) )
        else:
            self.myrect = pygame.Rect( (0, 0), (self.rect.width*.65, self.rect.height) )

        self.rect.x = x
        self.rect.y = y

    def export(self,f):
        f.write("        self.add('%s',opponents.Mouse(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))

class Bug(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.changed()
        self.attributes = [("flipped",bool,True),("minx",int,True),("maxx",int,True)]
        self.attr.reset = True

    def changed(self):
        if hasattr(self,'rect'):
            x = self.rect.x
            y = self.rect.y
        else:
            x = y = 0

        if hasattr(self.attr,'flipped'):
            flipped = self.attr.flipped
        else:
            flipped = False

        self.image = pygame.transform.flip(pygame.image.load(utilities.dirname+'/graphics/bug.png'),not flipped,False)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def export(self,f):
        f.write("        self.add('%s',opponents.Bug(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))

class EvilFlower(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/evilflower3.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect((20,22), (37,37))
        self.attr.reset = True

    def export(self,f):
        f.write("        self.add('%s',opponents.EvilFlower(%d,%d),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.reset))


class UpsiOnCloud(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/upsi_on_cloud.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect( (75, 0), (65,30) )
        self.attr.badweather_color = (180,185,175)
        self.attr.reset = True
        self.attributes = [("badweather_color",'color')]

    def export(self,f):
        f.write("        self.add('%s',opponents.UpsiOnCloud(%d,%d,%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.badweather_color,self.attr.reset))


class Hedgehog(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.changed()
        self.attr.reset = True
        self.attributes = [("flipped",bool,True),("minx",int,True),("maxx",int,True)]

    def changed(self):
        if hasattr(self,'rect'):
            x = self.rect.x
            y = self.rect.y
        else:
            x = y = 0

        if hasattr(self.attr,'flipped'):
            flipped = self.attr.flipped
        else:
            flipped = False

        self.image = pygame.transform.flip(pygame.image.load(utilities.dirname+'/graphics/hedgehog1.png'),not flipped,False)
        self.rect = self.image.get_rect()

        if not flipped:
            self.myrect = pygame.Rect( (self.rect.width*.35, 0), (self.rect.width*.65, self.rect.height) )
        else:
            self.myrect = pygame.Rect( (0, 0), (self.rect.width*.65, self.rect.height) )

        self.rect.x = x
        self.rect.y = y

    def export(self,f):
        f.write("        self.add('%s',opponents.Hedgehog(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))

class Adelheid(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.image = pygame.image.load(utilities.dirname+'/graphics/adelheid1.png')
        self.rect = self.image.get_rect()
        self.attr.reset = True

    def export(self,f):
        f.write("        self.add('%s',opponents.Adelheid(%d,%d),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.attr.reset))

class Moskito(sprite.Sprite):
    def __init__(self):
        sprite.Sprite.__init__(self)
        self.changed()
        self.attr.reset = True
        self.attributes = [("flipped",bool,True),("minx",int,True),("maxx",int,True),("miny",int,True),("maxy",int,True)]

    def changed(self):
        if hasattr(self,'rect'):
            x = self.rect.x
            y = self.rect.y
        else:
            x = y = 0

        if hasattr(self.attr,'flipped'):
            flipped = self.attr.flipped
        else:
            flipped = False

        self.image = pygame.transform.flip(pygame.image.load(utilities.dirname+'/graphics/moskito1.png'),not flipped,False)
        self.rect = self.image.get_rect()

        if not flipped:
            self.myrect = pygame.Rect( (0, 0), (self.rect.width, self.rect.height) )
        else:
            self.myrect = pygame.Rect( (0, 0), (self.rect.width, self.rect.height) )

        self.rect.x = x
        self.rect.y = y

    def export(self,f):
        f.write("        self.add('%s',opponents.Moskito(%d,%d%s),%s)\n" % (self.attr.group,self.rect.x,self.rect.y,self.opt_attributes_string(),self.attr.reset))


