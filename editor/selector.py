#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



import pygame
import objects.blocks
import objects.opponents
import objects.friends
import objects.collectibles
import utilities

class Selector:
    def __init__(self,room):
        self.objects = []
        self.ypos = []

        for n in range(0,9):
            self.objects.append(pygame.sprite.LayeredUpdates())
            self.ypos.append(70)

        self.group = 0

        self.room = room

        self.font = pygame.freetype.Font(utilities.dirname+"/fonts/LiberationSans-Bold.ttf",32)
        self.arrow_left = pygame.rect.Rect((utilities.display_width+utilities.selector_width/2-60-30,10),(30,40))
        self.arrow_right = pygame.rect.Rect((utilities.display_width+utilities.selector_width/2+60,10),(30,40))

        self.positioner_up = pygame.rect.Rect((utilities.display_width+utilities.selector_width/2-10, utilities.display_height-100),(20,20))
        self.positioner_down = pygame.rect.Rect((utilities.display_width+utilities.selector_width/2-10, utilities.display_height-40),(20,20))
        self.positioner_left = pygame.rect.Rect((utilities.display_width+utilities.selector_width/2-40, utilities.display_height-70),(20,20))
        self.positioner_right = pygame.rect.Rect((utilities.display_width+utilities.selector_width/2+20, utilities.display_height-70),(20,20))
        self.positioner_cancel = pygame.rect.Rect((utilities.display_width+utilities.selector_width/2-10, utilities.display_height-70),(20,20))
        self.positioner = None

        self.add(0,objects.blocks.Block())
        self.add(0,objects.blocks.GrasBlock())
        self.add(0,objects.blocks.Gras())
        self.add(0,objects.blocks.Water())
        self.add(0,objects.blocks.Cloud())

        self.add(1,objects.blocks.Bar())
        self.add(1,objects.blocks.Box())
        self.add(1,objects.blocks.Trampoline())
        self.add(1,objects.blocks.WaterLily())

        self.add(2,objects.blocks.Tree(True))
        self.add(2,objects.blocks.Branch())
        self.add(2,objects.collectibles.Spiderweb())

        self.add(3,objects.opponents.Upsi())
        self.add(3,objects.opponents.DancingUpsi())
        self.add(3,objects.opponents.UpsiOnCloud())

        self.add(4,objects.opponents.Mouse())
        self.add(4,objects.opponents.Hedgehog())
        self.add(4,objects.opponents.Bug())
        self.add(4,objects.opponents.Adelheid())
        self.add(4,objects.opponents.Moskito())

        self.add(5,objects.blocks.DecoFlower())
        self.add(5,objects.collectibles.Cookieflower())
        self.add(5,objects.opponents.EvilFlower())
        self.add(5,objects.collectibles.FlowerCusp())

        self.add(6,objects.collectibles.Cookie())
        self.add(6,objects.collectibles.Umbrella())
        self.add(6,objects.collectibles.WaterCan())

        self.add(7,objects.friends.Murphy())

        self.add(8,objects.blocks.Bed())
        self.add(8,objects.blocks.Teleport())

    def add(self,group,sprite):
        sprite.rect.x = int(utilities.display_width + (utilities.selector_width - sprite.rect.width)/2)
        sprite.rect.y = self.ypos[group]
        self.objects[group].add(sprite)
        self.ypos[group] += sprite.rect.height + 20

    def click(self,pos):
        for obj in self.objects[self.group].get_sprites_at(pos):
            self.room.add(type(obj)(),self.positioner)

        if self.arrow_left.collidepoint(pos):
            self.group -= 1
            self.group %= len(self.objects)
        elif self.arrow_right.collidepoint(pos):
            self.group += 1
            self.group %= len(self.objects)
        if self.positioner_up.collidepoint(pos):
            self.positioner = "up"
        elif self.positioner_down.collidepoint(pos):
            self.positioner = "down"
        elif self.positioner_left.collidepoint(pos):
            self.positioner = "left"
        elif self.positioner_right.collidepoint(pos):
            self.positioner = "right"
        elif self.positioner_cancel.collidepoint(pos):
            self.positioner = None


    def draw(self,display):
        pygame.draw.rect(display,(255,255,255),pygame.rect.Rect( (utilities.display_width,0),(utilities.selector_width,utilities.display_height)))
        pygame.draw.line(display,(0,0,100),(utilities.display_width,0),(utilities.display_width,utilities.display_height),2)

        self.objects[self.group].draw(display)

        # arrows
        pygame.draw.polygon(display,(0,0,0),[ self.arrow_left.midleft, self.arrow_left.topright, self.arrow_left.bottomright ])
        pygame.draw.polygon(display,(0,0,0),[ self.arrow_right.midright, self.arrow_right.topleft, self.arrow_right.bottomleft ])

        # group
        self.font.render_to(display,((self.arrow_left.topright[0] + self.arrow_right.topleft[0])/2-8, self.arrow_left.midright[1]-10),str(self.group))

        # positioner
        pygame.draw.rect(display,(0,0,0),self.positioner_up,0 if self.positioner == "up" else 1)
        pygame.draw.rect(display,(0,0,0),self.positioner_down,0 if self.positioner == "down" else 1)
        pygame.draw.rect(display,(0,0,0),self.positioner_left,0 if self.positioner == "left" else 1)
        pygame.draw.rect(display,(0,0,0),self.positioner_right,0 if self.positioner == "right" else 1)
        pygame.draw.line(display,(0,0,0),self.positioner_cancel.topleft,self.positioner_cancel.bottomright,1)
        pygame.draw.line(display,(0,0,0),self.positioner_cancel.topright,self.positioner_cancel.bottomleft,1)




