#!/usr/bin/env python

#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



import pygame
import selector
import room
import utilities
import sys
import tkinter
import tkinter.filedialog
import tkinter.messagebox

pygame.init()
utilities.tk = tkinter.Tk()

utilities.tk.wm_withdraw()

def usage():
    print("Usage: %s" % sys.argv[0])
    print("       %s room.yaml" % sys.argv[0])
    print("       %s room.yaml --export room.py" % sys.argv[0])
    print("       %s room.yaml --save newroom.yaml" % sys.argv[0])
    print("       %s --help" % sys.argv[0])
    sys.exit(1)

if len(sys.argv) == 1:
    room = room.Room()
elif len(sys.argv) == 2:
    if sys.argv[1] == "--help":
        usage()
    else:
        room = room.Room(sys.argv[1])
elif len(sys.argv) == 4 and sys.argv[2] == "--export":
    room.Room(sys.argv[1]).export(sys.argv[3])
    sys.exit(0)
elif len(sys.argv) == 4 and sys.argv[2] == "--save":
    room.Room(sys.argv[1]).save(sys.argv[3])
    sys.exit(0)
else:
    usage()

win = pygame.display.set_mode((utilities.display_width+utilities.selector_width,utilities.display_height))
clock=pygame.time.Clock()


selector = selector.Selector(room)
skyblue = (216,242,255)

key_timer = None
framecount = 0
move = None

quiteditor = False

def quit():
    global quiteditor
    if utilities.modified:
        if tkinter.messagebox.askokcancel("nicht gespeichert","Warnung: Nicht gespeicherte Änderungen\nTrotzdem beenden?",default=tkinter.messagebox.CANCEL):
            quiteditor = True
    else:
        quiteditor = True

while not quiteditor:
    title = "Room "+str(room.number)
    if utilities.modified:
        title += " *"
    pygame.display.set_caption(title)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            quit()
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            room.click(event.pos)
            selector.click(event.pos)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 3:
            room.right_click(event.pos)
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 2:
            room.middle_click(event.pos)
        elif event.type == pygame.MOUSEMOTION and event.buttons[0] == 1:
            room.mouse_move(event.pos)
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_F2:
            filename = tkinter.filedialog.asksaveasfilename(filetypes=(("yaml files","*.yaml"),("all files","*")))
            if isinstance(filename,str) and filename != "":
                room.save(filename)
        elif event.type == pygame.KEYDOWN and event.key == pygame.K_F1:
            filename = tkinter.filedialog.asksaveasfilename(filetypes=(("python files","*.py"),("all files","*")),title="Export As")
            if isinstance(filename,str) and filename != "":
                room.export(filename)
        elif event.type == pygame.KEYDOWN:
            move = None
            if event.key == pygame.K_UP:
                move = (0,-1)
            elif event.key == pygame.K_DOWN:
                move = (0,1)
            elif event.key == pygame.K_LEFT:
                move = (-1,0)
            elif event.key == pygame.K_RIGHT:
                move = (1,0)

            if move != None:
                room.key_move(move)
                key_timer = framecount
        elif event.type == pygame.KEYUP:
            key_timer = None
            move = None

    if move != None:
        if (framecount-key_timer) > 30:
            room.key_move(move)

    room.cleanup()
    room.draw(win)
    selector.draw(win)
    pygame.display.update()
    utilities.tk.update()
    clock.tick(60)
    framecount += 1



