#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



import pygame
import tkinter
import utilities
import copy

class Attributes:
    def __init__(self,sprite):
        self.sprite_type = type(sprite)
        self.group = "background"

class Sprite(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.attr = Attributes(self)
        self.attributes = []

    def get_attributes(self):
        self.attr.x = self.rect.x
        self.attr.y = self.rect.y
        return self.attr

    def set_attributes(self,attr):
        self.attr = attr
        self.rect.x = self.attr.x
        self.rect.y = self.attr.y

        if hasattr(self,'changed'):
            self.changed()

    def opt_attributes_string(self):
        s = ""
        for a in self.attributes:
            if len(a) != 3 or not a[2]:
                continue
            if not hasattr(self.attr,a[0]):
                continue
            v = getattr(self.attr,a[0])
            s += ",%s=%s" % (a[0],v)
        return s

    def right_click(self):
        root = tkinter.Toplevel(utilities.tk)

        attributes = [ ("x",int), ("y",int) ]
        if hasattr(self.attr,'reset'):
            attributes.append( ("reset",bool) )
        attributes += self.attributes

        attr = self.get_attributes()
        attr0 = copy.deepcopy(attr)

        root.title(attr.sprite_type.__name__)

        entries = []

        g = tkinter.StringVar()
        g.set(attr.group)

        disabled_color = '#d9d9d9'

        def apply():
            for n in range(0,len(attributes)):
                a = attributes[n]
                e = entries[n].get()

                if (isinstance(e,str) and e == '') or (a[1] == bool and e == -1):
                    if len(a) == 3 and a[2] and hasattr(attr,a[0]):
                        delattr(attr,a[0])
                elif a[1] == 'color':
                    setattr(attr,a[0],utilities.colortuple(e))
                else:
                    try:
                        setattr(attr,a[0],a[1](e))
                    except ValueError:
                        pass

            attr.group = g.get()
            self.set_attributes(attr)

        def cancel():
            root.destroy()
            self.set_attributes(attr0)

        def ok():
            root.destroy()
            utilities.modified = True

        def optcheckbutton(var,e):
            if e.configure("state")[4] == 'disabled':
                e.configure(state="normal")
                var.set(0)
            else:
                e.configure(state="disabled")
                var.set(-1)

        def optcolorbutton(var,e):
            if e.configure("state")[4] == 'disabled':
                e.configure(state="normal")
                color='#000000'
                var.set(color)
                e.config(background=color, activebackground=color)
            else:
                e.configure(state="disabled")
                var.set('')
                e.config(background=disabled_color, activebackground=disabled_color)

        def colorpicker(var,e):
            color = tkinter.colorchooser.askcolor(var.get())[1]
            if color == None:
                return

            var.set(color)
            e.config(background=color, activebackground=color)

        root.protocol("WM_DELETE_WINDOW", cancel)

        tkinter.Label(root,text="group").grid(row=0)
        g.trace("w",lambda name, index, mode, sv=g: apply())

        gmenu = tkinter.OptionMenu(root,g,"background","foreground","objects","bgdeco","fgdeco")
        gmenu.configure(width=10)
        gmenu.grid(row=0,column=1)

        for n in range(0,len(attributes)):
            a = attributes[n]
            label = tkinter.Label(root,text=a[0])
            label.grid(row=n+1)

            if a[1] == bool:
                var = tkinter.IntVar()
                state="normal"
                if hasattr(attr,a[0]):
                    var.set(int(getattr(attr,a[0])))
                elif len(a) == 3 and a[2]:
                    var.set(-1)
                    state="disabled"

                var.trace("w",lambda name, index, mode, var=var: apply())
                if len(a) == 3 and a[2]:
                    e = tkinter.Checkbutton(root,variable=var,state=state)
                    label.bind("<Button-1>",lambda ev,var=var,e=e:optcheckbutton(var,e))
                else:
                    e = tkinter.Checkbutton(root,variable=var)
            elif a[1] == 'color':
                color = '#000000'
                state = "normal"

                if hasattr(attr,a[0]):
                    color = utilities.webcolor(getattr(attr,a[0]))
                elif len(a) == 3 and a[2]:
                    state = "disabled"
                    color = disabled_color

                var = tkinter.StringVar()
                var.set(color if state != 'disabled' else '')
                var.trace("w",lambda name, index, mode, var=var: apply())

                e = tkinter.Button(root,background=color,activebackground=color,state=state)
                e.config(command=lambda var=var,e=e:colorpicker(var,e))
                if len(a) == 3 and a[2]:
                    label.bind("<Button-1>",lambda ev,var=var,e=e:optcolorbutton(var,e))
            else:
                var = tkinter.StringVar()
                if hasattr(attr,a[0]):
                    var.set(str(getattr(attr,a[0])))

                var.trace("w",lambda name, index, mode, var=var: apply())

                e = tkinter.Entry(root,textvariable=var)

            e.grid(row=n+1,column=1)
            entries.append(var)

        tkinter.Button(root,text="OK", command=ok).grid(row = len(attributes)+1, column = 0)
        tkinter.Button(root,text="Cancel", command=cancel).grid(row = len(attributes)+1, column = 1)


