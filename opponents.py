#
#  opponents.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#




import pygame
import sprite
import utilities
import world
import math
import random
import objects
import block

class Upsi(sprite.Sprite):

    def __init__(self,x,y,flipped=False,minx=0,maxx=utilities.display_width):
        sprite.Sprite.__init__(self,x,y,flipped,minx,maxx)

        self.image_original =pygame.image.load('graphics/upsi2.png')

        self.picture_1 =pygame.image.load('graphics/upsi2.png')

        self.picture_2 = pygame.image.load('graphics/upsi.png')
        self.picture_3 = pygame.image.load('graphics/upsi3.png')

        self.image = self.image_original
        self.picturecount = 1

        self.image_dizzy = pygame.image.load('graphics/dizzyupsi.png')
        self.image_trapped = pygame.image.load('graphics/caught_upsi.png')

        self.rect = self.image.get_rect()

        self.minx = minx
        self.maxx = maxx

        self.speed = 7

        self.cookiecount = 0
        self.attackmode = True
        self.trapped = False
        self.lrcollision = 0
        self.rect.x = x
        self.rect.y = y
        self.speed_x = self.speed if not flipped else -self.speed
        self.speed_y = 0
        self.gravity = 2
        self.timer = 0
        self.on_ground = True

        self._set_myrect()

    def _set_myrect(self):

        if self.trapped:
            myrect = pygame.Rect( (self.rect.width*.02, 0), (self.rect.width*.8, self.rect.height) )
        elif not self.attackmode or self.speed_x == 0:
            myrect = pygame.Rect( (self.rect.width*.2, 0), (self.rect.width*.8, self.rect.height) )
        elif self.speed_x > 0:
            myrect = pygame.Rect( (self.rect.width*.05, 0), (self.rect.width*.8, self.rect.height) )
        elif self.speed_x < 0:
            myrect = pygame.Rect( (self.rect.width*.15, 0), (self.rect.width*.8, self.rect.height) )


        # korrigiere x-koordinate, so dass alte und neue maske übereinander liegen
        if hasattr(self,'myrect'):
            self.rect.x += self.myrect.x - myrect.x

        self.myrect = myrect

        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)     # zeichne das rechteck


    def update(self):
        old_x = self.rect.x

        self.speed_y = self.speed_y + self.gravity
        self.rect.y += self.speed_y
        self.rect.x +=self.speed_x

        self._collision_detect()

        if self.rect.x < self.minx and self.speed_x < 0:
            self.speed_x = -self.speed_x
            self.rect.x = self.minx

        if self.rect.x > self.maxx and self.speed_x > 0:
            self.speed_x = -self.speed_x
            self.rect.x = self.maxx

        if not self.attackmode and utilities.diff_framecount(utilities.framecount,self.timer) > 120:
            self.speed_x = self.speed*(random.randint(0,1)*2 - 1)
            self.attackmode = True

        self.picture()
        self._set_myrect()

        # quick & dirty fix gegen wobbeln
        if self.trapped and self.lrcollision < 0:
            self.rect.x = old_x

    def picture(self):
        if self.attackmode == False:
            self.image = pygame.transform.flip(self.image_dizzy,self.speed_x>0,False)
        elif self.trapped == True:
            self.image = self.image_trapped
        else:
            if self.speed_x ==0:
                self.image_original = self.picture_1
                self.picturecount = 1
            else:
                if utilities.diff_framecount(utilities.framecount,self.timer) > 15:
                    self.timer = utilities.framecount
                    #print('bild soll sich ändern')
                    #print(self.picturecount)
                    if self.picturecount ==1:
                        self.image_original = self.picture_1
                        self.picturecount = 2

                    elif self.picturecount == 2:
                        self.image_original = self.picture_2
                        self.picturecount = 3

                    elif self.picturecount == 3:
                        self.image_original = self.picture_1
                        self.picturecount = 4

                    elif self.picturecount == 4:
                        self.image_original = self.picture_3
                        self.picturecount = 1



            self.image = pygame.transform.flip(self.image_original,self.speed_x>0,False)

    def _collision_detect(self):
        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        lrcollision = 0

        for tpl in collisions:
            sprite = tpl[1]

            if sprite == self:
                continue

            if not utilities.collide_myrect(self,sprite):
                continue

            t = tpl[0]
            direction = tpl[2]

            sprite.collision(direction,t,self,speed_x-sprite.speed_x,speed_y-sprite.speed_y)

            if direction == "right":
                lrcollision = 1
            elif direction == "left":
                lrcollision = -1

        self.trapped = self.lrcollision*lrcollision < 0
        self.lrcollision = lrcollision

    def get_wet(self):
        self.speed_x = 0
        self.attackmode = False
        self.timer = utilities.framecount

    def get_hit(self,elfi):
        if not self.attackmode:
            return

        elfi.cookiecount += self.cookiecount
        self.cookiecount = 0
        self.speed_x = 0
        self.attackmode = False
        self.timer = utilities.framecount

        if elfi.cookiecount>= 100:
            elfi.lives += 1
            elfi.cookiecount -=100

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:
            if not self.attackmode and hasattr(player,'is_movable') and player.is_movable:
                player.block_collision(direction,t,self,speed_x,speed_y)
            return

        if not self.attackmode:
            return

        if direction != 'up':
            player.get_hit()
            if player.cookiecount != 0:
                player.cookiecount -= 1
                self.cookiecount += 1
        else:
            self.get_hit(player)
            player.speed_y = -10
            player.hoptimer = utilities.framecount


    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.rect.x -= utilities.round_up(t*speed_x) + utilities.sgn(speed_x)
            self.speed_x = -self.speed_x

        if direction == "up" or direction == "down":
            self.rect.y -= utilities.round_up(t*speed_y)
            self.speed_y = 0

        if direction == "up":
            self.on_ground = True

class EvilFlower(sprite.Sprite):

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.pictures = [
            pygame.image.load('graphics/evilflower.png'),
            pygame.image.load('graphics/evilflower2.png'),
            pygame.image.load('graphics/evilflower3.png'),
            pygame.image.load('graphics/wiltedflower.png')
        ]
        self.picture = 2

        self.image = self.pictures[self.picture]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.dead = False
        self.attackmode = False
        self.jet = None
        self.timer = 0

        self.myrect = pygame.Rect((20,22), (37,37))
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)

        self.speed_x = 0
        self.speed_y = 0
        self.dist_x = utilities.display_width
        self.dist_y = utilities.display_height
        self.mouth_x = self.rect.x + self.myrect.x + self.myrect.width/2
        self.mouth_y = self.rect.y + self.myrect.y + self.myrect.height - 8

    def update(self):
        if self.dead:
            return

        self.distance()

        if abs(self.dist_x) > 400:
            self.attackmode = False
        elif not self.attackmode:
            self.attackmode = True
            self.timer = utilities.framecount - 140
            self.picture = 0

        if self.attackmode:
            t = utilities.diff_framecount(utilities.framecount,self.timer)
            if t > 300:
                self.picture = 0
                self.jet = objects.WaterJet(self.mouth_x,self.mouth_y,-self.dist_x,-self.dist_y)
                room = world.current_world.current_level.rooms[world.current_world.current_level.current_room]
                room.add('objects',self.jet)
                self.timer = utilities.framecount
            elif self.jet != None and not self.jet.done:
                self.picture = 1
            else:
                self.jet = None
                self.picture = 0
        else:
            self.picture = 2

        self.image = self.pictures[self.picture]

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:
            return

        if self.dead:
            return

        if direction != 'down':
            self.get_hit(player)
            player.speed_y = -10
            player.hoptimer = utilities.framecount
        else:
            player.get_hit()

    def destroy(self):
        if self.jet != None:
            self.jet.destroy()
        sprite.Sprite.destroy(self)

    def get_hit(self,elfi):
        if self.dead:
            return

        self.picture = 3
        self.dead = True
        self.image = self.pictures[self.picture]
        self.rect.y += 45

        if self.jet != None:
            self.jet.destroy()
            self.jet = None

    def distance(self):
        self.dist_x = utilities.display_width
        self.dist_y = utilities.display_height

        for player in world.current_world.players.sprites():
            if abs(player.rect.x+player.myrect.width/2+player.myrect.x-self.mouth_x) < self.dist_x :

                self.dist_x = self.mouth_x - player.rect.x - player.myrect.width/2 - player.myrect.x
                self.dist_y = self.mouth_y - player.rect.y - player.myrect.height/2 - player.myrect.y


class DancingUpsi(sprite.Sprite):

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)

        self.image_original =pygame.image.load('graphics/jumping_upsi.png')
        self.image = self.image_original
        self.image_dizzy = pygame.image.load('graphics/dizzyupsi2.png')

        self.rect = self.image.get_rect()

        self.cookiecount = 0
        self.attackmode = True

        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0
        self.gravity = 2
        self.timer = 0
        self.angle = 0

        #self._set_myrect()

    def update(self):

        self.speed_y = self.speed_y + self.gravity
        self.rect.y += self.speed_y

        self._collision_detect()
        self.picture()


    def picture(self):
        if self.attackmode == True:
            self.image =  pygame.transform.rotate(self.image_original, self.angle)
        elif utilities.diff_framecount(utilities.framecount,self.timer) < 120:

            self.image = self.image_dizzy
        else:
            self.image = self.image_original
            self.attackmode = True

    def get_wet(self):
        self.speed_x = 0
        self.attackmode = False
        self.timer = utilities.framecount


    def get_hit(self,elfi):
        if not self.attackmode:
            return

        elfi.cookiecount += self.cookiecount
        self.cookiecount = 0
        self.speed_y = 0
        self.attackmode = False
        self.timer = utilities.framecount

        if elfi.cookiecount>= 100:
            elfi.lives += 1
            elfi.cookiecount -=100

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:
            return

        if not self.attackmode:
            return

        if direction != 'up':
            player.get_hit()
            if player.cookiecount != 0:
                player.cookiecount -= 1
                self.cookiecount += 1
        else:
            self.get_hit(player)
            player.hoptimer = utilities.framecount
            player.speed_y = -10



    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "up" or direction == "down":
            self.rect.y -= utilities.round_up(t*speed_y)
            self.speed_y = 0

        if direction == "up" and self.attackmode:
            self.rotate()
            self.speed_y = - 30

    def _collision_detect(self):
        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y


        for tpl in collisions:
            sprite = tpl[1]

            if sprite == self:
                continue

            if not utilities.collide_myrect(self,sprite):
                continue

            t = tpl[0]
            direction = tpl[2]

            sprite.collision(direction,t,self,speed_x-sprite.speed_x,speed_y-sprite.speed_y)

    def sit_on(self,sprite):
        pass

    def rotate(self):
        pic = random.randint(0,5)
        if  pic == 0:
            self.angle = 0
        if pic == 1:
            self.angle = 30
        if pic == 2:
            self.angle = -30
        if pic == 3:
            self.angle = 180
        if pic == 4:
            self.angle = 150
        if pic == 5:
            self.angle = 210



class Mouse(sprite.Sprite):

    def __init__(self,x,y,flipped=False,minx=0, maxx=utilities.display_width):
        sprite.Sprite.__init__(self,x,y,flipped,minx,maxx)

        self.pictures = [ pygame.image.load('graphics/mouse2.png'),
                          pygame.image.load('graphics/mouse1.png'),
                          pygame.image.load('graphics/mouse2.png'),
                          pygame.image.load('graphics/mouse3.png'),
                          pygame.image.load('graphics/deadmouse.png')
                        ]
        self.pictures_flipped = []
        self.masks = []
        self.masks_flipped = []

        for pic in self.pictures:
            pic_flipped = pygame.transform.flip(pic,True,False)
            self.pictures_flipped.append(pic_flipped)
            self.masks.append(pygame.mask.from_surface(pic))
            self.masks_flipped.append(pygame.mask.from_surface(pic_flipped))


        self.dead = False


        self.picture = 0
        self.speed = 7
        self.speed_x = self.speed if not flipped else -self.speed
        self.speed_y = 0
        self.gravity = 2
        self.timer = 0
        self.on_ground = True

        self.set_picture()

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        self.minx = minx
        self.maxx = maxx


        self._set_myrect()

    def _set_myrect(self):
        if self.speed_x > 0:
            myrect = pygame.Rect( (self.rect.width*.35, 0), (self.rect.width*.65, self.rect.height) )
        elif self.speed_x < 0:
            myrect = pygame.Rect( (0, 0), (self.rect.width*.65, self.rect.height) )


        # korrigiere x-koordinate, so dass alte und neue maske übereinander liegen
        if hasattr(self,'myrect'):
            self.rect.x += self.myrect.x - myrect.x

        self.myrect = myrect

       # pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)     # zeichne das rechteck


    def update(self):
        if self.dead:
            if utilities.diff_framecount(utilities.framecount,self.timer)>30:
                self.kill()
            return

        self.speed_y = self.speed_y + self.gravity
        self.rect.y += self.speed_y
        self.rect.x += self.speed_x

        self._collision_detect()

        if self.rect.x < self.minx and self.speed_x < 0:
            self.speed_x = -self.speed_x
            self.rect.x = self.minx

        if self.rect.x > self.maxx and self.speed_x > 0:
            self.speed_x = -self.speed_x
            self.rect.x = self.maxx

        self.set_picture()
        self._set_myrect()



    def set_picture(self):
        if utilities.diff_framecount(utilities.framecount,self.timer) > 15:
            self.timer = utilities.framecount
            self.picture +=1
            if self.picture == 4:
                self.picture =0
        if self.speed_x > 0:
            self.image = self.pictures_flipped[self.picture]
            self.mask = self.masks_flipped[self.picture]
        else:
            self.image = self.pictures[self.picture]
            self.mask = self.masks[self.picture]

    def _collision_detect(self):
        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        for tpl in collisions:
            sprite = tpl[1]

            if sprite == self:
                continue

            if not utilities.collide_myrect(self,sprite):
                continue

            t = tpl[0]
            direction = tpl[2]

            sprite.collision(direction,t,self,speed_x-sprite.speed_x,speed_y-sprite.speed_y)


    def get_wet(self):
        self.get_hit()

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:

            return
        if self.dead == True:
            return
        #if self.attackmode == True:
        if direction != 'up':
            player.get_hit()

        else:
            self.get_hit(player)
            player.speed_y = -10
            player.hoptimer = utilities.framecount

    def get_hit(self,player=None):
        if self.dead:
            return

        self.dead = True
        if self.speed_x <0:
            self.image = self.pictures[4]
        else:
            self.image = self.pictures_flipped[4]
        self.rect.y += 12
        self.timer = utilities.framecount

    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.rect.x -= utilities.round_up(t*speed_x) + utilities.sgn(speed_x)
            self.speed_x = -self.speed_x

        if direction == "up" or direction == "down":
            self.rect.y -= t*speed_y
            self.speed_y = 0

        if direction == "up":
            self.on_ground = True

class UpsiOnCloud(sprite.Sprite):

    def __init__(self,x,y,badweather_color):
        sprite.Sprite.__init__(self,x,y,badweather_color)

        self.image = pygame.image.load('graphics/upsi_on_cloud.png')
        self.dizzy_image = pygame.image.load('graphics/dizzyupsi_on_cloud.png')
        self.dead = False

        self.speed_x = 0
        self.speed_y = 0


        self.on_ground = True

        self.badweather_color = badweather_color

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        self.mode = 'normal'

        self.myrect = pygame.Rect( (75, 0), (65,30) )
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)     # zeichne das rechteck


    def update(self):
        if not hasattr(self,'original_bgcolor'):
            self.original_bgcolor = world.current_world.current_level.rooms[world.current_world.current_level.current_room].bgcolor
            world.current_world.current_level.rooms[world.current_world.current_level.current_room].set_bgcolor(self.badweather_color)
        if not hasattr(self,'timer'):
            self.timer = utilities.framecount

        if self.dead:
            if self.rect.x+self.rect.width < 0:
                self.kill()

        elif utilities.diff_framecount(utilities.framecount,self.timer)>120 and self.mode == 'normal':
            self.mode = 'thunder'
            self.spawn_thunder()

        elif utilities.diff_framecount(utilities.framecount,self.timer)>180 and  self.mode == 'thunder':
            self.mode = 'rain'

        elif utilities.diff_framecount(utilities.framecount,self.timer)>360 and self.mode == 'rain':
            self.mode = 'normal'
            self.timer = utilities.framecount

        if self.mode == 'rain'and (utilities.framecount % 3) == 0:
            self.spawn_rain()


        self.set_speed()

        self.rect.x += self.speed_x

    def set_speed(self):
        if self.mode == 'thunder':
            self.speed_x = 0
            return

        if self.dead:
            self.speed_x = -4
            return
        else:
            player = world.current_world.players.sprites()[0]
            distance = player.rect.x+player.rect.width/2 - self.rect.x-self.rect.width/2

            if distance > 1 and self.speed_x < 5:
                self.speed_x +=3
            if distance < -1 and self.speed_x > -5:
                self.speed_x -= 3

            self.speed_x -= utilities.sgn(self.speed_x)
            #print(self.speed_x)

    def spawn_thunder(self):
        self.thunder = block.Thunderbolt(self.rect.x+ 40, self.rect.y + self.rect.height - 10)
        room = world.current_world.current_level.rooms[world.current_world.current_level.current_room]
        room.add('bgdeco',self.thunder)

    def get_hit(self):
        self.dead = True
        self.mode = 'normal'
        self.image = self.dizzy_image

        if hasattr(self,'thunder'):
            self.thunder.kill()

        world.current_world.current_level.rooms[world.current_world.current_level.current_room].set_bgcolor(self.original_bgcolor)

    def destroy(self):
        if hasattr(self,'thunder'):
            self.thunder.destroy()
        room = world.current_world.current_level.rooms[world.current_world.current_level.current_room]
        for obj in room.objects.sprites():
            if isinstance(obj,objects.RainDrop):
                obj.destroy()

        sprite.Sprite.destroy(self)

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:
            return

        if self.dead:
            return

        if direction == 'up':
            self.get_hit()
            player.speed_y = -10
            player.hoptimer = utilities.framecount

    def spawn_rain(self):

        pos = random.randint(10, self.rect.width-10)

        rain = objects.RainDrop(self.rect.x+ pos, self.rect.y + self.rect.height + 10)
        room = world.current_world.current_level.rooms[world.current_world.current_level.current_room]
        room.add('objects',rain)


class Hedgehog(sprite.Sprite):

    def __init__(self,x,y,flipped=False,minx=0, maxx=utilities.display_width):
        sprite.Sprite.__init__(self,x,y,flipped,minx,maxx)

        self.pictures = [ pygame.image.load('graphics/hedgehog1.png'),
                          pygame.image.load('graphics/hedgehog2.png'),
                          pygame.image.load('graphics/hedgehog1.png'),
                          pygame.image.load('graphics/hedgehog3.png'),
                          pygame.image.load('graphics/dead_hedgehog.png')
                        ]
        self.pictures_flipped = []
        self.masks = []
        self.masks_flipped = []

        for pic in self.pictures:
            pic_flipped = pygame.transform.flip(pic,True,False)
            self.pictures_flipped.append(pic_flipped)
            self.masks.append(pygame.mask.from_surface(pic))
            self.masks_flipped.append(pygame.mask.from_surface(pic_flipped))


        self.dead = False


        self.picture = 0
        self.speed = 3
        self.speed_x = self.speed if not flipped else -self.speed
        self.speed_y = 0
        self.gravity = 2
        self.timer = 0
        self.on_ground = True

        self.set_picture()

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        self.minx = minx
        self.maxx = maxx





    def update(self):
        if self.dead:
            if utilities.diff_framecount(utilities.framecount,self.timer)>60:
                self.kill()
            return

        self.speed_y = self.speed_y + self.gravity
        self.rect.y += self.speed_y
        self.rect.x += self.speed_x

        self._collision_detect()

        if self.rect.x < self.minx and self.speed_x < 0:
            self.speed_x = -self.speed_x
            self.rect.x = self.minx

        if self.rect.x > self.maxx and self.speed_x > 0:
            self.speed_x = -self.speed_x
            self.rect.x = self.maxx

        self.set_picture()



    def set_picture(self):
        if utilities.diff_framecount(utilities.framecount,self.timer) > 15:
            self.timer = utilities.framecount
            self.picture +=1
            if self.picture == 4:
                self.picture =0
        if self.speed_x > 0:
            self.image = self.pictures_flipped[self.picture]
            self.mask = self.masks_flipped[self.picture]
        else:
            self.image = self.pictures[self.picture]
            self.mask = self.masks[self.picture]

    def _collision_detect(self):
        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        for tpl in collisions:
            sprite = tpl[1]

            if sprite == self:
                continue

            if not utilities.collide_myrect(self,sprite):
                continue

            t = tpl[0]
            direction = tpl[2]

            sprite.collision(direction,t,self,speed_x-sprite.speed_x,speed_y-sprite.speed_y)


    def get_wet(self):
        self.get_hit()

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:

            return


        #if self.attackmode == True:
        player.get_hit()

        player.speed_y = -10
        player.hoptimer = utilities.framecount

    def get_hit(self,player=None):
        if self.dead:
            return

        self.dead = True
        if self.speed_x <0:
            self.image = self.pictures[4]
        else:
            self.image = self.pictures_flipped[4]
        self.rect.y += 12
        self.timer = utilities.framecount

    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.rect.x -= utilities.round_up(t*speed_x) + utilities.sgn(speed_x)
            self.speed_x = -self.speed_x

        if direction == "up" or direction == "down":
            self.rect.y -= utilities.round_up(t*speed_y)
            self.speed_y = 0

        if direction == "up":
            self.on_ground = True

class Bug(sprite.Sprite):

    def __init__(self,x,y,flipped=False,minx=25, maxx=utilities.display_width):
        sprite.Sprite.__init__(self,x,y,flipped,minx,maxx)

        self.pictures = [
            pygame.image.load('graphics/bug.png'),
            pygame.image.load('graphics/bug2.png'),
            pygame.image.load('graphics/bug3.png'),
        ]
        self.dead_picture = [ pygame.image.load('graphics/dead_bug.png') ]
        self.dead_picture.append(pygame.transform.flip(self.dead_picture[0],True,False))

        self.want_tree_path = True
        self.path = None
        self.path_pos = None

        self.picture = 0
        self.picture_cache = {}

        self.speed = 3
        self.speed_x = self.speed if not flipped else -self.speed
        self.speed_y = 0
        self.flipped = flipped
        self.angle = 0
        self.gravity = 2
        self.timer = 0
        self.on_ground = True

        self.width = 51
        self.height = 31
        self.anchor = (16,29)

        anchor = self.get_anchor()

        self.x = x+anchor[0]
        self.y = y+anchor[1]

        self.set_picture()

        self.set_pos()

        self.minx = minx
        self.maxx = maxx

    def set_pos(self):
        anchor = self.get_anchor()

        self.rect.x = self.x-anchor[0]
        self.rect.y = self.y-anchor[1]

    def is_dead(self):
        return hasattr(self,'dead')

    def retrieve_paths(self):
        bgdeco = world.current_world.current_level.rooms[world.current_world.current_level.current_room].bgdeco
        fgdeco = world.current_world.current_level.rooms[world.current_world.current_level.current_room].fgdeco

        sprites = bgdeco.sprites() + fgdeco.sprites()

        self.paths = []

        for sprite in sprites:
            if hasattr(sprite,'add_paths'):
                sprite.add_paths(self)

    def update(self):
        if not hasattr(self,'paths'):
            self.retrieve_paths()

        if self.is_dead():
            if utilities.diff_framecount(utilities.framecount,self.dead)>60:
                self.kill()

        if self.path == None or self.is_dead():
            self.angle = 0
            self.speed_y = self.speed_y + self.gravity
            self.y += self.speed_y
            self.x += self.speed_x

            self.set_pos()
            self._collision_detect()
            self._path_detect()

            if self.x < self.minx and self.speed_x < 0:
                self.speed_x = -self.speed_x
                self.flipped = not self.flipped
                self.x = self.minx
                self.set_pos()

            if self.x > self.maxx and self.speed_x > 0:
                self.speed_x = -self.speed_x
                self.flipped = not self.flipped
                self.x = self.maxx
                self.set_pos()
        else:
            n,pos = self.path_pos
            pos += self.speed

            while n < len(self.path)-1 and pos > self.path[n+1][0]:
                n += 1

            if n != self.path_pos[0] and n > 0 and n < len(self.path)-1 and \
               self.path[n-1][1] == self.path[n+1][1] and \
               self.path[n-1][2] == self.path[n+1][2]:
                self.flipped = not self.flipped

            if n == len(self.path)-1:
                self.path = None
            else:
                self.path_pos = (n,pos)

                pos1,x1,y1 = self.path[n]
                pos2,x2,y2 = self.path[n+1]

                self.angle = math.pi/2 if x2 == x1 else math.atan((y1-y2)/(x2-x1))

                if self.path_orientation and self.angle < 0:
                    self.angle += math.pi
                elif not self.path_orientation and self.angle > 0:
                    self.angle -= math.pi

                x0 = self.x
                y0 = self.y

                t = (pos-pos1)/(pos2-pos1)
                self.x = x1+(x2-x1)*t
                self.y = y1+(y2-y1)*t

                self.set_pos()

                self.speed_x = self.x-x0
                self.speed_y = self.y-y0

            self._collision_detect()
            self._path_detect()

        self.set_picture()

    def get_anchor(self):
        s = math.sin(self.angle)
        c = math.cos(self.angle)

        def rotate0(p):
            x,y = p
            if not self.flipped:
                x = self.width - x

            return (c*x+s*y,-s*x+c*y)

        c1 = rotate0((0,0))
        c2 = rotate0((self.width,0))
        c3 = rotate0((self.width,self.height))
        c4 = rotate0((0,self.height))

        c0 = (min(c1[0],c2[0],c3[0],c4[0]),min(c1[1],c2[1],c3[1],c4[1]))

        def rotate(p):
            q = rotate0(p)
            return (int(q[0]-c0[0]),int(q[1]-c0[1]))

        return rotate(self.anchor)

    def set_picture(self):
        if self.is_dead():
            self.image = self.dead_picture[0 if self.flipped else 1]
            self.rect = self.image.get_rect()
            self.set_pos()
            self.myrect = pygame.Rect( (0,0), (self.rect.width, 23) )
            self.anchor = (self.anchor[0],self.myrect.height)
            #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)     # zeichne das rechteck
            return

        if utilities.diff_framecount(utilities.framecount,self.timer) > 3:
            self.timer = utilities.framecount
            self.picture += 1

        self.picture %= len(self.pictures)

        anchor = self.get_anchor()

        if (self.picture,self.angle,self.flipped) in self.picture_cache:
            self.image,self.mask = self.picture_cache[(self.picture,self.angle,self.flipped)]
        else:
            self.image = pygame.transform.rotate(pygame.transform.flip(self.pictures[self.picture],not self.flipped,False),math.degrees(self.angle))
            self.mask = pygame.mask.from_surface(self.image)

            #pygame.draw.circle(self.image,(255,0,0),anchor,2)  # zeichne den anker

            self.picture_cache[(self.picture,self.angle,self.flipped)] = (self.image,self.mask)

        self.rect = self.image.get_rect()
        self.set_pos()

        self.myrect = pygame.Rect( (self.rect.width/2-15,self.rect.height/2-15), (30,anchor[1] if self.angle == 0 else 30) )
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)     # zeichne das rechteck

    def _collision_detect(self):
        anchor = self.get_anchor()
        myrect = self.myrect
        self.myrect = pygame.Rect ( (0,0), (self.rect.width,anchor[1]) )
        collisions = utilities.detect_collisions(self)
        self.myrect = myrect

        speed_x = self.speed_x
        speed_y = self.speed_y

        for tpl in collisions:
            sprite = tpl[1]

            if sprite == self:
                continue

            if not utilities.collide_myrect(self,sprite):
                continue

            t = tpl[0]
            direction = tpl[2]

            sprite.collision(direction,t,self,speed_x-sprite.speed_x,speed_y-sprite.speed_y)

    def _path_detect(self):
        def collision(p1,p2):
            pos1,p1x,p1y = p1
            pos2,p2x,p2y = p2
            q1x = self.x
            q1y = self.y
            q2x = q1x - self.speed_x
            q2y = q1y - self.speed_y

            sd = p1y*q1x - p2y*q1x - p1x*q1y + p2x*q1y - p1y*q2x + p2y*q2x + p1x*q2y - p2x*q2y
            td = -(p1y*q1x) + p2y*q1x + p1x*q1y - p2x*q1y + p1y*q2x - p2y*q2x - p1x*q2y + p2x*q2y

            if sd == 0 or td == 0:
                return None

            s = -(p1y*p2x - p1x*p2y - p1y*q1x + p2y*q1x + p1x*q1y - p2x*q1y)/sd
            t = -(p1y*q1x - p1x*q1y - p1y*q2x + q1y*q2x + p1x*q2y - q1x*q2y)/td

            if s < 0 or s > 1 or t < 0 or t > 1:
                return None

            return pos1+t*(pos2-pos1)

        xdir = -1 if self.flipped else 1

        for orientation,path in self.paths:
            if path == self.path:
                continue

            for n in range(len(path)-1):
                p1 = path[n]
                p2 = path[n+1]

                pos = collision(p1,p2)
                if pos != None:
                    if (p2[1]-p1[1])*xdir > 0:
                        self.path = path
                        self.path_orientation = orientation
                        self.path_pos = (n,pos)
                        return
                    break


    def get_wet(self):
        self.get_hit()

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:
            return

        if self.is_dead():
            return
        #if self.attackmode == True:
        if direction != 'up':
            player.get_hit()

        else:
            self.get_hit(player)
            player.speed_y = -10
            player.hoptimer = utilities.framecount

    def get_hit(self,player=None):
        if self.is_dead():
            return

        self.dead = utilities.framecount
        self.speed_x = 0

    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.x -= utilities.round_up(t*speed_x) + utilities.sgn(speed_x)
            self.speed_x = -self.speed_x
            self.flipped = not self.flipped
            self.set_pos()

        if direction == "up" or direction == "down":
            self.y -= t*speed_y
            self.speed_y = 0
            self.path = None
            self.set_pos()

        if direction == "up":
            self.on_ground = True

class Adelheid(sprite.Sprite):
    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)

        self.normal_pictures = [
            (0,pygame.image.load('graphics/adelheid1.png'),(0,0)),
            self._to_boo
        ]

        self.boo_pictures = [
            (10,pygame.image.load('graphics/adelheid2.png'),(-10,0)),
            (10,pygame.image.load('graphics/adelheid3.png'),(-27,0)),
            (10,pygame.image.load('graphics/adelheid4.png'),(-30,0)),
            (100,pygame.image.load('graphics/adelheid5.png'),(-30,-52)),
            self._to_normal
        ]

        self.dead_pictures = [
            (120,pygame.image.load('graphics/defeated_adelheid.png'),(0,0)),
            self.kill
        ]

        self.picture = 0

        self._to_normal()

        self.image = self.normal_pictures[0][1]
        self.rect = self.image.get_rect()

        self.x = x
        self.y = y

        self.rect.x = self.x + self.normal_pictures[0][2][0]
        self.rect.y = self.y + self.normal_pictures[0][2][1]

        self.speed_x = 0
        self.speed_y = 0
        self.dead_gravity =  2

    def _to_normal(self):
        time,pic,offset = self.normal_pictures[0]
        time = random.randint(300,600)
        self.normal_pictures[0] = (time,pic,offset)

        self.mode = 'normal'

    def _to_boo(self):
        self.mode = 'boo'
        for player in world.current_world.players.sprites():
            player.boo()


    def update(self):
        if not hasattr(self,'timer'):
            self.timer = utilities.framecount

        if self.mode == 'dead':
            self.speed_y += self.dead_gravity
            self.y += self.speed_y
            self.rect.y += self.speed_y
            self._collision_detect()

        pictures = getattr(self,self.mode+'_pictures')

        if utilities.diff_framecount(utilities.framecount,self.timer) < pictures[self.picture][0]:
            return

        self.picture += 1
        self.timer = utilities.framecount

        if callable(pictures[self.picture]):
            pictures[self.picture]()
            self.picture = 0

        self.update_picture()

    def update_picture(self):
        pictures = getattr(self,self.mode+'_pictures')

        self.image = pictures[self.picture][1]
        self.rect = self.image.get_rect()
        self.rect.x = self.x + pictures[self.picture][2][0]
        self.rect.y = self.y + pictures[self.picture][2][1]

    def _collision_detect(self):    # diese funktion wird nur genutzt, wenn adelheid tot ist
        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        for tpl in collisions:
            sprite = tpl[1]

            if sprite == self:
                continue

            if not utilities.collide_myrect(self,sprite):
                continue

            t = tpl[0]
            direction = tpl[2]

            sprite.collision(direction,t,self,speed_x-sprite.speed_x,speed_y-sprite.speed_y)

    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "up":
            self.y -= t*speed_y
            self.rect.y -= t*speed_y
            self.speed_y = 0

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:
            return

        if self.mode == 'dead':
            return

        if direction != 'down':
            self.get_hit(player)
            player.speed_y = -10
            player.hoptimer = utilities.framecount
        else:
            player.get_hit()

    def get_hit(self,elfi):
        if self.mode == 'dead':
            return

        self.mode = 'dead'
        self.timer = utilities.framecount
        self.myrect = pygame.Rect( (0,0), (43,73) ) # TODO
        #pygame.draw.rect(self.dead_pictures[0][1], (255,0,0), self.myrect, 1)     # zeichne das rechteck

        self.picture = 0
        self.update_picture()


class Moskito(sprite.Sprite):

    def __init__(self,x,y,flipped=False,minx=0, maxx=utilities.display_width, miny=0, maxy=utilities.display_height):
        sprite.Sprite.__init__(self,x,y,flipped,minx,maxx,miny,maxy)

        self.pictures = [ pygame.image.load('graphics/moskito2.png'),
                          pygame.image.load('graphics/moskito1.png'),
                          pygame.image.load('graphics/moskito2.png'),
                          pygame.image.load('graphics/moskito3.png'),
                          pygame.image.load('graphics/dizzy_moskito.png')
                        ]
        self.pictures_flipped = []
        self.masks = []
        self.masks_flipped = []

        for pic in self.pictures:
            pic_flipped = pygame.transform.flip(pic,True,False)
            self.pictures_flipped.append(pic_flipped)
            self.masks.append(pygame.mask.from_surface(pic))
            self.masks_flipped.append(pygame.mask.from_surface(pic_flipped))


        self.dead = False


        self.picture = 0
        self.speed_x = 0
        self.speed_y = 0

        self.timer = 0
        self.direction_timer = 0

        self.set_picture()

        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        self.minx = minx
        self.maxx = maxx
        self.miny = miny
        self.maxy = maxy
        self.flipped = flipped

        self._set_myrect()

    def _set_myrect(self):
        if self.flipped:
            myrect = pygame.Rect( (0, 0), (self.rect.width, self.rect.height) )
        else:
            myrect = pygame.Rect( (0, 0), (self.rect.width, self.rect.height) )


        # korrigiere x-koordinate, so dass alte und neue maske übereinander liegen
        if hasattr(self,'myrect'):
            self.rect.x += self.myrect.x - myrect.x
        self.myrect = myrect

        pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)     # zeichne das rechteck


    def update(self):
        if self.dead:
            if utilities.diff_framecount(utilities.framecount,self.timer)>30:
                self.kill()
            return

        if utilities.diff_framecount(utilities.framecount,self.direction_timer)>60:
            self.set_speed()


        self.rect.y += self.speed_y
        self.rect.x += self.speed_x

        if self.rect.x < self.minx and self.speed_x < 0:
            self.speed_x = -self.speed_x
            self.rect.x = self.minx

        if self.rect.x > self.maxx and self.speed_x > 0:
            self.speed_x = -self.speed_x
            self.rect.x = self.maxx

        if self.rect.y < self.miny and self.speed_y < 0:
            self.speed_y = -self.speed_y
            self.rect.y = self.miny

        if self.rect.y > self.maxy and self.speed_y > 0:
            self.speed_y = -self.speed_y
            self.rect.y = self.maxy

        self.set_picture()
        self._set_myrect()



    def set_picture(self):
        if utilities.diff_framecount(utilities.framecount,self.timer) > 1:
            self.timer = utilities.framecount
            self.picture +=1
            if self.picture == 4:
                self.picture =0
        if self.speed_x > 0:
            self.image = self.pictures_flipped[self.picture]
            self.mask = self.masks_flipped[self.picture]
        else:
            self.image = self.pictures[self.picture]
            self.mask = self.masks[self.picture]

    def get_wet(self):
        self.get_hit()

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:

            return
        if self.dead == True:
            return
        #if self.attackmode == True:
        if direction != 'up':
            player.get_hit()

        else:
            self.get_hit(player)
            player.speed_y = -10
            player.hoptimer = utilities.framecount

    def get_hit(self,player=None):
        if self.dead:
            return

        self.dead = True
        if self.speed_x <0:
            self.image = self.pictures[4]
        else:
            self.image = self.pictures_flipped[4]

        self.timer = utilities.framecount

    def set_speed(self):
        if self.speed_x == 0 and self.speed_y == 0:
            self.speed_x = random.randint(-5,5)
            if self.speed_x>0:
                self.flipped = True
            else:
                self.flipped = False
            self.speed_y = random.randint(-5,5)
        else:
            self.speed_x = 0
            self.speed_y = 0
        self.direction_timer = utilities.framecount

class Squirrel(sprite.Sprite):
    def __init__(self,x,y,minx=0, maxx=utilities.display_width, miny=0, maxy=utilities.display_height):
        sprite.Sprite.__init__(self,x,y,minx,maxx,miny,maxy)

        self.tree_pictures = [
            (30,pygame.image.load('graphics/squirrel1.png'),(0,0)),
            (30,pygame.image.load('graphics/squirrel1.png'),(0,0))   # TODO: anderes bild?
        ]

        self.jump_right_pictures = [
            (1.,pygame.image.load('graphics/squirrel2.png'))
        ]

        self.jump_left_pictures = []
        for q,pic in self.jump_right_pictures:
            self.jump_left_pictures.append( (q,pygame.transform.flip(pic,True,False)) )


        self.dead_picture = pygame.image.load('graphics/dead_squirrel.png')

        rect = self.tree_pictures[0][1].get_rect()
        self.width = rect.width
        self.height = rect.height

        self.x = x
        self.y = y

        self.cookiecount = 0
        self.active = False

        self.dead_gravity = 2
        self.gravity = .5
        self.jump_speed_x = 10

        self.tree_area = pygame.Rect(minx,miny,maxx-minx,maxy-miny)

        self.invisible_picture = pygame.Surface((0,0),pygame.SRCALPHA)

        self._to_invisible_mode()

    def _find_trees(self):
        bgdeco = world.current_world.current_level.rooms[world.current_world.current_level.current_room].bgdeco
        fgdeco = world.current_world.current_level.rooms[world.current_world.current_level.current_room].fgdeco

        sprites = bgdeco.sprites() + fgdeco.sprites()

        self.trees = []

        for sprite in sprites:
            if hasattr(sprite,'treetop') and self.tree_area.colliderect(sprite.treetop):
                rect = sprite.treetop.copy()
                rect.x += sprite.rect.x
                rect.y += sprite.rect.y

                self.trees.append(rect)

        if len(self.trees) < 2:
            return

        min_dist = math.inf

        for idx,tree in enumerate(self.trees):
            if self.x < tree.x:
                dist = tree.x-self.x
            elif self.x > tree.x+tree.width:
                dist = self.x-tree.x-tree.width
            else:
                dist = 0

            if dist < min_dist:
                min_dist = dist
                self.tree = idx

        print("self.trees = ",self.trees)
        print("self.tree = ",self.tree)

    def _choose_tree(self):
        min_left = math.inf
        min_right = math.inf

        left_idx = None
        right_idx = None

        for idx,tree in enumerate(self.trees):
            if idx == self.tree:
                continue
            dist = tree.x - self.trees[self.tree].x
            if dist < 0 and -dist < min_left:
                min_left = -dist
                left_idx = idx
            elif dist > 0 and dist < min_right:
                min_right = dist
                right_idx = idx

        if left_idx != None and right_idx != None:
            self.next_tree = left_idx if random.randint(0,1) == 0 else right_idx
        elif left_idx != None:
            self.next_tree = left_idx
        elif right_idx != None:
            self.next_tree = right_idx
        else:
            self.kill()
            return

    def _to_invisible_mode(self):
        self.mode = 'invisible'
        self.image = self.invisible_picture
        self.rect = pygame.Rect(0,0,0,0)
        self.speed_x = self.speed_y = 0
        self.timer = utilities.framecount

    def _to_tree_mode(self):
        print("tree mode")
        self._choose_tree()
        self.mode = 'tree'
        self.picture = 0
        tree = self.trees[self.tree]
        next_tree = self.trees[self.next_tree]
        self.image = pygame.transform.flip(self.tree_pictures[0][1],tree.x > next_tree.x,False)
        self.rect = self.image.get_rect()
        self.rect.x = random.randint(tree.x,tree.x+tree.width-self.width)
        self.rect.y = random.randint(tree.y,tree.y+tree.height-self.height)
        self.speed_x = self.speed_y = 0
        self.timer = utilities.framecount


    def _to_jump_mode(self):
        self.start = (self.rect.x,self.rect.y)

        self.tree = self.next_tree

        tree = self.trees[self.tree]

        self.end = (random.randint(tree.x,tree.x+tree.width-self.width),
                    random.randint(tree.y,tree.y+tree.height-self.height))

        self.mode = 'jump'
        self.active = True
        self.picture = 0

        dx = self.end[0] - self.start[0]
        dy = self.end[1] - self.start[1]

        self.image = self.jump_right_pictures[0][1] if dx>0 else self.jump_left_pictures[0][1]
        self.rect = self.image.get_rect()
        self.rect.x,self.rect.y = self.start

        self.speed_x = self.jump_speed_x if dx>0 else -self.jump_speed_x
        self.speed_y = self.speed_x*dy/dx - self.gravity/2*(1+dx/self.speed_x)

        print("self.tree = ",self.tree," self.start = ",self.start," self.end = ",self.end, "self.speed_x = ",self.speed_x," self.speed_y = ",self.speed_y)

    def _to_dead_mode(self):
        self.mode = 'dead'
        self.picture = 0
        self.image = self.dead_picture
        x,y = self.rect.x,self.rect.y
        self.rect = self.image.get_rect()
        self.rect.x,self.rect.y = x,y
        self.timer = utilities.framecount

    def update(self):
        if not hasattr(self,'trees'):
            self._find_trees()

        if not hasattr(self,'tree'):
            self.kill()

        if self.mode == 'invisible':
            if utilities.diff_framecount(utilities.framecount,self.timer) > 300:
                self._to_tree_mode()
                return
        elif self.mode == 'tree':
            t = self.tree_pictures[self.picture][0]
            if utilities.diff_framecount(utilities.framecount,self.timer) > t:
                self.timer = utilities.framecount
                self.picture += 1
                if self.picture == len(self.tree_pictures):
                    self._to_jump_mode()
                    return
                tree = self.trees[self.tree]
                next_tree = self.trees[self.next_tree]
                self.image = pygame.transform.flip(self.tree_pictures[self.picture][1],tree.x > next_tree.x,False)
        elif self.mode == 'jump':
            self.speed_y += self.gravity
            self.rect.x += self.speed_x
            self.rect.y += self.speed_y

            pictures = self.jump_right_pictures if self.speed_x > 0 else self.jump_left_pictures

            dx = self.end[0]-self.start[0]
            q0 = (self.rect.x - self.start[0])/dx
            q = pictures[self.picture][0]

            if q0 >= q:
                self.picture += 1
                if self.picture == len(pictures):
                    self._to_invisible_mode()
                    return
                self.image = pictures[self.picture][1]
        elif self.mode == 'dead':
            self.speed_y += self.dead_gravity
            self.rect.y += self.speed_y

            self._collision_detect()

            if utilities.diff_framecount(utilities.framecount,self.timer) > 120:
                self.kill()


    def _collision_detect(self):
        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        for tpl in collisions:
            sprite = tpl[1]

            if sprite == self:
                continue

            if not utilities.collide_myrect(self,sprite):
                continue

            t = tpl[0]
            direction = tpl[2]

            sprite.collision(direction,t,self,speed_x-sprite.speed_x,speed_y-sprite.speed_y)

    def block_collision(self,direction,t,block,speed_x,speed_y):
        if direction == "up":
            self.y -= t*speed_y
            self.rect.y -= t*speed_y
            self.speed_y = 0

    def collision(self,direction,t,player,speed_x,speed_y):
        if not hasattr(player,'is_player') or not player.is_player:
            return

        if self.mode == 'jump' and self.active:
            steal = 5 if player.cookiecount >= 5 else player.cookiecount
            player.cookiecount -= steal
            self.cookiecount += steal
            self.active = False
        elif self.mode == 'tree' and direction == 'up':
            player.cookiecount += self.cookiecount
            self.cookiecount = 0
            self._to_dead_mode()


