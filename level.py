#
#   level.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#





import pygame
import utilities
import world

class Room:
    def __init__(self,room_id):
        self.room_id = room_id

        self.bgcolor = (216,242,255)        # skyblue
        self.bgdeco = pygame.sprite.Group()
        self.background = pygame.sprite.Group()
        self.objects = pygame.sprite.Group()
        self.fgdeco = pygame.sprite.Group()
        self.foreground = pygame.sprite.Group()

        self.render_group = pygame.sprite.LayeredDirty()
        self.render_group.set_timing_treshold(1000.0)   # haha... typo in function name

        self.bgdeco.name = "bgdeco"
        self.bgdeco.render_layer = 0
        self.background.name = "background"
        self.background.render_layer = 1
        self.objects.name = "objects"
        self.objects.render_layer = 2
        self.fgdeco.name = "fgdeco"
        self.fgdeco.render_layer = 4
        self.foreground.name = "foreground"
        self.foreground.render_layer = 5

        self.reset_list = []

        self.left = 0
        self.right = 0
        self.up = 0
        self.down = 0

        self.do_fill = True

    def enter(self):
        for player in world.current_world.players.sprites():
            self.render_group.add(player,layer=3)
        self.bgimage = pygame.Surface( (utilities.display_width,utilities.display_height) )
        self.bgimage.fill(self.bgcolor)
        self.do_fill = True
        for sprite in self.render_group.sprites():
            sprite.dirty = 1

    def reset(self):
        reset_list = self.reset_list
        self.reset_list = []
        for sprite,group,args in reset_list:
            sprite.destroy()
            self.add(group,type(sprite)(*args),True)

    def set_bgcolor(self,color):
        self.bgcolor = color
        self.bgimage = pygame.Surface( (utilities.display_width,utilities.display_height) )
        self.bgimage.fill(self.bgcolor)
        self.do_fill = True
        for sprite in self.render_group.sprites():
            sprite.dirty = 1

    def get_group(self,name):
        if not hasattr(self,name):
            return None
        return getattr(self,name)

    def add(self,group,sprite,reset=False):
        g = getattr(self,group)
        g.add(sprite)
        self.render_group.add(sprite,layer=g.render_layer)
        if reset:
            self.reset_list.append((sprite,group,sprite.arguments))

    def draw(self,display):
        if self.do_fill:
            display.blit(self.bgimage,(0,0))

        self.render_group.clear(display,self.bgimage)
        rects = self.render_group.draw(display,self.bgimage)

        if self.do_fill:
            self.do_fill = False
            return [display.get_rect()]
        return rects

    def update(self):
        self.bgdeco.update()
        self.background.update()
        self.objects.update()
        world.current_world.players.update()
        self.fgdeco.update()
        self.foreground.update()

        for sprite in self.render_group.sprites():
            sprite.set_dirty()


class Level:
    def __init__(self):
        self.spawn_x = 0
        self.spawn_y = 0
        self.spawn_flipped = False
        self.spawn_room = 0
        self.current_room = 0
        self.rooms = {}

    def respawn(self,player):
        player.flipped = self.spawn_flipped
        player.rect.x = self.spawn_x
        player.rect.y = self.spawn_y
        self.current_room = self.spawn_room
        self.rooms[self.current_room].enter()

    def reset(self,player):
        for n,room in self.rooms.items():
            room.reset()
        self.respawn(player)

    def leave_room(self,player,direction):
        if direction == "left":
            self.change_room(self.rooms[self.current_room].left,player,utilities.display_width-player.rect.width,player.rect.y)
            return True
        elif direction == "right":
            self.change_room(self.rooms[self.current_room].right,player,0,player.rect.y)
            return True
        elif direction == "up":
            newroom = self.rooms[self.current_room].up
            if newroom != 0:
                self.change_room(newroom,player,player.rect.x,utilities.display_height-player.rect.height)
                return True
        elif direction == "down":
            newroom = self.rooms[self.current_room].down
            if newroom == 0:
                player.is_dead = True
            else:
                self.change_room(newroom,player,player.rect.x,1)
            return True
        return False

    def change_room(self,room,player,x,y):
        self.new_room = (room,player,x,y)

    def draw(self,display):
        return self.rooms[self.current_room].draw(display)

    def update(self):
        self.rooms[self.current_room].update()
        if hasattr(self,'new_room'):
            room,player,x,y = self.new_room
            delattr(self,'new_room')

            player.rect.x = x
            player.rect.y = y
            self.current_room = room
            self.rooms[room].enter()




