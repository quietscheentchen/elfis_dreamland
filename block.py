#
#   block.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#




import pygame
import sprite
import utilities
import world
import math
import utilities
import sys

class Block(sprite.Sprite):
    #blocks für den hintergrund

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/block.png')
        self.rect = self.image.get_rect()
        self.top = y
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0


    def collision(self,direction,t,player,speed_x,speed_y):
        player.block_collision(direction,t,self,speed_x,speed_y)

class GrasBlock(sprite.Sprite):
    #block mit gras obendrauf

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/grasblock.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect( (0,10), (60, 61) )
        self.top = y
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0


    def collision(self,direction,t,player,speed_x,speed_y):
        player.block_collision(direction,t,self,speed_x,speed_y)

class Trampoline(sprite.Sprite):
    #trampolin block

    def __init__(self,x,y,minx=None,maxx=None):
        sprite.Sprite.__init__(self,x,y,minx,maxx)

        self.is_movable = True

        self.image = pygame.image.load('graphics/trampolin.png')
        self.rect = self.image.get_rect()
        self.myrect = self.rect.copy()
        self.top = y
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0
        self.gravity = 2
        self.on_ground = False
        self.on_top_of = None

        if minx != None:
            self.minx = minx
        if maxx != None:
            self.maxx = maxx

    def update(self):
        if self.speed_y == 0 and self.on_ground and self.on_top_of == None:
            return
        self.on_ground = False

        self.speed_y = self.speed_y + self.gravity
        self.rect.x += self.speed_x
        self.rect.y += self.speed_y
        if self.on_top_of != None:
            self.rect.x += self.on_top_of.speed_x
            self.rect.y += self.on_top_of.speed_y

        if hasattr(self,'maxx') and self.rect.x > self.maxx:
            self.rect.x = self.maxx

        if hasattr(self,'minx') and self.rect.x < self.minx:
            self.rect.x = self.minx

        self.boundary_check()

        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        self.speed_x = 0

        for tpl in collisions:
            t = tpl[0]
            block = tpl[1]
            direction = tpl[2]

            if block == self:
                continue

            if not utilities.collide_myrect(self,block):
                continue

            block.collision(direction,t,self,speed_x-block.speed_x,speed_y-block.speed_y)

        if not self.on_ground and self.on_top_of != None:
            self.on_top_of.rider = None
            self.on_top_of = None


    def boundary_check(self):

        if self.rect.x <= -self.rect.width and self.speed_x<0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].left
            self.rect.x = utilities.display_width - self.rect.width - 10

        elif self.rect.x >= utilities.display_width and self.speed_x > 0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].right
            self.rect.x = 5

        elif self.rect.y <= 0 and self.speed_y<0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].up
            if newroom == 0:
                return
            self.rect.y = utilities.display_height - self.rect.height

        elif self.rect.y >= utilities.display_height and self.speed_y > 0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].down
            self.rect.y = 1
        else:
            return

        if newroom == 0:
            self.kill()
        else:
            groups = self.groups()
            self.kill()
            for g0 in groups:
                if hasattr(g0,'name'):
                    world.current_world.current_level.rooms[newroom].add(g0.name,self)

    def collision(self,direction,t,player,speed_x,speed_y):
        player.block_collision(direction,t,self,speed_x,speed_y)

        if direction == "up":
            player.speed_y = -50
            player.hoptimer = utilities.framecount
        if hasattr(player,'is_player') and player.is_player and (direction == "left" or direction == "right"):
            self.on_ground = False
            self.rect.x += speed_x
            collisions = utilities.detect_collisions(self)
            has_collision = False
            for tpl in collisions:
                sprite = tpl[1]
                if sprite != self:
                    has_collision = True
                    break

            if has_collision:
                self.rect.x -= speed_x
                return

            self.rect.x -= speed_x
            self.speed_x = speed_x/8

    def block_collision(self,direction,t,player,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.rect.x -= utilities.round_up(t*speed_x) + utilities.sgn(speed_x)

        if direction == "down":
            self.rect.y -= utilities.round_up(t*speed_y)

        if direction == "up":
            self.rect.y -= utilities.round_up(t*speed_y)
            if self.speed_y > 0:
                self.speed_y = 0
                self.on_ground = True

    def sit_on(self,sprite):
        self.on_top_of = sprite
        sprite.rider = self

    def turn(self):
        pass

class Cloud(sprite.Sprite):
    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/cloud.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0



class Gras(sprite.Sprite):

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/gras.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0


        self.myrect = pygame.Rect( (0,10), (60, 11) )
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)

    def collision(self,direction,t,player,speed_x,speed_y):
        if direction != "up" or t>1:
            return

        player.block_collision(direction,t,self,speed_x,speed_y)

class Box(sprite.Sprite):
    #verschiebbare kiste

    def __init__(self,x,y,minx=None,maxx=None):
        sprite.Sprite.__init__(self,x,y,minx,maxx)
        self.is_movable = True

        self.image = pygame.image.load('graphics/box.png')
        self.rect = self.image.get_rect()
        self.myrect = self.rect.copy()
        self.top = y
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0
        self.gravity = 2
        self.on_ground = False
        self.on_top_of = None

        if minx != None:
            self.minx = minx
        if maxx != None:
            self.maxx = maxx


    def update(self):
        if self.speed_y == 0 and self.on_ground and self.on_top_of == None:
            return

        self.on_ground = False

        self.speed_y = self.speed_y + self.gravity
        self.rect.x += self.speed_x
        self.rect.y += self.speed_y
        if self.on_top_of != None:
            self.rect.x += self.on_top_of.speed_x
            self.rect.y += self.on_top_of.speed_y

        if hasattr(self,'maxx') and self.rect.x > self.maxx:
            self.rect.x = self.maxx

        if hasattr(self,'minx') and self.rect.x < self.minx:
            self.rect.x = self.minx

        self.boundary_check()

        collisions = utilities.detect_collisions(self)

        speed_x = self.speed_x
        speed_y = self.speed_y

        self.speed_x = 0

        for tpl in collisions:
            t = tpl[0]
            block = tpl[1]
            direction = tpl[2]

            if block == self:
                continue

            if not utilities.collide_myrect(self,block):
                continue

            block.collision(direction,t,self,speed_x-block.speed_x,speed_y-block.speed_y)

        if not self.on_ground and self.on_top_of != None:
            self.on_top_of.rider = None
            self.on_top_of = None

    def boundary_check(self):

        if self.rect.x <= -self.rect.width and self.speed_x<0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].left
            self.rect.x = utilities.display_width - self.rect.width - 10

        elif self.rect.x >= utilities.display_width and self.speed_x > 0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].right
            self.rect.x = 5

        elif self.rect.y <= 0 and self.speed_y<0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].up
            if newroom == 0:
                return
            self.rect.y = utilities.display_height - self.rect.height

        elif self.rect.y >= utilities.display_height and self.speed_y > 0:
            newroom = world.current_world.current_level.rooms[world.current_world.current_level.current_room].down
            self.rect.y = 1
        else:
            return

        if newroom == 0:
            self.kill()
        else:
            groups = self.groups()
            self.kill()
            for g0 in groups:
                if hasattr(g0,'name'):
                    world.current_world.current_level.rooms[newroom].add(g0.name,self)

    def collision(self,direction,t,player,speed_x,speed_y):
        player.block_collision(direction,t,self,speed_x,speed_y)

        if hasattr(player,'is_player') and player.is_player and (direction == "left" or direction == "right"):
            self.on_ground = False

            self.rect.x += speed_x
            collisions = utilities.detect_collisions(self)
            has_collision = False
            for tpl in collisions:
                sprite = tpl[1]
                if sprite != self:
                    has_collision = True
                    break

            if has_collision:
                self.rect.x -= speed_x
                return

            self.rect.x -= speed_x
            self.speed_x = speed_x/8

    def block_collision(self,direction,t,player,speed_x,speed_y):
        if direction == "right" or direction == "left":
            self.rect.x -= utilities.round_up(t*speed_x) + utilities.sgn(speed_x)

        if direction == "down":
            self.rect.y -= utilities.round_up(t*speed_y)

        if direction == "up":
            self.rect.y -= utilities.round_up(t*speed_y)
            if self.speed_y > 0:
                self.speed_y = 0
                self.on_ground = True

    def sit_on(self,sprite):
        self.on_top_of = sprite
        sprite.rider = self

    def turn(self):
        pass

class Bar(sprite.Sprite):
    def __init__(self,x,y,min_pos,max_pos,speed):
        sprite.Sprite.__init__(self,x,y,min_pos,max_pos,speed)
        self.image = pygame.image.load('graphics/bar.png')
        self.rect = self.image.get_rect()

        self.myrect = pygame.Rect((0,.2*self.rect.height), (self.rect.width,.7*self.rect.height))
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)

        self.top = y
        self.rect.x = x
        self.rect.y = y
        self.min_x,self.min_y = min_pos
        self.max_x,self.max_y = max_pos
        self.speed_x,self.speed_y = speed

    def update(self):
        if (self.rect.x < self.min_x and self.speed_x < 0) or \
           (self.rect.y < self.min_y and self.speed_y < 0) or \
           (self.rect.x > self.max_x and self.speed_x > 0) or \
           (self.rect.y > self.max_y and self.speed_y > 0):
            self.speed_x = -self.speed_x
            self.speed_y = -self.speed_y

        self.rect.x += self.speed_x
        self.rect.y += self.speed_y


    def collision(self,direction,t,player,speed_x,speed_y):
        if direction != "up":
            return

        player.block_collision(direction,t,self,speed_x,speed_y)
        player.sit_on(self)

class Teleport(sprite.Sprite):
    #teleporter block

    def __init__(self,x,y,width,height,spawn_room,spawn_x,spawn_y,exact_pos=False,reset_speed=False):
        sprite.Sprite.__init__(self,x,y,width,height,spawn_room,spawn_x,spawn_y,exact_pos,reset_speed)
        self.image = pygame.Surface((width,height))
        self.image.set_colorkey( (0,0,0) )
        self.image.fill( (0,0,0) )

        self.rect = pygame.Rect(x,y,width,height)
        self.top = y
        self.speed_x = 0
        self.speed_y = 0
        self.spawn_room = spawn_room
        self.spawn_x = spawn_x
        self.spawn_y = spawn_y
        self.exact_pos = exact_pos
        self.reset_speed = reset_speed


    def collision(self,direction,t,player,speed_x,speed_y):
        if hasattr(player,'is_player') and player.is_player:
            x = self.spawn_x
            y = self.spawn_y
            if not self.exact_pos:
                x += player.rect.x - self.rect.x
                y += player.rect.y - self.rect.y

            if self.reset_speed:
                player.speed_x = 0
                player.speed_y = 0

            world.current_world.current_level.change_room(self.spawn_room,player,x,y)
        else:
            groups = player.groups()
            player.rect.x += self.spawn_x - self.rect.x
            player.rect.y += self.spawn_y - self.rect.y

            player.kill()

            for g0 in groups:
                if hasattr(g0,'name'):
                    world.current_world.current_level.rooms[self.spawn_room].add(g0.name,player)

class Bed(sprite.Sprite):
    # levelziel

    def __init__(self,x,y,flipped):
        sprite.Sprite.__init__(self,x,y,flipped)
        self.image = pygame.transform.flip(pygame.image.load('graphics/bed.png'),flipped,False)
        self.win_image = pygame.transform.flip(pygame.image.load('graphics/sleeping_elfi.png'),flipped,False)
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect((0,.5*self.rect.height), (self.rect.width,.5*self.rect.height))

        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)                         # zeichne das rechteck

        self.top = y
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0

    def update(self):
        if (hasattr(self,'win_timer') and utilities.diff_framecount(utilities.framecount,self.win_timer) > 120):
            world.current_world.won = True

    def collision(self,direction,t,player,speed_x,speed_y):
        if direction == "up" and \
           hasattr(player,'is_player') and player.is_player and \
           player.rect.x+player.myrect.x+player.myrect.width > self.rect.x+40 and \
           player.rect.x+player.myrect.x < self.rect.x+self.rect.width-40:

            self.image = self.win_image
            player.kill()
            self.win_timer = utilities.framecount
        else:
            player.block_collision(direction,t,self,speed_x,speed_y)

class Water(sprite.Sprite):
    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/water.png')
        self.rect = self.image.get_rect()
        self.myrect = pygame.Rect((0,.6*self.rect.height), (self.rect.width,.4*self.rect.height))
        self.top = y
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0

    def collision(self,direction,t,player,speed_x,speed_y):
        player.block_collision(direction,t,self,speed_x,speed_y)
        if hasattr(player,'is_player') and player.is_player:
            player.get_wet(1,True)
        elif hasattr(player,'get_wet'):
            player.get_wet()

class DecoFlower(sprite.Sprite):
    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/decoflower.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0


class Thunderbolt(sprite.Sprite):
    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.picture = pygame.image.load('graphics/thunderbolt.png')
        self.image = self.picture
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0
        self.timer = utilities.framecount
        self.original_bgcolor = world.current_world.current_level.rooms[world.current_world.current_level.current_room].bgcolor


        self.thundercolor = (252,255,234)
        world.current_world.current_level.rooms[world.current_world.current_level.current_room].set_bgcolor(self.thundercolor)

    def update(self):
        if utilities.diff_framecount(utilities.framecount,self.timer) > 15:
            self.image = pygame.Surface((10,10),pygame.SRCALPHA)
            world.current_world.current_level.rooms[world.current_world.current_level.current_room].set_bgcolor(self.original_bgcolor)

        if utilities.diff_framecount(utilities.framecount,self.timer) > 30:
            self.image = self.picture
            world.current_world.current_level.rooms[world.current_world.current_level.current_room].set_bgcolor(self.thundercolor)

        if utilities.diff_framecount(utilities.framecount,self.timer)>60:
            world.current_world.current_level.rooms[world.current_world.current_level.current_room].set_bgcolor(self.original_bgcolor)
            self.kill()


class WaterLily(sprite.Sprite):

    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/seerose.png')
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0
        self.livetimer = 0

        self.myrect = pygame.Rect( (0,0), (40, 20) )
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)

    def collision(self,direction,t,player,speed_x,speed_y):
        if direction != "up" or t > 1:
            return
        self.livetimer+=1
        player.block_collision(direction,t,self,speed_x,speed_y)

    def update(self):
        if self.livetimer > 45:
            self.kill()

class Tree(sprite.Sprite):
    def __init__(self,x,y):
        sprite.Sprite.__init__(self,x,y)
        self.image = pygame.image.load('graphics/tree.png')
        self.rect = self.image.get_rect()
        self.top = y
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0

        self.left_path = [(70,688),(130,657),(250,625),(265,600),(285,500),(285,350)]
        self.right_path = [(600,688),(570,670),(520,660),(450,655),(410,630),(385,620),(366,555),(366,350)]
        self.treetop = pygame.Rect( 123,72,470,170 )

        # zeichne pfade
        #pygame.draw.lines(self.image,(255,0,0),False,self.left_path,2)
        #pygame.draw.lines(self.image,(255,0,0),False,self.right_path,2)

        #zeichne baumkrone
        pygame.draw.rect(self.image, (0,0,255), self.treetop, 1)


    def _add_path(self,player,path0,orientation):
        path = []

        x0,y0=path0[0]
        pos=0
        for x,y in path0:
            pos += math.sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0))
            x0 = x
            y0 = y

            path.append( (pos,self.rect.x+x,self.rect.y+y) )

        for n in range(len(path0)-1,0,-1):
            x,y = path0[n-1]
            pos += math.sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0))
            x0 = x
            y0 = y

            path.append( (pos,self.rect.x+x,self.rect.y+y) )

        player.paths.append( (orientation,path) )

    def add_paths(self,player):
        if not hasattr(player,'want_tree_path') or not player.want_tree_path:
            return

        self._add_path(player,self.left_path,True)
        self._add_path(player,self.right_path,False)

class Branch(sprite.Sprite):
    def __init__(self,x,y,flipped):
        sprite.Sprite.__init__(self,x,y,flipped)
        self.image = pygame.transform.flip(pygame.image.load('graphics/branch.png'),flipped,False)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed_x = 0
        self.speed_y = 0


        self.myrect = pygame.Rect( (0 if flipped else 54,15), (190, 11) )
        #pygame.draw.rect(self.image, (255,0,0), self.myrect, 1)    # zeichne das rechteck

    def collision(self,direction,t,player,speed_x,speed_y):
        if direction != "up" or t>1:
            return

        player.block_collision(direction,t,self,speed_x,speed_y)

