#
#  sprite.py
#
#  Copyright (C) 2019-2020 Sonja Fischer, Mario Prausa
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#



import pygame

class Sprite(pygame.sprite.DirtySprite):
    def __init__(self,*args):
        pygame.sprite.DirtySprite.__init__(self)

        self.arguments = args

    def set_dirty(self):
        if self.dirty == 1:
            return

        if not hasattr(self,'xold_image') or self.xold_image != self.image:
            self.dirty = 1
        if not hasattr(self,'xold_rect') or self.xold_rect != self.rect:
            self.dirty = 1

        self.xold_image = self.image
        self.xold_rect = self.rect.copy()

    def destroy(self):
        self.kill()
